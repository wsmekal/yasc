#/usr/bin/sh

my_dir=yasc$(date +%Y%m%d)

# makes a zip package for release
rm -f $my_dir.zip
rm -rf $my_dir
mkdir -p $my_dir

# copy source
cp atom.cpp $my_dir/
cp data.cpp $my_dir/
cp main.cpp $my_dir/
cp test.cpp $my_dir/
cp clebsch.cpp $my_dir/
cp specfunctions.cpp $my_dir/
cp yascmain.h $my_dir/

# copy miscelleneaous files
cp makefile $my_dir/
cp greens_lib.zip $my_dir/
mkdir -p $my_dir/phases
cp phases/*.TXT $my_dir/phases
mkdir -p $my_dir/radma
cp radma/*.txt $my_dir/radma
cp radma/*.new $my_dir/radma

# copy test files
mkdir -p $my_dir/test_yasc/
cp test_yasc/*_edac.* $my_dir/test_yasc/ 
cp test_yasc/*_yasc.in $my_dir/test_yasc/ 
cp test_yasc/phase.dat $my_dir/test_yasc/
cp test_yasc/plot_test.g $my_dir/test_yasc/ 
cp test_yasc/testall.sh $my_dir/test_yasc/ 

# copy documentation
mkdir -p $my_dir/docs
cp docs/html/* $my_dir/docs

zip -9 -r $my_dir.zip $my_dir/*

rm -rf $my_dir
