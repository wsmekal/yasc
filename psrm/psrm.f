c     calculation for phase shift and radial matrix element
c     main program
      program psrm

	include 'scatuser.def'
	integer quantum,linitial,lnum,npout,npmax,nlmax,npoint,error
	integer lengdash,lengvers,lengroup,lengacom,lengbcom,
     +    lengcopy,lengtitl,lengmsg
	parameter (npmax=256,nlmax=20)
	real benergy
	real radist(npmax),radpot(npmax),eigwave(npmax),plawave(npmax)
	real radmat(npmax,5),phase(npmax,nlmax),wavetemp(npmax,4)
	character*80 pofile,psfile,rmfile,eifile,ssname,sbname,atname
	character*80 namedash,namevers,namegroup,nameacom,namebcom,
     +    namecopy,nametitl,errmsg
	common /intblock/ lengdash,lengvers,lengroup,lengacom,lengbcom,
     +    lengcopy,lengtitl
	common /charblock/ namedash,namevers,namegroup,nameacom,namebcom,
     +    namecopy,nametitl

	nametitl='Calculation for phase shift and radial matrix'//
     +    ' element data'
	call nameadjust(nametitl,spaceline,nametitl,lengtitl)
	call nameadjust(dashline,spaceline,namedash,lengdash)
	call nameadjust(version,spaceline,namevers,lengvers)
	call nameadjust(groupname,spaceline,namegroup,lengroup)
	call nameadjust(acompany,spaceline,nameacom,lengacom)
	call nameadjust(bcompany,spaceline,namebcom,lengbcom)
	call nameadjust(copyright,spaceline,namecopy,lengcopy)

	write(*,'(/,2(/,7x,a),/,5(/,7x,a),/)')
     +    namedash(1:lengdash),nametitl(1:lengtitl),
     +    namegroup(1:lengroup),nameacom(1:lengacom),
     +    namebcom(1:lengbcom),namecopy(1:lengcopy),
     +    namedash(1:lengdash)
	  write(*,'(/,1x,a,/)') 'Name of input file: psrmin.txt'

	error=0
	call pararead(quantum,linitial,lnum,npout,npmax,nlmax,
     +    benergy,radmat,pofile,psfile,rmfile,eifile,ssname,sbname,
     +    atname,error)
	if (error .ne. 0) goto 900
	call poteread(npoint,npmax,radist,radpot,wavetemp,pofile,
     +    error)
	if (error .ne. 0) goto 900
	call calmatrix(quantum,linitial,lnum,npoint,npout,benergy,
     +    radist,radpot,eigwave,plawave,wavetemp,phase,radmat,error)
	if (error .ne. 0) goto 900
	call savmatrix(linitial,lnum,npoint,npout,benergy,phase,
     +    radist,eigwave,radmat,psfile,rmfile,eifile,ssname,sbname,
     +    atname,error)

900	call geterrormsg(error,lengmsg,errmsg)
	write(*,'(/,1x,a)') errmsg(1:lengmsg)

      end
c     end of scat main program
c     eigenfunction calculation
      subroutine eigenfunc(npoint,an,al,benergy,radist,radpot,eigwave,
     +  wavetemp,trymax,error)
	implicit none
	integer npoint,an,al,trymax,error
	real benergy
	real radist(npoint),radpot(npoint),eigwave(npoint)
	real wavetemp(npoint,4)

	integer i,k,nzero
	real aenergy,denergy,localeps

	error=0
	localeps=1.0e-10
	aenergy=benergy
	denergy=0.0
	do 250 i=1,trymax
	  if ((i .gt. 1) .and. (((aenergy+denergy)*benergy .lt. 0.0)
     +      .or. ((aenergy+denergy-benergy)/benergy .lt. -0.75)))
     +      then
	    error=626
	    goto 900
	  elseif ((i .gt. 1) .and. ((aenergy+denergy-benergy)/benergy
     +      .gt. 0.75)) then
	    error=625
	    goto 900
	  elseif ((i .gt. 1) .and. (abs(denergy/benergy) .lt. 0.001))
     +      then
	    goto 260
	  endif
	  aenergy=aenergy+denergy
	  call matcheigen(npoint,al,aenergy,denergy,radist,radpot,
     +      eigwave,wavetemp)
	  if (abs(denergy/benergy) .gt. 0.1)
     +      denergy=sign(benergy/10.0,denergy)
250	continue
	error=624
	goto 900
260	benergy=aenergy
	k=0
	nzero=0
	do 270 i=1,npoint-1
	  if ((k .eq. 0) .and. (eigwave(i) .gt. localeps)) then
	    k=1
	  elseif ((k .eq. 0) .and. (eigwave(i) .lt. -localeps)) then
	    k=-1
	  elseif ((k .lt. 0) .and. (eigwave(i) .gt. localeps)) then
	    nzero=nzero+1
	    k=1
	  elseif ((k .gt. 0) .and. (eigwave(i) .lt. -localeps)) then
	    nzero=nzero+1
	    k=-1
	  endif
270	continue
	if (nzero .gt. an-al-1) then
	  error=627
	elseif (nzero .lt. an-al-1) then
	  error=628
	endif

900	return
      end
c     end of eigenfunc

c     matching the inward and outward eigen function of ODE integrations
      subroutine matcheigen(npoint,al,aenergy,denergy,radist,radpot,
     +  eigwave,wavetemp)
	implicit none
	integer npoint,al
	real aenergy,denergy
	real radist(npoint),radpot(npoint),eigwave(npoint)
	real wavetemp(npoint,4)

	integer i,k,npbeg,npend
	real difmin,wnorm,localeps

	localeps=1.0e-20

	call outeigen(npoint,al,aenergy,radist,radpot,wavetemp(1,1))
	call ineigen(npoint,al,aenergy,radist,radpot,wavetemp(1,2))

	do 220 i=1,npoint
	  if ((abs(wavetemp(i,1)) .gt. localeps) .and.
     +      (abs(wavetemp(i,2)) .gt. localeps)) goto 230
220	continue
230	npbeg=i
	do 240 i=npoint,1,-1
	  if ((abs(wavetemp(i,1)) .gt. localeps) .and.
     +      (abs(wavetemp(i,2)) .gt. localeps)) goto 250
240	continue
250	npend=i
	npbeg=npbeg+2
	npend=npend-2

	k=1
	do 280 i=npbeg,npend
	  if ((abs(wavetemp(i,1)) .gt. localeps) .and.
     +      (abs(wavetemp(i,2)) .gt. localeps)) then
	    wavetemp(i,3)=(wavetemp(i+1,1)-wavetemp(i-1,1))/
     +        wavetemp(i,1)-(wavetemp(i+1,2)-wavetemp(i-1,2))/
     +        wavetemp(i,2)
	    wavetemp(i,4)=abs(wavetemp(i,3))
	    difmin=wavetemp(i,4)
	    k=i
	  else
	    wavetemp(i,3)=0.0
	    wavetemp(i,4)=-1.0
	  endif
280	continue
	do 290 i=npbeg,npend
	  if ((wavetemp(i,4) .ge. 0.0) .and. (wavetemp(i,4) .lt. difmin))
     +      then
	    difmin=abs(wavetemp(i,4))
	    k=i
	  endif
290	continue
	if (abs(wavetemp(k,2)) .gt. localeps) then
	  wnorm=wavetemp(k,1)/wavetemp(k,2)
	else
	  wnorm=1.0
	endif
	denergy=wavetemp(k,3)/(radist(k+1)-radist(k-1))
	do 300 i=1,npoint
	  wavetemp(i,2)=wavetemp(i,2)*wnorm
300	continue

	wnorm=0.0
	do 340 i=1,npoint
	  if (i .lt. k) then
	    eigwave(i)=wavetemp(i,1)
	  else
	    eigwave(i)=wavetemp(i,2)
	  endif
	  if (i .gt. 1) wnorm=wnorm+(eigwave(i)+eigwave(i-1))*
     +      (eigwave(i)+eigwave(i-1))*(radist(i)-radist(i-1))/4.0
340	continue
	difmin=abs(wavetemp(k,1))
	if (wnorm .gt. localeps) then
	  denergy=denergy*difmin*difmin/wnorm
	  wnorm=sqrt(wnorm)
	  do 350 i=1,npoint
	    eigwave(i)=eigwave(i)/wnorm
350	  continue
	endif

	return
      end
c     end of matcheigen

c     outward integrating the ordinary differential equations (ODE)
      subroutine outeigen(npoint,al,aenergy,radist,radpot,radwave)
	implicit none
	integer al,npoint
	real aenergy
	real radist(npoint),radpot(npoint),radwave(npoint)

	integer i,j,npbeg,npend
	real akin,aua,aub,axa,axb
	real ya(2),yb(2)
	external schrodinger

	axa=exp(-69.0/(al+1.0))
	if (axa .lt. radist(1)/2.0) axa=radist(1)/2.0
	do 220 i=1,npoint
	  if (radist(i) .gt. axa) goto 230
220	continue
230	npbeg=i

	do 240 i=npoint,npbeg+16,-1
	  if (radpot(i) .lt. aenergy) goto 250
	  akin=sqrt(radpot(i)-aenergy)
	  axa=radist(i)
	  if (axa*akin .lt. 40.0) goto 250
240	continue
250	npend=i

	axa=radist(npbeg)
	axb=radpot(npbeg)*radist(npbeg)*radist(npbeg)*0.5/(al+1.0)
	yb(1)=(1.0+axb)*axa**(al+1)
	yb(2)=axb*axa**(al+1)
	radwave(npbeg)=yb(1)

	do 280 i=npbeg,npend-1
	  aua=radpot(i)
	  aub=radpot(i+1)
	  axa=radist(i)
	  axb=radist(i+1)
	  do 270 j=1,2
	    ya(j)=yb(j)
270	  continue
	  call rungekutta(al,aenergy,aua,aub,axa,axb,ya,yb,schrodinger)
	  radwave(i+1)=yb(1)
280	continue

	do 300 i=1,npbeg-1
	  radwave(i)=0.0
300	continue
	do 310 i=npend+1,npoint
	  radwave(i)=0.0
310	continue

	return
      end
c     end of outeigen

c     inward integrating the ordinary differential equations (ODE)
      subroutine ineigen(npoint,al,aenergy,radist,radpot,radwave)
	implicit none
	integer al,npoint
	real aenergy
	real radist(npoint),radpot(npoint),radwave(npoint)

	integer i,j,npbeg,npend
	real akin,aua,aub,axa,axb
	real ya(2),yb(2)
	external schrodinger

	axa=exp(-69.0/(al+1.0))
	if (axa .lt. radist(1)/2.0) axa=radist(1)/2.0
	do 220 i=1,npoint
	  if (radist(i) .gt. axa) goto 230
220	continue
230	npbeg=i

	do 240 i=npoint,npbeg+16,-1
	  if (radpot(i) .lt. aenergy) goto 250
	  akin=sqrt(radpot(i)-aenergy)
	  axa=radist(i)
	  if (axa*akin .lt. 40.0) goto 250
240	continue
250	npend=i

	yb(1)=exp(-axa*akin)
	yb(2)=-akin*yb(1)
	radwave(npend)=yb(1)

	do 280 i=npend,npbeg+1,-1
	  aua=radpot(i)
	  aub=radpot(i-1)
	  axa=radist(i)
	  axb=radist(i-1)
	  do 270 j=1,2
	    ya(j)=yb(j)
270	  continue
	  call rungekutta(al,aenergy,aua,aub,axa,axb,ya,yb,schrodinger)
	  radwave(i-1)=yb(1)
280	continue

	do 300 i=1,npbeg-1
	  radwave(i)=0.0
300	continue
	do 310 i=npend+1,npoint
	  radwave(i)=0.0
310	continue

	return
      end
c     end of ineigen

c     integrate one step of ODEs, fourth-order Runge-Kutta
      subroutine rungekutta(al,aenergy,aua,aub,axa,axb,ya,yb,derivs)
	implicit none
	integer al
	real aenergy,axa,axb,aua,aub
	real ya(2),yb(2)
	external derivs

	integer n
	real astep,ax,au
	real ytemp(2),dya(2),dytemp(2),dymid(2)

	astep=axb-axa

	au=aua
	ax=axa
	call derivs(al,aenergy,au,ax,ya,dya)
	do 220 n=1,2
	  ytemp(n)=ya(n)+dya(n)*astep/2.0
220	continue
	au=(aua+aub)/2.0
	ax=(axa+axb)/2.0
	call derivs(al,aenergy,au,ax,ytemp,dytemp)
	do 230 n=1,2
	  ytemp(n)=ya(n)+dytemp(n)*astep/2.0
230	continue
	au=(aua+aub)/2.0
	ax=(axa+axb)/2.0
	call derivs(al,aenergy,au,ax,ytemp,dymid)
	do 240 n=1,2
	  ytemp(n)=ya(n)+dymid(n)*astep
	  dymid(n)=dytemp(n)+dymid(n)
240	continue
	au=aub
	ax=axb
	call derivs(al,aenergy,au,ax,ytemp,dytemp)
	do 250 n=1,2
	  yb(n)=ya(n)+(dya(n)+dytemp(n)+dymid(n)*2.0)*astep/6.0
250	continue

	return
      end
c     end of rungekutta

c     schrodinger equations
      subroutine schrodinger(al,aenergy,au,ax,y,dydx)
	implicit none
	integer al
	real aenergy,au,ax
	real y(2),dydx(2)

	dydx(1)=y(2)+(al+1.0)*y(1)/ax
	dydx(2)=(au-aenergy)*y(1)-(al+1.0)*y(2)/ax

        return
      end
c     end of schrodinger
c     calculate phase shift and radial matrix element
      subroutine calmatrix(quantum,linitial,lnum,npoint,npout,benergy,
     +  radist,radpot,eigwave,plawave,wavetemp,phase,radmat,error)
	implicit none
	integer quantum,linitial,lnum,npoint,npout,error
	real benergy
	real radist(npoint),radpot(npoint),eigwave(npoint),
     +    plawave(npoint)
	real wavetemp(npoint,4),radmat(npout,5),phase(npout,lnum)

	integer i,j,k,an,al,trymax
	real aenergy,value,pi,localeps

	trymax=25
	pi=3.1415926
	localeps=1.0e-30
	error=0

	if (linitial .ge. 0) then
	  an=quantum
	  al=linitial
	  aenergy=benergy
	  call eigenfunc(npoint,an,al,benergy,radist,radpot,eigwave,
     +      wavetemp,trymax,error)
	  if (error .ne. 0) goto 900
	  write(*,'(1x,a,f9.3,a,/)') 'binding energy =',
     +      -benergy/0.26248,' eV'
	endif

	do 230 i=1,npout
	  radmat(i,2)=0.0
	  radmat(i,4)=0.0
230	continue

	do 250 al=0,lnum-1
	  if ((linitial .ge. 0) .and. (al .ne. linitial+1) .and.
     +      (al .ne. linitial-1)) goto 250
	  do 240 i=1,npout
	    aenergy=radmat(i,1)*radmat(i,1)
	    call planefunc(npoint,al,aenergy,radist,radpot,plawave,
     +        wavetemp,value)
	    phase(i,al+1)=value
	    if ((linitial .ge. 0) .and. ((al .eq. linitial+1) .or.
     +        (al .eq. linitial-1))) then
	      call radinteg(npoint,radist,eigwave,plawave,value)
	    endif
	    if ((linitial .ge. 0) .and. (al .eq. linitial+1)) then
	      radmat(i,2)=abs(value)
	      radmat(i,3)=phase(i,al+1)
	    elseif ((linitial .ge. 0) .and. (al .eq. linitial-1)) then
	      radmat(i,4)=abs(value)
	      radmat(i,5)=phase(i,al+1)
	    endif
240	  continue
250	continue

	do 320 j=1,lnum
	  if ((linitial .ge. 0) .and. (j-1 .ne. linitial+1) .and.
     +      (j-1 .ne. linitial-1)) goto 320
	  do 310 i=1,npout-1
	    if (phase(i+1,j)-phase(i,j) .gt. pi*1.5) then
	      do 270 k=1,i
	        phase(k,j)=phase(k,j)+pi*2.0
270	      continue
	    elseif (phase(i+1,j)-phase(i,j) .gt. pi*0.5) then
	      do 280 k=1,i
	        phase(k,j)=phase(k,j)+pi
280	      continue
	    elseif (phase(i,j)-phase(i+1,j) .gt. pi*1.5) then
	      do 290 k=1,i
	        phase(k,j)=phase(k,j)-pi*2.0
290	      continue
	    elseif (phase(i,j)-phase(i+1,j) .gt. pi*0.5) then
	      do 300 k=1,i
	        phase(k,j)=phase(k,j)-pi
300	      continue
	    endif
310	  continue
320	continue
	do 340 j=1,lnum
	  if ((linitial .ge. 0) .and. (j-1 .ne. linitial+1) .and.
     +      (j-1 .ne. linitial-1)) goto 340
	  do 330 i=1,npout
	    phase(i,j)=phase(i,j)-anint(phase(i,j)/(pi*2.0))*pi*2.0
330	  continue
340	continue

900	return
      end
c     end of calmatrix

c     calculate radial dipole matrix element
      subroutine radinteg(npoint,radist,eigwave,plawave,valint)
	implicit none
	integer npoint
	real valint
	real radist(npoint),eigwave(npoint),plawave(npoint)

	integer i
	real axa,axb

	valint=0.0
	do 220 i=1,npoint-1
	  axa=eigwave(i)*plawave(i)*radist(i)
	  axb=eigwave(i+1)*plawave(i+1)*radist(i+1)
	  valint=valint+(axa+axb)*(radist(i+1)-radist(i))/2.0
220	continue

	return
      end
c     end of radinteg

c     plane wave function calculation
      subroutine planefunc(npoint,al,kenergy,radist,radpot,radwave,
     +  wavetemp,valphase)
	implicit none
	integer npoint,al
	real kenergy,valphase
	real radist(npoint),radpot(npoint),radwave(npoint)
	real wavetemp(npoint,4)

	integer i,j,npbeg,npend
	real akin,aua,aub,axa,axb,aenergy,wnorm,xsj,xsy,xsjp,xsyp,
     +    pi,localeps
	real ya(2),yb(2)
	external schrodinger

	pi=3.1415926
	localeps=1.0e-20
	aenergy=kenergy

	axa=exp(-69.0/(al+1.0))
	if (axa .lt. radist(1)/2.0) axa=radist(1)/2.0
	do 220 i=1,npoint
	  if (radist(i) .gt. axa) goto 230
220	continue
230	npbeg=i

	do 240 i=npoint,npbeg+16,-1
	  if (abs(radpot(i)) .lt. 0.001*abs(aenergy)) goto 240
	  if (radpot(i) .lt. aenergy) then
	    akin=sqrt(aenergy-radpot(i))
	    axa=radist(i)
	    if (axa*akin .lt. 40.0) goto 250
	  endif
240	continue
250	npend=i

	axa=radist(npbeg)
	axb=radpot(npbeg)*radist(npbeg)*radist(npbeg)*0.5/(al+1.0)
	yb(1)=(1.0+axb)*axa**(al+1)
	yb(2)=axb*axa**(al+1)
	radwave(npbeg)=yb(1)

	do 280 i=npbeg,npend-1
	  aua=radpot(i)
	  aub=radpot(i+1)
	  axa=radist(i)
	  axb=radist(i+1)
	  do 270 j=1,2
	    ya(j)=yb(j)
270	  continue
	  call rungekutta(al,aenergy,aua,aub,axa,axb,ya,yb,schrodinger)
	  radwave(i+1)=yb(1)
	  if (abs(yb(1)) .gt. localeps) then
	    wavetemp(i+1,1)=yb(2)/yb(1)+(al+1.0)/radist(i+1)
	  else
	    wavetemp(i+1,1)=0.0
	  endif
280	continue

	do 300 i=1,npbeg-1
	  radwave(i)=0.0
300	continue
	do 310 i=npend,npbeg+1,-1
	  if (abs(wavetemp(i,1)) .gt. localeps) goto 320
310	continue
320	npend=i

	akin=sqrt(abs(aenergy))
	axa=akin*radist(npend)
	call xsphebess(al,axa,xsj,xsy,xsjp,xsyp)
	wnorm=wavetemp(npend,1)
	aua=xsjp*akin-xsj*wnorm
	aub=xsyp*akin-xsy*wnorm
	if (abs(aua)+abs(aub) .lt. localeps) then
	  valphase=0.0
	else
	  valphase=atan2(aua,aub)
	endif

	wnorm=radwave(npend)
	do 350 i=npend,npoint
	  axa=radist(i)*sqrt(abs(aenergy))
	  call xsphebess(al,axa,xsj,xsy,xsjp,xsyp)
	  radwave(i)=cos(valphase)*xsj-sin(valphase)*xsy
350	continue
	wnorm=radwave(npend)/wnorm
	do 360 i=1,npend-1
	  radwave(i)=radwave(i)*wnorm
360	continue
	if (wnorm .lt. 0.0) valphase=valphase+pi
	valphase=valphase-anint(valphase/(pi*2.0))*pi*2.0

900	return
      end
c     end of planefunc

c     spherical bessel function
      subroutine xsphebess(al,kx,xsj,xsy,xsjp,xsyp)
	implicit none
	integer al
	real kx,xsj,xsy,xsjp,xsyp

	integer i
	real xsj0,xsy0,xsj1,xsy1,localeps

	localeps=1.0e-10

	if (kx .gt. localeps) then
	  xsj0=sin(kx)
	  xsj1=sin(kx)/kx-cos(kx)
	  xsy0=-cos(kx)
	  xsy1=-cos(kx)/kx-sin(kx)
	  if (al .lt. 1) then
	    xsj=xsj0
	    xsy=xsy0
	    xsjp=cos(kx)
	    xsyp=sin(kx)
	  elseif (al .eq. 1) then
	    xsj=xsj1
	    xsy=xsy1
	    xsjp=cos(kx)/kx-sin(kx)/kx/kx+sin(kx)
	    xsyp=sin(kx)/kx+cos(kx)/kx/kx-cos(kx)
	  else
	    do 220 i=2,al
	      xsj=(i+i-1.0)*xsj1/kx-xsj0
	      xsy=(i+i-1.0)*xsy1/kx-xsy0
	      xsj0=xsj1
	      xsy0=xsy1
	      xsj1=xsj
	      xsy1=xsy
220	    continue
	    xsjp=-al*xsj1/kx+xsj0
	    xsyp=-al*xsy1/kx+xsy0
	  endif
	else
	  xsj=0.0
	  xsy=0.0
	  xsjp=0.0
	  xsyp=0.0
	endif

	return
      end
c     end of xsphebess
c     read in the parameters
      subroutine pararead(quantum,linitial,lnum,npout,npmax,nlmax,
     +  benergy,radmat,pofile,psfile,rmfile,eifile,ssname,sbname,
     +  atname,error)
	implicit none
	integer quantum,linitial,lnum,npout,npmax,nlmax,error
	real benergy
	real radmat(npmax,5)
	character*80 pofile,psfile,rmfile,eifile,atname,sbname,ssname

	integer i,output,datakind,begrow,linenum
	real kmin,kmax,kstep
	character*80 name,buffer

	error=0
	pofile=' '
	psfile=' '
	rmfile=' '
	eifile=' '
	open(unit=10,file='psrmin.txt',status='old',iostat=error)
	if (error .ne. 0) then
	  error=821*1000+201
	  goto 900
	endif
	read(10,*,iostat=error) datakind,begrow,linenum
	if (error .eq. -1) then
	  error=821*1000+206
	elseif (error .ne. 0) then
	  error=821*1000+204
	elseif ((datakind .lt. 820) .or. (datakind .ge. 830)) then
	  error=821*1000+209
	endif
	if (error .ne. 0) goto 260
	do 220 i=2,begrow-1
	  if (error .eq. 0) read(10,*,iostat=error)
220	continue
	do 230 i=1,12
	  if (error .eq. 0) read(10,*,iostat=error) name,buffer
	  if ((name(1:2) .eq. 'po') .or. (name(1:2) .eq. 'PO')) then
	    pofile=buffer
	  elseif ((name(1:2) .eq. 'ps') .or. (name(1:2) .eq. 'PS'))
     +      then
	    psfile=buffer
	  elseif ((name(1:2) .eq. 'rm') .or. (name(1:2) .eq. 'RM'))
     +      then
	    rmfile=buffer
	  elseif ((name(1:2) .eq. 'ei') .or. (name(1:2) .eq. 'EI'))
     +      then
	    eifile=buffer
	  elseif ((name(1:2) .eq. 'ss') .or. (name(1:2) .eq. 'SS'))
     +      then
	    ssname=buffer
	  elseif ((name(1:2) .eq. 'sb') .or. (name(1:2) .eq. 'SB'))
     +      then
	    sbname=buffer
	  elseif ((name(1:2) .eq. 'at') .or. (name(1:2) .eq. 'AT'))
     +      then
	    atname=buffer
	    goto 240
	  endif
230	continue
240	if (error .eq. 0) read (10,*,iostat=error)
	if (error .eq. 0) read (10,*,iostat=error) lnum,output
	if (error .eq. 0) read (10,*,iostat=error) kmin,kmax,kstep
	if ((error .eq. 0) .and. (output .ne. 0))
     +    read (10,*,iostat=error) benergy
	if (error .eq. -1) then
	  error=datakind*1000+206
	elseif (error .ne. 0) then
	  error=datakind*1000+204
	endif
260	close(unit=10)
	if (error .ne. 0) goto 900
	benergy=-0.26248*abs(benergy)

	if (lnum .lt. 1) lnum=1
	if (lnum .gt. 20) lnum=20
	if (lnum .gt. nlmax) lnum=nlmax
	if ((error .eq. 0) .and. (((output .eq. 0) .and.
     +    (psfile .eq. ' ')) .or. ((output .ne. 0) .and.
     +    (rmfile .eq. ' ')))) then
	  error=datakind*1000+631
	elseif ((error .eq. 0) .and. (output .ne. 0)) then
	  do 270 i=1,80
	    if (ssname(i:i) .ne. ' ') goto 280
270	  continue
280	  quantum=ichar(ssname(i:i))-ichar('0')
	  i=i+1
	  if ((ssname(i:i) .eq. 's') .or. (ssname(i:i) .eq. 'S')) then
	    linitial=0
	  elseif ((ssname(i:i) .eq. 'p') .or. (ssname(i:i) .eq. 'P'))
     +      then
	    linitial=1
	  elseif ((ssname(i:i) .eq. 'd') .or. (ssname(i:i) .eq. 'D'))
     +      then
	    linitial=2
	  elseif ((ssname(i:i) .eq. 'f') .or. (ssname(i:i) .eq. 'F'))
     +      then
	    linitial=3
	  elseif ((ssname(i:i) .eq. 'g') .or. (ssname(i:i) .eq. 'G'))
     +      then
	    linitial=4
	  else
	    error=datakind*1000+623
	  endif
	  if ((quantum .lt. linitial+1) .or. (quantum .gt. 6))
     +      error=datakind*1000+623
	  if (lnum .lt. linitial+2) lnum=linitial+2
	elseif (error .eq. 0) then
	  quantum=0
	  linitial=-10
	endif
	if (kmin .lt. 3.0) kmin=3.0
	if (kmin .gt. 20.0) kmin=20.0
	if (kmax .gt. 20.0) kmax=20.0
	if (kmax .lt. kmin) kmax=kmin
	if (kstep .lt. 0.001) then
	  kmax=kmin
	  npout=1
	else
	  npout=1.001+(kmax-kmin)/kstep
	endif
	if (npout .gt. npmax) npout=npmax
	do 300 i=1,npout
	  radmat(i,1)=kmin+(i-1.0)*kstep
300	continue

900	return
      end
c     end of pararead

c     read in the potential data
      subroutine poteread(npoint,npmax,radist,radpot,wavetemp,
     +  pofile,error)
	implicit none
	integer npoint,npmax,error
	real radist(npoint),radpot(npoint)
	real wavetemp(npmax,4)
	character*80 pofile

	integer i,k,datakind,begrow,linenum
	real xa,xb,xc,ya,yc

	error=0
	open(unit=10,file=pofile,status='old',iostat=error)
	if (error .ne. 0) then
	  error=811*1000+201
	  goto 900
	endif
	read(10,*,iostat=error) datakind,begrow,linenum
	if (error .eq. -1) then
	  error=811*1000+206
	elseif (error .ne. 0) then
	  error=811*1000+204
	elseif ((datakind .lt. 810) .or. (datakind .ge. 820)) then
	  error=811*1000+209
	endif
	if (error .ne. 0) goto 260
	do 220 i=2,begrow-1
	  if (error .eq. 0) read(10,*,iostat=error)
220	continue
	npoint=linenum
	if (npoint .gt. npmax) npoint=npmax
	do 240 i=1,npoint
	  if (error .eq. 0) read(10,*,iostat=error) radist(i),radpot(i)
240	continue
	if (error .eq. -1) then
	  error=datakind*1000+206
	elseif (error .ne. 0) then
	  error=datakind*1000+204
	endif
260	close(unit=10)
	if (error .ne. 0) then
	  goto 900
	elseif ((npoint .lt. 16) .or. (npmax .lt. 64)) then
	  error=datakind*1000+701
	elseif (radist(1) .gt. 1.0e-4) then
	  do 270 i=npoint+1,2,-1
	    if (i .gt. npmax) goto 270
	    radist(i)=radist(i-1)
	    radpot(i)=radpot(i-1)
270	  continue
	  radist(1)=radist(1)/2.0
	  if (radist(1) .gt. 1.0e-4) radist(1)=1.0e-4
	  npoint=npoint+1
	  if (npoint .gt. npmax) npoint=npmax
	endif

	if ((error .eq. 0) .and. (npoint .gt. npmax*9/10)) then
	  call spline(radist,radpot,npoint,1.0e30,1.0e30,wavetemp(1,1))
	  xa=radist(1)
	  xb=radist(npoint)
	  k=npmax*3/4
	  do 280 i=1,k
	    xc=xa+(i-1.0)*(i-1.0)/(k-1.0)/(k-1.0)*(xb-xa)
	    wavetemp(i,2)=xc
	    call splint(radist,radpot,wavetemp(1,1),npoint,xc,yc)
	    wavetemp(i,3)=yc
280	  continue
	  npoint=k
	  do 290 i=1,npoint
	    radist(i)=wavetemp(i,2)
	    radpot(i)=wavetemp(i,3)
290	  continue
	endif

	if (error .eq. 0) then
	  xa=radist(1)
	  xc=radist(npoint)
	  xb=xc+xc
	  do 320 i=npoint+1,npmax
	    radist(i)=xc+(xb-xc)*(i-npoint)/(npmax-npoint)
	    radpot(i)=radpot(npoint)
	    radpot(i)=0.0
320	  continue
	  npoint=npmax
	  xa=1.0e-4
	  xb=radist(npoint)
	  if (xb .gt. 100.0) xb=100.0
	  if (xb .lt. xa) error=datakind*1000+207
	endif
	if (error .eq. 0) then
	  call spline(radist,radpot,npoint,1.0e30,1.0e30,wavetemp(1,1))
	  ya=0.0
	  do 330 i=1,npoint
	    xc=xa+(i-1.0)*(i-1.0)/(npoint-1.0)/(npoint-1.0)*(xb-xa)
	    wavetemp(i,2)=xc
	    call splint(radist,radpot,wavetemp(1,1),npoint,xc,yc)
	    wavetemp(i,3)=yc
	    ya=ya+yc
330	  continue
	  do 340 i=1,npoint
	    radist(i)=wavetemp(i,2)
	    if (ya .lt. 0.0) then
	      radpot(i)=0.26248*wavetemp(i,3)/radist(i)
	    else
	      radpot(i)=-0.26248*wavetemp(i,3)/radist(i)
	    endif
340	  continue
	endif

900	return
      end
c     end of poteread

c     save the data of phase shift and radial matrix element
      subroutine savmatrix(linitial,lnum,npoint,npout,benergy,phase,
     +  radist,eigwave,radmat,psfile,rmfile,eifile,ssname,sbname,
     +  atname,error)
	include 'scatuser.def'
	integer linitial,lnum,npoint,npout,error
	real benergy
	real radist(npoint),eigwave(npoint)
	real radmat(npout,5),phase(npout,lnum)
	character*9 pstitle(41)
	character*12 rmtitle(5)
	character*80 psfile,rmfile,eifile,ssname,sbname,atname

	integer i,j,nl,lename,lensym,datakind,begrow,linenum
	integer lengdash,lengvers,lengroup,lengacom,lengbcom,
     +    lengcopy,lengtitl
	real localeps
	character*80 namedash,namevers,namegroup,nameacom,namebcom,
     +    namecopy,nametitl
	common /intblock/ lengdash,lengvers,lengroup,lengacom,lengbcom,
     +    lengcopy,lengtitl
	common /charblock/ namedash,namevers,namegroup,nameacom,namebcom,
     +    namecopy,nametitl

	error=0
	localeps=1.0e-4
	do 230 j=lnum,1,-1
	  do 220 i=1,npout
	    if (abs(phase(i,j)) .ge. localeps) goto 240
220	  continue
230	continue
240	lnum=j

	do 260 i=1,lnum+1
	  pstitle(i)=' '
	  if (i .eq. 1) then
	    pstitle(i)(9:9)='k'
	  elseif (i .lt. 10) then
	    pstitle(i)(4:9)='p(l=1)'
	    pstitle(i)(8:8)=char(48+i-2)
	  else
	    pstitle(i)(3:9)='p(l=10)'
	    pstitle(i)(7:7)=char(48+(i-2)/10)
	    pstitle(i)(8:8)=char(48+i-2-(i-2)/10*10)
	  endif
260	continue
	do 270 j=80,1,-1
	  if ((atname(j:j) .gt. char(32)) .or. (atname(j:j) .lt.
     +      char(0))) goto 280
270	continue
280	lename=j
	do 290 j=80,1,-1
	  if ((sbname(j:j) .gt. char(32)) .or. (sbname(j:j) .lt.
     +      char(0))) goto 300
290	continue
300	lensym=j
	if (lensym .gt. 9) lensym=9
	do 310 j=9,1,-1
	  i=j-9+lensym
	  if (i .gt. 0) then
	    sbname(j:j)=sbname(i:i)
	  else
	    sbname(j:j)=' '
	  endif
310	continue
	do 320 j=1,80
	  if ((ssname(j:j) .gt. char(32)) .or. (ssname(j:j) .lt.
     +      char(0))) goto 330
320	continue
330	ssname=ssname(j:j+1)

	if ((linitial .lt. 0) .and. (psfile .ne. ' ')) then
	  write(*,'(1x,2a,/)') 'phase shift data output in ',
     +      psfile(1:40)
	  datakind=711
	  begrow=12
	  linenum=npout+1
	  open(unit=10,file=psfile,status='unknown',iostat=error)
	  if (error .ne. 0) then
	    error=datakind*1000+203
	    goto 900
	  endif
	  write(10,'(1x,3i6,5x,a,6(/,1x,a))') datakind,begrow,linenum,
     +      'datakind beginning-row linenumbers',
     +    namedash(1:lengdash),
     +    namegroup(1:lengroup),nameacom(1:lengacom),
     +    namebcom(1:lengbcom),namecopy(1:lengcopy),
     +    namedash(1:lengdash)
	  write(10,'(/,1x,a9,14x,2a,/)') sbname(1:9),atname(1:lename),
     +      ' phase shift data'
	  write(10,'(1x,40a9)') (pstitle(i),i=1,lnum+1)
	  write(10,'(1x,i9,14x,a)') lnum,'lnum'
	  do 420 i=1,npout
	    write(10,'(1x,21f9.4)') radmat(i,1),(phase(i,j),j=1,lnum)
420	  continue
	  endfile(unit=10)
	  close(unit=10)
	endif

	if (linitial .gt. 0) then
	  nl=5
	else
	  nl=3
	endif
	rmtitle(1)='           k'
	rmtitle(2)='     R(li+1)'
	rmtitle(3)=' phase(li+1)'
	rmtitle(4)='     R(li-1)'
	rmtitle(5)=' phase(li-1)'

	if ((linitial .ge. 0) .and. (rmfile .ne. ' ')) then
	  write(*,'(1x,2a,/)') 'radial matrix data output in ',
     +      rmfile(1:40)
	  datakind=721
	  begrow=14
	  linenum=npout
	  open(unit=10,file=rmfile,status='unknown',iostat=error)
	  if (error .ne. 0) then
	    error=datakind*1000+203
	    goto 900
	  endif
	  write(10,'(1x,3i6,5x,a,6(/,1x,a))') datakind,begrow,linenum,
     +      'datakind beginning-row linenumbers',
     +    namedash(1:lengdash),
     +    namegroup(1:lengroup),nameacom(1:lengacom),
     +    namebcom(1:lengbcom),namecopy(1:lengcopy),
     +    namedash(1:lengdash)
	  write(10,'(/,1x,a9,a3,11x,2a,/)') sbname(1:9),ssname(1:2),
     +      atname(1:lename),' radial matrix data'
	  write(10,'(8x,a,f9.3,a,/)') 'binding energy =',
     +      -benergy/0.26248,' eV'
	  write(10,'(1x,a12,2(a12,a15))') (rmtitle(j),j=1,nl)
	  do 430 i=1,npout
	    write(10,'(1x,f12.4,2(g15.5,f12.4))') (radmat(i,j),j=1,nl)
430	  continue
	  endfile(unit=10)
	  close(unit=10)
	endif

	if ((linitial .ge. 0) .and. (eifile .ne. ' ')) then
	  write(*,'(1x,2a,/)') 'wave function data output in ',
     +      eifile(1:40)
	  datakind=831
	  begrow=14
	  linenum=npoint
	  open(unit=10,file=eifile,status='unknown',iostat=error)
	  if (error .ne. 0) then
	    error=datakind*1000+203
	    goto 900
	  endif
	  write(10,'(1x,3i6,5x,a,6(/,1x,a))') datakind,begrow,linenum,
     +      'datakind beginning-row linenumbers',
     +    namedash(1:lengdash),
     +    namegroup(1:lengroup),nameacom(1:lengacom),
     +    namebcom(1:lengbcom),namecopy(1:lengcopy),
     +    namedash(1:lengdash)
	  write(10,'(/,1x,a9,a3,11x,2a,/)') sbname(1:9),ssname(1:2),
     +      atname(1:lename),' radial wave function data'
	  write(10,'(8x,a,f9.3,a,/)') 'binding energy =',
     +      -benergy/0.26248,' eV'
	  write(10,'(1x,2a15)') 'r (angstrom)','u (arb unit)'
	  do 440 i=1,npoint
	    write(10,'(1x,2g15.5)') radist(i),eigwave(i)
440	  continue
	  endfile(unit=10)
	  close(unit=10)
	endif

900	return
      end
c     end of savmatrix

c     cubic spline interpolation
      subroutine spline(x,y,n,yp1,ypn,y2)
	implicit none
	integer n,nmax
	real yp1,ypn,x(n),y(n),y2(n)
	parameter (nmax=500)

	integer i,k
	real p,qn,sig,un,u(nmax)
	if (yp1 .gt. 0.99e30) then
	  y2(1)=0.0
	  u(1)=0.0
	else
	  y2(1)=-0.5
	  u(1)=(3.0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
	endif
	do 220 i=2,n-1
	  sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
	  p=sig*y2(i-1)+2
	  y2(i)=(sig-1.0)/p
	  u(i)=(6.0*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/
     +      (x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
220	continue
	if (ypn .gt. 0.99e30) then
	  qn=0.0
	  un=0.0
	else
	  qn=0.5
	  un=(3.0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
	endif
	y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.0)
	do 240 k=n-1,1,-1
	  y2(k)=y2(k)*y2(k+1)+u(k)
240	continue
	return
      end
c     end of spline

c     return a cubic spline interpolated value y
      subroutine splint(xa,ya,y2a,n,x,y)
	implicit none
	integer n
	real x,y,xa(n),y2a(n),ya(n)

	integer k,khi,klo
	real a,b,h
	klo=1
	khi=n
220	if (khi-klo .gt. 1) then
	  k=(khi+klo)/2
	  if (xa(k) .gt. x) then
	    khi=k
	  else
	    klo=k
	  endif
	  goto 220
	endif
	if (klo .gt. n-1) klo=n-1
	if (khi .lt. 2) khi=2
	if (khi .eq. klo) khi=klo+1
	h=xa(khi)-xa(klo)
	if (h .eq. 0.0) stop 'bad xa input in splint'
	a=(xa(khi)-x)/h
	b=(x-xa(klo))/h
	y=a*ya(klo)+b*ya(khi)+((a*a*a-a)*y2a(klo)+
     +    (b*b*b-b)*y2a(khi))*(h*h)/6.0
        return
      end
c     end of splint

c     get error message
      subroutine geterrormsg(error,lengmsg,errmsg)
	implicit none
	integer error,lengmsg
	character*80 errmsg

	integer k,errorcode,filecode,asczero

	filecode=error/1000
	errorcode=error-filecode*1000

	asczero=ichar('0')
	errmsg(1:6)='Error '
	errmsg(7:7)=char(asczero+errorcode/100)
	errmsg(8:8)=char(asczero+errorcode/10-errorcode/100*10)
	errmsg(9:9)=char(asczero+errorcode-errorcode/10*10)

	if ((errorcode/10 .ne. 20) .and. (errorcode/10 .ne. 70) .and.
     +    (errorcode/10 .ne. 71)) filecode=0

	if ((filecode .ge. 100) .and. (filecode .lt. 300)) then
	  errmsg(10:80)=', photoemission'
	elseif ((filecode .ge. 300) .and. (filecode .lt. 500)) then
	  errmsg(10:80)=', photoemission'
	elseif ((filecode .ge. 710) .and. (filecode .lt. 720)) then
	  errmsg(10:80)=', phase shift'
	elseif ((filecode .ge. 720) .and. (filecode .lt. 730)) then
	  errmsg(10:80)=', radial matrix'
	elseif ((filecode .ge. 730) .and. (filecode .lt. 740)) then
	  errmsg(10:80)=', report'
	elseif ((filecode .ge. 740) .and. (filecode .lt. 750)) then
	  errmsg(10:80)=', scat input'
	elseif ((filecode .ge. 750) .and. (filecode .lt. 760)) then
	  errmsg(10:80)=', batch input'
	elseif ((filecode .ge. 810) .and. (filecode .lt. 820)) then
	  errmsg(10:80)=', potential'
	elseif ((filecode .ge. 820) .and. (filecode .lt. 830)) then
	  errmsg(10:80)=', psrm input'
	elseif ((filecode .ge. 830) .and. (filecode .lt. 840)) then
	  errmsg(10:80)=', eigen wave'
	elseif ((filecode .ge. 910) .and. (filecode .lt. 920)) then
	  errmsg(10:80)=', hologram'
	elseif ((filecode .ge. 920) .and. (filecode .lt. 930)) then
	  errmsg(10:80)=', holo input'
	elseif ((filecode .ge. 930) .and. (filecode .lt. 940)) then
	  errmsg(10:80)=', scattering factor'
	elseif ((filecode .ge. 940) .and. (filecode .lt. 950)) then
	  errmsg(10:80)=', mean free path'
	elseif ((filecode .ge. 950) .and. (filecode .lt. 960)) then
	  errmsg(10:80)=', thermal vibration'
	else
	  errmsg(10:80)=','
	endif

	do 220 k=80,1,-1
	  if (errmsg(k:k) .gt. ' ') goto 230
220	continue
230	if (k .lt. 40) k=k+1

	if (errorcode .eq. 0) then
	  errmsg='Program terminated normally'
	elseif (errorcode .eq. 101) then
	  errmsg(k:80)=' out of memory'
	elseif (errorcode .eq. 102) then
	  errmsg(k:80)=' integer size too short'
	elseif (errorcode .eq. 103) then
	  errmsg(k:80)=' out of allocated memory'
	elseif (errorcode .eq. 201) then
	  errmsg(k:80)=' file not found'
	elseif (errorcode .eq. 202) then
	  errmsg(k:80)=' file open error'
	elseif (errorcode .eq. 203) then
	  errmsg(k:80)=' file creation error'
	elseif (errorcode .eq. 204) then
	  errmsg(k:80)=' data reading error'
	elseif (errorcode .eq. 205) then
	  errmsg(k:80)=' data writing error'
	elseif (errorcode .eq. 206) then
	  errmsg(k:80)=' end-of-file reading error'
	elseif (errorcode .eq. 207) then
	  errmsg(k:80)=' data format error'
	elseif (errorcode .eq. 208) then
	  errmsg(k:80)=' no data available'
	elseif (errorcode .eq. 209) then
	  errmsg(k:80)=' data type error'
	elseif (errorcode .eq. 601) then
	  errmsg(k:80)=' too few atoms (no or one)'
	elseif (errorcode .eq. 602) then
	  errmsg(k:80)=' too many atoms'
	elseif (errorcode .eq. 603) then
	  errmsg(k:80)=' too many kinds of atoms'
	elseif (errorcode .eq. 604) then
	  errmsg(k:80)=' emitter not found'
	elseif (errorcode .eq. 605) then
	  errmsg(k:80)=' too small distance between atoms'
	elseif (errorcode .eq. 611) then
	  errmsg(k:80)=' no available experimental data'
	elseif (errorcode .eq. 612) then
	  errmsg(k:80)=' experimental chi data too small'
	elseif (errorcode .eq. 613) then
	  errmsg(k:80)=' data files not match'
	elseif (errorcode .eq. 614) then
	  errmsg(k:80)=' too few input data points'
	elseif (errorcode .eq. 615) then
	  errmsg(k:80)=' too many input data points'
	elseif (errorcode .eq. 616) then
	  errmsg(k:80)=' too few output data points'
	elseif (errorcode .eq. 617) then
	  errmsg(k:80)=' too many output data points'
	elseif (errorcode .eq. 618) then
	  errmsg(k:80)=' no available hologram data'
	elseif (errorcode .eq. 621) then
	  errmsg(k:80)=' symmetry search error'
	elseif (errorcode .eq. 622) then
	  errmsg(k:80)=' symmetry order error'
	elseif (errorcode .eq. 623) then
	  errmsg(k:80)=' subshell name error'
	elseif (errorcode .eq. 624) then
	  errmsg(k:80)=' binding energy adjustment error'
	elseif (errorcode .eq. 625) then
	  errmsg(k:80)=' binding energy may be too small'
	elseif (errorcode .eq. 626) then
	  errmsg(k:80)=' binding energy may be too large'
	elseif (errorcode .eq. 627) then
	  errmsg(k:80)=' binding energy be too small'
	elseif (errorcode .eq. 628) then
	  errmsg(k:80)=' binding energy be too large'
	elseif (errorcode .eq. 631) then
	  errmsg(k:80)=' output file name not found'
	elseif (errorcode .eq. 632) then
	  errmsg(k:80)=' duplicate output file name'
	elseif (errorcode .eq. 633) then
	  errmsg(k:80)=' list file open error'
	elseif (errorcode .eq. 701) then
	  errmsg(k:80)=' too few data points'
	elseif (errorcode .eq. 702) then
	  errmsg(k:80)=' too many data points'
	elseif (errorcode .eq. 703) then
	  errmsg(k:80)=' too few curves'
	elseif (errorcode .eq. 704) then
	  errmsg(k:80)=' too many curves'
	elseif (errorcode .eq. 705) then
	  errmsg(k:80)=' no available data'
	elseif (errorcode .eq. 706) then
	  errmsg(k:80)=' data too small'
	elseif (errorcode .eq. 711) then
	  errmsg(k:80)=' energy order error'
	elseif (errorcode .eq. 712) then
	  errmsg(k:80)=' energy step too small'
	elseif (errorcode .eq. 713) then
	  errmsg(k:80)=' too few energy points'
	elseif (errorcode .eq. 714) then
	  errmsg(k:80)=' too many energy points'
	elseif (errorcode .eq. 715) then
	  errmsg(k:80)=' too few angle points'
	elseif (errorcode .eq. 716) then
	  errmsg(k:80)=' too many angle points'
	elseif (errorcode .eq. 901) then
	  errmsg(k:80)=' unknown error occured'
	else
	  errmsg(k:80)=' unknown error occured'
	endif

	do 250 k=80,1,-1
	  if (errmsg(k:k) .gt. ' ') goto 260
250	continue
260	if (k .lt. 60) k=k+1

	if (errorcode .ne. 0) errmsg(k:80)=', program terminated'

	do 280 k=80,1,-1
	  if (errmsg(k:k) .gt. ' ') goto 290
280	continue
290	if (k .gt. 76) k=76
	lengmsg=k

	return
      end
c     end of geterrormsg

c     get sizes of integer and floating point number
      subroutine getsize(sizechar,sizeint,sizereal,sizecomp)
	implicit none
	integer sizechar,sizeint,sizereal,sizecomp

	integer i,valint,fixnum
	real valreal
	complex valcomp
	character*80 valcha,valchb,valchc
	equivalence (valint,valcha),(valreal,valchb),(valcomp,valchc)

	sizechar=1

	fixnum=255
	do 220 i=1,40
	  valcha(i:i)=char(fixnum)
	  valchb(i:i)=char(fixnum)
	  valchc(i:i)=char(fixnum)
220	continue
	valint=1
	valreal=1.0
	valcomp=(1.0,1.0)
	do 230 i=40,1,-1
	  if (ichar(valcha(i:i)) .ne. fixnum) goto 240
230	continue
240	sizeint=i
	do 250 i=40,1,-1
	  if (ichar(valchb(i:i)) .ne. fixnum) goto 260
250	continue
260	sizereal=i
	do 270 i=40,1,-1
	  if (ichar(valchc(i:i)) .ne. fixnum) goto 280
270	continue
280	sizecomp=i

	fixnum=127
	do 320 i=1,40
	  valcha(i:i)=char(fixnum)
	  valchb(i:i)=char(fixnum)
	  valchc(i:i)=char(fixnum)
320	continue
	valint=1
	valreal=1.0
	valcomp=(1.0,1.0)
	do 330 i=40,1,-1
	  if (ichar(valcha(i:i)) .ne. fixnum) goto 340
330	continue
340	if (sizeint .lt. i) sizeint=i
	do 350 i=40,1,-1
	  if (ichar(valchb(i:i)) .ne. fixnum) goto 360
350	continue
360	if (sizereal .lt. i) sizereal=i
	do 370 i=40,1,-1
	  if (ichar(valchc(i:i)) .ne. fixnum) goto 380
370	continue
380	if (sizecomp .lt. i) sizecomp=i

	if (sizechar .lt. 1) sizechar=1
	if (sizeint .lt. 1) sizeint=4
	if (sizereal .lt. 1) sizereal=4
	if (sizecomp .lt. 1) sizecomp=8

	return
      end
c     end of getsize

c     names adjust
      subroutine nameadjust(asource,bsource,dest,lengdest)
	implicit none
	integer lengdest
	character*80 asource,bsource,dest

	integer i,j,k,m,abig,aend,bbig,bend
	character*80 temp

	temp=asource
	do 220 abig=1,80
	  if (temp(abig:abig) .ne. ' ') goto 230
220	continue
230	do 240 aend=80,1,-1
	  if (temp(aend:aend) .ne. ' ') goto 250
240	continue
250	temp=bsource
	do 260 bbig=1,80
	  if (temp(bbig:bbig) .ne. ' ') goto 270
260	continue
270	do 280 bend=80,1,-1
	  if (temp(bend:bend) .ne. ' ') goto 300
280	continue

300	k=0
	do 320 i=abig,aend
	  k=k+1
	  if (k .gt. 76) goto 400
	  temp(k:k)=asource(i:i)
320	continue
	if ((k .gt. 0) .and. (k .lt. 76) .and. (bbig .le. bend)) then
	  k=k+1
	  temp(k:k)=' '
	endif
	do 330 i=bbig,bend
	  k=k+1
	  if (k .gt. 76) goto 400
	  temp(k:k)=bsource(i:i)
330	continue

400	if (k .gt. 64) k=64
	m=(64-k)/2
	dest=' '
	do 420 i=1,k
	  j=i+m
	  dest(j:j)=temp(i:i)
420	continue
	lengdest=k+m

	return
      end
c     end of nameadjust

c     adjust a real variable
      subroutine realadjust(x,xmin,xmax)
	real x,xmin,xmax

	if (x .lt. xmin) then
	  x=xmin
	elseif (x .gt. xmax) then
	  x=xmax
	endif

	return
      end
c     end of realadjust

c     adjust a integer variable
      subroutine intadjust(x,xmin,xmax)
	integer x,xmin,xmax

	if (x .lt. xmin) then
	  x=xmin
	elseif (x .gt. xmax) then
	  x=xmax
	endif

	return
      end
c     end of intadjust

