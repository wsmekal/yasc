
#include "vector.h"


vector vadd( vector a, vector b )
{
	vector c;

	c.x = a.x + b.x;
	c.y = a.y + b.y;
	c.z = a.z + b.z;

	return( c );
}


vector vsub( vector a, vector b )
{
	vector c;

	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;

	return( c );
}


double vinprod( vector a, vector b )
{
	return( a.x*b.x+a.y*b.y+a.z*b.z );
}


double vnorm( vector a )
{
	return( sqrt(vinprod(a, a)) );
}


vector coord2vector( double x, double y, double z )
{
  vector a;
  
  a.x = x;
  a.y = y;
  a.z = z;  
  
  return a;
}


vector vmultconst( double c, vector a )
{
  vector b;
  
  b.x = c*a.x;
  b.y = c*a.y;
  b.z = c*a.z;

  return b;
}


void vprint( char* name, vector a )
{
  printf( "%s(%lf, %lf, %lf), |%s|=%lf\n", name, a.x, a.y, a.z, name, vnorm(a) );
}

