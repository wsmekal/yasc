
#include "vector.h"
#include "crystruc.h"

#include <stdlib.h>
#include <stdio.h>


vector_item* crystruc( char* surface, vector m, double a, double radius )
{
  int n, step, k;
  vector e, f, g;
  vector_item* head = NULL;
  vector_item* curr = head;
  
  if( !strncmp("sc100", surface, 5) ) {
    e = coord2vector( a, 0.0, 0.0 );
    f = coord2vector( 0.0, a, 0.0 );
    g = coord2vector( 0.0, 0.0, -a );
  } else if( !strncmp("sc110", surface, 5) ) {
    e = coord2vector( sqrt(2.0)*a, 0.0, 0.0 );
    f = coord2vector( 0.0, a, 0.0 );
    g = coord2vector( a/sqrt(2.0), 0.0, -a/sqrt(2.0) );
  } else if( !strncmp("sc111", surface, 5) ) {
    e = coord2vector( sqrt(3.0/2.0)*a, -a/sqrt(2.0), 0.0 );
    f = coord2vector( sqrt(3.0/2.0)*a, a/sqrt(2.0), 0.0 );
    g = coord2vector( sqrt(2.0/3.0)*a, 0.0, -a/sqrt(3.0) );
  } else if( !strncmp("bcc100", surface, 6) ) {
    e = coord2vector( a, 0.0, 0.0 );
    f = coord2vector( 0.0, a, 0.0 );
    g = coord2vector( a/2.0, a/2.0, -a/2.0 );
  } else if( !strncmp("bcc110", surface, 6) ) {
    e = coord2vector( a/sqrt(2.0), -a/2.0, 0.0 );
    f = coord2vector( a/sqrt(2.0), a/2.0, 0.0 );
    g = coord2vector( a/sqrt(2.0), 0, -a/sqrt(2.0) );
  } else if( !strncmp("bcc111", surface, 6) ) {
    e = coord2vector( a*sqrt(3.0/2.0), -a/sqrt(2.0), 0.0 );
    f = coord2vector( a*sqrt(3.0/2.0), a/sqrt(2.0), 0.0 );
    g = coord2vector( a*sqrt(2.0/3.0), 0, -a/2.0/sqrt(3.0) );
  } else if( !strncmp("fcc100", surface, 6) ) {
    e = coord2vector( a/2.0, -a/2.0, 0.0 );
    f = coord2vector( a/2.0, a/2.0, 0.0 );
    g = coord2vector( a/2.0, 0.0, -a/2.0 );
  } else if( !strncmp("fcc110", surface, 6) ) {
    e = coord2vector( a/sqrt(2.0), 0.0, 0.0 );
    f = coord2vector( 0.0, a, 0.0 );
    g = coord2vector( a/2.0/sqrt(2.0), a/2.0, -a/2.0/sqrt(2.0) );
  } else if( !strncmp("fcc111", surface, 6) ) {
    e = coord2vector( a*sqrt(3.0/2.0)/2.0, -a/sqrt(2.0)/2.0, 0.0 );
    f = coord2vector( a*sqrt(3.0/2.0)/2.0, a/sqrt(2.0)/2.0, 0.0 );
    g = coord2vector( a/sqrt(6.0), 0.0, -a/sqrt(3.0) );
  } else {
    fprintf( stderr, "Error: \"%s\" is an unknown surface!\n", surface );
    exit( 1 );
  }

  for( n=0, step=1, k=0;; ) {
    double p, q, delta, t1, t2;
    int t;
    vector tmp;
    
    tmp=vsub(vadd(vmultconst(n, e), vmultconst(k, g)), m);
    q=(vinprod(tmp, tmp)-pow(radius, 2.0))/vinprod(f, f);
    p=2.0*vinprod(f, tmp)/vinprod(f, f);
    
    delta=pow(p/2.0, 2.0)-q;
    if(delta>=0) {
      t1=-p/2.0-sqrt(delta);
      t2=-p/2.0+sqrt(delta);

      for( t=ceil(t1); t<=t2; t++ ) {
        curr=addatom( curr );
        curr->atom=vadd(vadd(vmultconst(n, e), vmultconst(t, f)), vmultconst(k, g));
        if(!head)
          head=curr;
      }
      
      n+=step;  /* next row */
    } else {
      if( n==0 )
        break;
      if( step==1 ) {
        step=-1;
        n=-1;
      } else {
        n=0;
        step=1;
        k++;
      }        
    }    
  }
  
  return head;
}


vector_item* addatom( vector_item* item )
{
  vector_item* new_item = (vector_item*)malloc( sizeof(vector_item) );
  memset( new_item, 0, sizeof(vector_item) );
  
  if( item != NULL )
    item->next = new_item;
  return new_item;
}


void freelist( vector_item* head )
{
  vector_item* item;
  
  while( head ) {
    item=head->next;
    free( head );
    head=item;
  }    
}
