/*!
 * @file
 * @brief main() function for crystruc program
 *   
 * The main function only reads in the arguments calls the crystruc function
 * which calculates the atom positions and writes the data to stdout.
 * Usage: crystruc surface x y z a
 *        surface - is sc100, sc110, sc111, bcc100, bcc110, bcc111, fcc100,
 *                  fcc110, fcc111
 *        x y z - position of the first atom
 *        a - layer constant
 *        r - radius of the cluster sphere
 *        x,y,z,a,r have to be in the same dimension
 * 
 * Changes: 20070108 - initial release
 */

#include "crystruc.h"
#include "vector.h"

#include <stdio.h>
#include <stdlib.h>

/*!
 * The main function only reads in the arguments calls the crystruc function
 * which calculates the atom positions and writes the data to stdout.
 *
 * @author Werner Smekal
 * @date 2007.01.08
 * @param argc - number of arguments (including the filename)
 * @param argv - array of char arrays containing the arguments
 */
int main( int argc, char* argv[] )
{
	double a, r;
	vector m;
  char surface[256];
  vector_item* list;  /*!< A linked list of all atom positions. */  
  vector_item* curr;

	/* check arguments */
	if( argc != 7 ) {
		fprintf( stderr, "Usage: %s surface x y z a r\n", argv[0] );
    fprintf( stderr, "       surface: sc100, sc110, sc111 (similar for bcc, fcc)\n" );
    fprintf( stderr, "       x y z: position of first atom\n" );
    fprintf( stderr, "       a: layer constant\n" );
    fprintf( stderr, "       r: radius of cluster sphere\n" );
		exit( 1 );
	}

  /* copy arguments */
  strncpy( surface, argv[1], 256 );
	m.x = atof( argv[2] );
	m.y = atof( argv[3] );
	m.z = atof( argv[4] );
	a = atof( argv[5] );
  r = atof( argv[6] );

	/* call crystruc subroutine */ 
  list =  crystruc( surface, m, a, r );
  
  curr = list;
  while( curr ) {
    vprint( "atom", curr->atom );
    curr=curr->next;
  }
 
  /* free atom list */
  freelist( list );
}
