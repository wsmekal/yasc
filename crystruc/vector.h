
#ifndef __VECTOR_H
#define __VECTOR_H

/* declaration of vector */
typedef struct {
	double x;
	double y;
	double z;
} vector;

/* vector operations */
vector coord2vector( double x, double y, double z );
vector vadd( vector a, vector b );
vector vsub( vector a, vector b );
vector vmultconst( double c, vector a );
double vinprod( vector a, vector b );
double vnorm( vector a );
void vprint( char* name, vector a );

#endif /* __VECTOR_H */
