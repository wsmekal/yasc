
#ifndef __CRYSTRUC_H
#define __CRYSTRUC_H

#include "vector.h"

struct vec_el {
  vector atom;
  struct vec_el* next;
};
typedef struct vec_el vector_item;

void freelist( vector_item* head );
vector_item* addatom( vector_item* item );
vector_item* crystruc( char* surface, vector m, double a, double radius );


#endif /* __CRYSTRUC_H */
