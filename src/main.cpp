
#include "yascmain.h"
#include <cstdarg>

// all the data is globally stored here
Cdata data;

// Clebsch Gordan Coefficients
complex<yreal> *Y;

// if quiet==true no output
bool quiet;

// output file
ofstream output;

/*--------------------------------------------------------------------------*\
 *  void Log_Verbose( char *fmt, ... )
 *
 *  Print verbose debug message to stderr (printf style).
\*--------------------------------------------------------------------------*/
void Log_Verbose( char *fmt, ... )
{
#ifdef _DEBUG_VERBOSE
  va_list args;
  va_start( args, fmt );
  fprintf( stderr, "Verbose: " );
  vfprintf( stderr, fmt, args );
  fprintf( stderr, "\n" );
  va_end( args );
  fflush( stderr );
#endif
}


/*--------------------------------------------------------------------------*\
 *  void Log_Debug( char *fmt, ... )
 *
 *  Print debug message to stderr (printf style).
\*--------------------------------------------------------------------------*/
void Log_Debug( char *fmt, ... )
{
#ifdef _DEBUG
  va_list args;
  va_start( args, fmt );
  fprintf( stderr, "Debug: " );
  vfprintf( stderr, fmt, args );
  fprintf( stderr, "\n" );
  va_end( args );
  fflush( stderr );
#endif
}


int main(int argc, char** argv)
{
  // set options
	quiet=false;
	data.filename_in = "input.dat";

  // get options from user
	int option_char;
	while ((option_char = getopt(argc, argv, "i:?q")) != EOF)
		switch(option_char) {
		case 'i':
			data.filename_in = optarg;
			break;
		case '?':
			cout << "Usage: " << argv[0] << " -i<input filename> -q" << endl;
			exit(1);
			break;
		case 'q':
			quiet = true;
			break;
		}


	// read in data
	if( !quiet )
		cout << "Reading data ..." << endl;
  data.read();
  
  Cwatch watch;
  calculate();
  watch.stop();
  watch.print( "Duration of calculation" );
  
  return 0;
}

void calculate( void )
{
  yreal theta, phi, k;
	char cr=13;

  // get memory for spherical harmonics
  Y = new complex<yreal>[(data.l_max)*(data.l_max)];
	if(Y==0)
		yasc_error("Couldn't allocate memory for Y[]!");

	if (!quiet)
		cout << "Initialising scatterers ..." << endl;
	for(int i=0; i<data.natom; i++)
		data.atoms[i]->init();

	if (!quiet)
		cout << "Starting calculations ..." << endl << flush;
  if(data.rotation==0) {
		yreal x,y,z, xp,yp,zp;
		yreal theta_e, phi_e, theta_anal, phi_anal, theta_analp, phi_analp;
		yreal intensity0, intensity;
		complex<yreal> psi, psi0, psi1;
		if (!quiet)
			cout << "Rotating analyzer ..." << endl << flush;

		theta_e = data.theta_e;
		phi_e = data.phi_e;

		// change coordinate system
		for(int i=0; i<data.natom; i++)
			data.atoms[i]->change_coord_system(theta_e, phi_e);

		for(int i=0; i<data.natom; i++)
			data.atoms[i]->calculate_B();

		for(int i=0; i<data.natom; i++)
			data.atoms[i]->calculate_a1();

    while(data.next_scan(theta, phi, k)) {
			intensity = 0.0;
			intensity0 = 0.0;

			theta_anal = data.refract(theta*pi/180.0);
			phi_anal = phi*pi/180.0;
			check_angle(&theta_anal, &phi_anal);

			x=sin(theta_anal)*cos(phi_anal);
			y=sin(theta_anal)*sin(phi_anal);
			z=cos(theta_anal);
			xp =  cos(theta_e)*cos(phi_e)*x + cos(theta_e)*sin(phi_e)*y - sin(theta_e)*z;
			yp = -sin(phi_e)*x              + cos(phi_e)*y;
			zp =  sin(theta_e)*cos(phi_e)*x + sin(theta_e)*sin(phi_e)*y + cos(theta_e)*z;

			phi_analp = atan2(yp, xp);
			theta_analp = acos(zp);

			if(!quiet)
				cout << cr << "(" << theta << "," << phi << ")" << flush;

			check_angle(&theta_analp, &phi_analp);

			//cout << "t: " << theta << " p: " << phi;
			//cout << " | te: " << theta_e*180/pi << " pe: " << phi_e*180/pi;
			//cout << " | ta: " << theta_analp*180/pi << " pa: " << phi_analp*180/pi << endl;

			for(int n=0; n<data.emitters.size(); n++) {
				// calculate wave of zeroth order
				psi0 = data.atoms[data.emitters[n]]->psi0( theta_analp, phi_analp, theta_anal );

				// calculate wave of first order
				psi1=(0,0);
        for( int i=0; i<data.natom; i++ )
					psi1 += data.atoms[i]->psi1( theta_analp, phi_analp, theta_anal, n );

				psi=psi0+psi1;

				intensity0 += real(psi0)*real(psi0)+imag(psi0)*imag(psi0);
				intensity += real(psi)*real(psi)+imag(psi)*imag(psi);
			}

      // for a holographic scan we add a newline if the polarangle changed
      // for gnuplot to be able to make holographic plots
      if( data.newline )
        output << endl;
			output << theta << " " << phi << " " << intensity0 << " " << intensity << endl;
		}
  } else {
		if (!quiet)
			cout << "Rotating sample ..." << endl << flush;

    while(data.next_scan(theta, phi, k)) {
			yreal intensity0=0.0, intensity=0.0;
			complex<yreal> psi, psi0, psi1;

			if(!quiet)
				cout << cr << "(" << theta << "," << phi << ")" << flush;

			// change coordinate system
			for(int i=0; i<data.natom; i++)
				data.atoms[i]->change_coord_system( pi/2.0f-data.theta_anal+theta*pi/180, phi*pi/180 );

			for(int i=0; i<data.natom; i++) {
				data.atoms[i]->calculate_B();
				data.atoms[i]->calculate_a1();
			}

			for(int n=0; n<data.emitters.size(); n++) {
				// calculate wave of zeroth order
				psi0 = data.atoms[data.emitters[n]]->psi0(data.refract(pi/2.0f-data.theta_anal), pi, data.refract(theta*pi/180));

				// calculate wave of first order
				psi1=(0,0);
        for(int i=0; i<data.natom; i++)
					psi1 += data.atoms[i]->psi1(data.refract(pi/2.0f-data.theta_anal), pi, data.refract(theta*pi/180), n);

				psi=psi0+psi1;

				intensity0 += real(psi0)*real(psi0)+imag(psi0)*imag(psi0);
				intensity += real(psi)*real(psi)+imag(psi)*imag(psi);
			}

      // for a holographic scan we add a newline if the polarangle changed
      // for gnuplot to be able to make holographic plots
      if( data.newline )
        output << endl;
			output << theta << " " << phi << " " << intensity0 << " " << intensity << endl;
		}
  }

	if (!quiet)
		cout << endl;

	if (!quiet)
		cout << "Output written to >" << data.filename_out.c_str() << "<." << endl;

  delete[] Y;
}
