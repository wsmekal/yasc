
#include <cstdlib>
#include <cmath>
#include <ctime>

#include "greens.h"
#include "greens_ylm.hpp"
#include "yascmain.h"

#include "gsl/gsl_sf_legendre.h"
#include "gsl/gsl_sf_bessel.h"
#include "gsl/gsl_sf_coupling.h"

#define TEST_EPS 1e-5
#define J_MAX 12

// set tests true/false
const bool test_3j_speed=true;
const bool test_3j_values=true;
const bool test_spharm=true;
const bool test_pow=true;
const bool test_hankel=true;

bool compare( complex<yreal> c1, complex<yreal> c2, complex<yreal> c3, double rel_err )
{
	return fabs((real(c1)-real(c2))/real(c1))<rel_err &&
  				fabs((imag(c1)-imag(c2))/imag(c1))<rel_err &&
					fabs((real(c1)-real(c3))/real(c1))<rel_err &&
  				fabs((imag(c1)-imag(c3))/imag(c1))<rel_err;
}  

bool compare( complex<yreal> c1, complex<yreal> c2, double rel_err )
{
	return fabs((real(c1)-real(c2))/real(c1))<rel_err &&
  				fabs((imag(c1)-imag(c2))/imag(c1))<rel_err;
}  

int main(void)
{
  int j1, j2, j3, m1, m2, m3;
  clock_t watch;

  printf("Dummy calculation, to get computer running ... " ); fflush( stdout );
  double g3j_n;
  for( j1=0; j1<=J_MAX; j1++ )
    for( j2=0; j2<=J_MAX; j2++ )
      for( j3=0; j3<=J_MAX; j3++ )
        for( m1=-j1; m1<=j1; m1++ )
          for( m2=-j2; m2<=j2; m2++ )
            for( m3=-j3; m3<=j3; m3++ )
              g3j_n = greens_w3j( j1, j2, j3, m1, m2, m3 );
  puts( "Done!" );

   /*********************************************************************************
   **  Test 3j symbols functions
   *********************************************************************************/
  
  // test speed
  if( test_3j_speed ) {
    spec_func::init( J_MAX );
    printf("Testing speed of greens lib 3j function ... " ); fflush( stdout );
    watch=clock(); 
    double g3j_n;
    for( j1=0; j1<=J_MAX; j1++ )
      for( j2=0; j2<=J_MAX; j2++ )
        for( j3=0; j3<=J_MAX; j3++ )
          for( m1=-j1; m1<=j1; m1++ )
            for( m2=-j2; m2<=j2; m2++ )
              for( m3=-j3; m3<=j3; m3++ )
                g3j_n = greens_w3j( j1, j2, j3, m1, m2, m3 );
    printf("%f sec (j_max=%d).\n", (float)(clock()-watch)/CLOCKS_PER_SEC, J_MAX );
  
    printf("Testing speed of Ricardo's 3j function ... " ); fflush( stdout );
    watch=clock();
    double r3j_n;
    for( j1=0; j1<=J_MAX; j1++ )
      for( j2=0; j2<=J_MAX; j2++ )
        for( j3=0; j3<=J_MAX; j3++ )
          for( m1=-j1; m1<=j1; m1++ )
            for( m2=-j2; m2<=j2; m2++ )
              for( m3=-j3; m3<=j3; m3++ )
                if( (m1==0.0) && (m2==0.0) && (m3==0.0) )
                  r3j_n = spec_func::C3j_zero( j1, j2, j3 );
                else
                  r3j_n = spec_func::C3j( j1, j2, j3, m1, m2, m3 );
    printf("%f sec (j_max=%d).\n", (float)(clock()-watch)/CLOCKS_PER_SEC, J_MAX );

    printf("Testing speed of gsl 3j function ... " ); fflush( stdout );
    watch=clock();
    double gsl_3j_n;
    for( j1=0; j1<=J_MAX; j1++ )
      for( j2=0; j2<=J_MAX; j2++ )
        for( j3=0; j3<=J_MAX; j3++ )
          for( m1=-j1; m1<=j1; m1++ )
            for( m2=-j2; m2<=j2; m2++ )
              for( m3=-j3; m3<=j3; m3++ )
                  gsl_3j_n = gsl_sf_coupling_3j( 2*j1, 2*j2, 2*j3, 2*m1, 2*m2, 2*m3 );
    printf("%f sec (j_max=%d).\n", (float)(clock()-watch)/CLOCKS_PER_SEC, J_MAX );
  }
            
  if( test_3j_values ) {
    printf("Testing values of all 3j function ... " ); fflush( stdout );
    for( j1=0; j1<=J_MAX; j1++ )
      for( j2=0; j2<=J_MAX; j2++ )
        for( j3=0; j3<=J_MAX; j3++ )
          for( m1=-j1; m1<=j1; m1++ )
            for( m2=-j2; m2<=j2; m2++ )
              for( m3=-j3; m3<=j3; m3++ ) {
                double g3j_n = greens_w3j( j1, j2, j3, m1, m2, m3 );
                double c3j_n;
                if( (m1==0) && (m2==0) && (m3==0) )
                  c3j_n = spec_func::C3j_zero( j1, j2, j3 );
                else
                  c3j_n = spec_func::C3j( j1, j2, j3, m1, m2, m3 );
                double gsl_3j_n = gsl_sf_coupling_3j( 2*j1, 2*j2, 2*j3, 2*m1, 2*m2, 2*m3 );
                if( (fabs(g3j_n-c3j_n)>TEST_EPS) || (fabs(g3j_n-gsl_3j_n)>TEST_EPS) )
                  printf( " *** DETECTED DIFFERENCE: g3j=%e, c3j=%e, gsl_3j=%e ***\n", g3j_n, c3j_n, gsl_3j_n );
              }
    puts( " done" );
    spec_func::free_C3j();
  }  

  if( test_spharm ) {
    printf("Testing spherical harmonics function ... " ); fflush( stdout );
    watch=clock();
    for( j1=0; j1<=J_MAX; j1++ )
      for( m1=-j1; m1<=j1; m1++ )
        for( float theta=0.0f; theta<=M_PI; theta+=0.1f )
         for( float phi=0.0f; phi<=2.0*M_PI; phi+=0.1f )
          complex<yreal> res = spec_func::yspharm(j1, m1, theta, phi);
    printf("%f sec.\n", (float)(clock()-watch)/CLOCKS_PER_SEC );

    printf("Testing spherical harmonics (green) function ... " ); fflush( stdout );
    watch=clock();
    for( j1=0; j1<=J_MAX; j1++ )
      for( m1=-j1; m1<=j1; m1++ )
        for( float theta=0.0f; theta<=M_PI; theta+=0.1f )
         for( float phi=0.0f; phi<=2.0*M_PI; phi+=0.1f )
          dcomplex res = greens_Ylm(j1, m1, theta, phi);
    printf("%f sec.\n", (float)(clock()-watch)/CLOCKS_PER_SEC );

    printf("Testing spherical harmonics (gsl) function ... " ); fflush( stdout );
    watch=clock();
    for( j1=0; j1<=J_MAX; j1++ )
      for( m1=-j1; m1<=j1; m1++ )
        for( float theta=0.0f; theta<=M_PI; theta+=0.1f )
         for( float phi=0.0f; phi<=2.0*M_PI; phi+=0.1f )
          complex<yreal> res = spec_func::gsl_spharm(j1, m1, theta, phi);
    printf("%f sec.\n", (float)(clock()-watch)/CLOCKS_PER_SEC );

    printf("Testing spherical harmonics values ... " ); fflush( stdout );
    watch=clock();
    for( j1=0; j1<=J_MAX; j1++ )
      for( m1=-j1; m1<=j1; m1++ )
        for( float theta=0.0f; theta<=M_PI; theta+=0.1f )
         for( float phi=0.0f; phi<=2.0*M_PI; phi+=0.1f ) {
          complex<yreal> res_gsl = spec_func::gsl_spharm(j1, m1, theta, phi);
          complex<yreal> res_own = spec_func::yspharm(j1, m1, theta, phi);
          dcomplex res_green = greens_Ylm(j1, m1, theta, phi);

          if( (fabs(real(res_gsl)-real(res_own))>TEST_EPS) || (fabs(imag(res_gsl)-imag(res_own))>TEST_EPS) ||
              (fabs(Re(res_green)-real(res_own))>TEST_EPS) || (fabs(Im(res_green)-imag(res_own))>TEST_EPS) )
            printf( " *** DETECTED DIFFERENCE: l=%d, m=%d, sph_gsl=(%e,%e), sph_own=(%e,%e), sph_gre=(%e,%e) ***\n",
                    j1, m1,
                    real(res_gsl), imag(res_gsl),
                    real(res_own), imag(res_own),
                    Re(res_green), Im(res_green) );
        }
    printf("%f sec.\n", (float)(clock()-watch)/CLOCKS_PER_SEC );
  }
  
  if( test_pow ) {
    printf("Testing m1_pow(-1,n) ... " ); fflush( stdout );
    watch=clock();
    int temp;
    for( int i=0; i<=1000; i++ )
      for( int m=-5000; m<=5000; m++ )
        temp = m1_pow( m );
    printf("%f sec.\n", (float)(clock()-watch)/CLOCKS_PER_SEC );
    
    printf("Testing pow(-1,n) ... " ); fflush( stdout );
    watch=clock();
    double ddd;
    for( int i=0; i<=1000; i++ )
      for( int m=-5000; m<=5000; m++ )
        ddd = pow(-1.0,(double)m);
    printf("%f sec.\n", (float)(clock()-watch)/CLOCKS_PER_SEC );

    printf("Testing pow values ... " ); fflush( stdout );
    for( int m=-5000; m<=5000; m++ )
      if( m1_pow(m)!=pow(-1.0,(double)m) )
        printf("error\n");
    puts( "Done!" );
  }

  if( test_hankel ) {
    printf("*** Testing hankel functions ***\n" ); fflush( stdout );

    printf("Testing hankel (yasc) function ... " ); fflush( stdout );
    watch=clock();
    for( int l=0; l<=J_MAX; l++ )
      for( float rho=0.01f; rho<=10.0f; rho+=0.001f )
        complex<yreal> res = spec_func::yhankel( l, rho );
    printf("%f sec.\n", (float)(clock()-watch)/CLOCKS_PER_SEC );

    printf("Testing hankel (gsl) function ... " ); fflush( stdout );
    watch=clock();
    for( int l=0; l<=J_MAX; l++ )
      for( float rho=0.01f; rho<=10.0f; rho+=0.001f )
        complex<yreal> res = spec_func::gsl_hankel( l, rho );
    printf("%f sec.\n", (float)(clock()-watch)/CLOCKS_PER_SEC );
      
		float rel_error=0.001;
    printf("Testing hankel function values (rel_error=%f) ... ", rel_error ); fflush( stdout );
    watch=clock();
    for( int l=0; l<=J_MAX; l++ )
      for( float rho=0.01f; rho<=10.0f; rho+=0.001f ) {
        complex<yreal> res_y = spec_func::yhankel( l, rho );
        complex<yreal> res_gsl = spec_func::gsl_hankel( l, rho );
        if( !compare( res_y, res_gsl, rel_error ) ) {
					printf( "\n *** DETECTED DIFFERENCE: l=%d, rho=%e, hankel(yasc)=(%e,%e),"
					        " hankel(gsl)=(%e,%e) ***\n",
									l, rho,
									real(res_y), imag(res_y),
									real(res_gsl), imag(res_gsl) );
					exit( -1 );
				}
      }
    printf("Done! (%f sec.)\n", (float)(clock()-watch)/CLOCKS_PER_SEC );
  }
  
  return 0;
}

