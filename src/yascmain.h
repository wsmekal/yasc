
#include <ctime>
#include <cmath>
#include <getopt.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <complex>
#include <valarray>
#include <numeric>
#include <vector>
#include <string>

#ifdef HAVE_GSL
  #include "gsl/gsl_math.h"
#endif

#ifdef HAVE_GREENS
  #include "greens.h"
  #include "greens_ylm.hpp"
#endif


using namespace std;

typedef double yreal;

const yreal pi=M_PI;

enum ScanType { polarscan, azimuthalscan, holoscan, energyscan };

// some extern declarations
extern complex<yreal> i_matrix[5];
extern complex<yreal> im_matrix[5];
extern complex<yreal> *Y;

/* define if you want debug output */
#define _DEBUG
#define _DEBUG_VERBOSE
void Log_Verbose( char *fmt, ... );
void Log_Debug( char *fmt, ... );

inline void yasc_error(char *message)
{
  cout << message << endl;
  exit(1);
}

inline void check_angle(yreal* theta, yreal* phi)
{
	if((*theta)<0.0) {
		(*theta) *= -1.0;
		(*phi) += pi;
	}
	if((*phi)>=(2.0*pi))
		(*phi) -= 2.0*pi;
	else if((*phi)<0.0)
		(*phi) += 2.0*pi;
}

inline complex<yreal> i_pow(int n)
{
  return n<0 ? i_matrix[4-abs(n)%4] : i_matrix[n%4];
}

inline complex<yreal> im_pow(int n)
{
  return n<0 ? im_matrix[4-abs(n)%4] : im_matrix[n%4];
}

/*!
 * This function calculates the powers of -1. It's about 10
 * times faster than pow(-1.0, m).
 *
 * @author Werner Smekal
 * @date 20060101
 * @param m : power of -1
 */
inline int m1_pow( int m )
{
  return m&0x1 ? -1 : 1 ;
}

void calculate( void );

class Cwatch
{
public :
  Cwatch() { start(); }
  void start() { m_watch=clock(); }
  void stop() { m_stop=clock(); }
  void print( string message )
  {
    cout << message <<  ": " << (float)(m_stop-m_watch)/CLOCKS_PER_SEC << " sec" << endl;
  }
    
private:
  clock_t m_watch;
  clock_t m_stop;
};

// class Catom
class Catom
{
	// information about atom
private:
  valarray<yreal> r;
	valarray<yreal> rp;  // p...prime
  bool emitter;
	int id;
	complex<yreal>** hankel;
	yreal* r_delta_norm;
	yreal* decay;
  int kind;
 
public:
  Catom(int id, yreal x, yreal y, yreal z, int ikind, bool iemitter, int li);
  ~Catom(void);
  valarray<yreal>& get_r(void);
	void change_coord_system(yreal theta, yreal phi);
	void init(void);
	void free(void);

	// variables and functions if the atom is emitter
private:
  int l_init;
  complex<yreal> *a0;
public:
  void calculate_a0(void);
  complex<yreal> get_a0(int l, int m);
  complex<yreal> psi0(yreal theta, yreal phi, yreal theta_decay);

	// variables and functions for the scattering part
private:
  complex<yreal> ***B;
  complex<yreal> **a1;

public:
  void calculate_a1(void);
	void calculate_B(void);
  complex<yreal> psi1(yreal theta, yreal phi, yreal theta_decay, int emitter);
};


class Cdata {
public:
  Cdata();
  ~Cdata();
  bool add_atom( yreal x, yreal y, yreal z, int kind, bool emitter );

  vector<Catom*> atoms;
	vector<int> emitters;
  complex<yreal>** T_matrix;
  yreal theta_start, theta_end, theta_step, theta;
  yreal phi_start, phi_end, phi_step, phi;
  yreal energy_start, energy_end, energy_step;
	yreal k_vec, ekin;  // k is the wave vector [1/A] and ekin is the corresponding kinetic energy in eV
  ScanType SType;
	bool first_run;
  bool newline;

	// photo excitation
  complex<yreal> Rm1, Rp1;
  int natom, num_kinds;
	int l_max, l_init, rotation, l_num;
  yreal theta_e, phi_e;
	yreal theta_analp, phi_analp;
	yreal theta_anal, phi_anal;

	//
	yreal V0, z0;
	yreal lambda;  //!< inelastic mean free path in Angstrom

	string filename_out; //!< name of the output file
	string filename_in; //!< name of the input file

  complex<yreal> T(int kind, int l);
  bool next_scan(yreal& _theta, yreal& _phi, yreal& _k);
  void read();
	yreal refract(yreal theta);
	yreal distance2edge(yreal z, yreal theta);
	yreal inel_decay(yreal dist);
};


namespace spec_func
{
  // Gaunt integral
  void init( int lm );
  void init_C3j( void );
  void free_C3j( void );
  yreal Gaunt(int l1, int l2, int l3, int m1, int m2, int m3);
  double C3j(int i1, int i2, int i3, int m1, int m2, int m3);
  double C3j_zero(int i1, int i2, int i3);

  // hankel function and spherical harmonics
  complex<yreal> yspharm( int l, int m, yreal theta, yreal phi );
  complex<yreal> yhankel( int n, yreal x );

#ifdef HAVE_GSL  
  complex<yreal> gsl_spharm( int l, int m, yreal theta, yreal phi );
  complex<yreal> gsl_hankel( int n, yreal x );
#endif

#ifdef USE_GSL   // use gnu scientific library
  inline complex<yreal> spharm( int l, int m, yreal theta, yreal phi )
  { return gsl_spharm( l, m, theta, phi ); }

  inline complex<yreal> hankel( int n, yreal x )
  { return gsl_hankel( n, x ); }

#elif defined( USE_GREENS )   // use greens library
  inline complex<yreal> spharm( int l, int m, yreal theta, yreal phi )
  { dcomplex res = greens_Ylm( l, m, theta, phi );
    return complex<yreal>(Re(res), Im(res)); }

  inline complex<yreal> hankel( int n, yreal x )
  { return yhankel( n, x ); }

#else   // use own functions
  inline complex<yreal> spharm( int l, int m, yreal theta, yreal phi )
  { return yspharm( l, m, theta, phi ); }

  inline complex<yreal> hankel( int n, yreal x )
  { return yhankel( n, x ); }
#endif
};

extern Cdata data;   // declared in main.cpp
extern bool quiet;
extern ofstream output;
