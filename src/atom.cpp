
#include "yascmain.h"

using namespace std;

complex<yreal> i_matrix[5] = {
	complex<yreal>(1,0),
	complex<yreal>(0,1),
	complex<yreal>(-1,0),
	complex<yreal>(0,-1),
	complex<yreal>(1,0)
};

complex<yreal> im_matrix[5] = {
	complex<yreal>(1,0),
	complex<yreal>(0,-1),
	complex<yreal>(-1,0),
	complex<yreal>(0,1),
	complex<yreal>(1,0)
};

/*!
 * In the constructor of the class atom, initial variables of this
 * class are set. If the atom is defined as an emitter, also the 
 * zeroth order wave factors are calculated.
 *
 * @author Werner Smekal
 * @date 2000
 *
 * @param iid : 
 * @param x : x position of atom
 * @param y : y position of atom
 * @param z : z position of atom
 * @param ikind : which kind of atom (defined by phases)
 * @param iemitter : true, if atom is an emitter
 * @param li : 
 */
Catom::Catom(int iid, yreal x, yreal y, yreal z, int ikind, bool iemitter, int li):
	id(iid), kind(ikind), emitter(iemitter), l_init(li)
{
  r.resize(3, 0.0);
  r[0] = x;
  r[1] = y;
  r[2] = z;

	rp.resize(3, 0.0);
	rp[0] = x;
  rp[1] = y;
  rp[2] = z;

  // TODO: do we need a0 here, if not emitter
  a0 = new complex<yreal>[(l_init+2)*(l_init+2)];
	if(a0==0)
		yasc_error("Unable to allocate memory for a0!");

	if(emitter)
		calculate_a0();
}


/*!
 * Clean up memory in deconstructor.
 *
 * @author Werner Smekal
 * @date 2000
 */
Catom::~Catom()
{
  int l_init_2=(l_init+2)*(l_init+2);

	for(int n=0; n<data.emitters.size(); n++) {
		for(int l=0; l<l_init_2; l++)
			delete[] B[n][l];
		delete[] B[n];
	}
	delete[] B;

	for(int n=0; n<data.emitters.size(); n++) {
		delete[] a1[n];
	}
  delete[] a1;

  for(int n=0; n<data.emitters.size(); n++)
		delete[] hankel[n];
	delete[] hankel;

	delete[] r_delta_norm;
	delete[] decay;

	delete[] a0;
}


/*!
 * Allocate memory for matrices, and precalculate some
 * things which are used in the process of the calculation
 * to accelerate it.
 *
 * @author Werner Smekal
 * @date 2000
 */
void Catom::init(void)
{
  int l_max=data.l_max;
  int l_max_2=(l_max)*(l_max);
  int l_init_2=(l_init+2)*(l_init+2);

	a1 = new complex<yreal>*[data.emitters.size()];
	for(int n=0; n<data.emitters.size(); n++) {
		a1[n] = new complex<yreal>[l_max_2];
		if(a1[n]==0)
			yasc_error("Unable to allocate memory for a1!");
	}

	B = new complex<yreal>**[data.emitters.size()];
	for(int n=0; n<data.emitters.size(); n++) {
		B[n] = new complex<yreal>*[l_init_2];
		for(int l=0; l<l_init_2; l++)
			B[n][l] = new complex<yreal>[l_max_2];
	}  

  hankel = new complex<yreal>*[data.emitters.size()];
	if(hankel==0)
		yasc_error("Unable to allocate memory for hankel!");

	r_delta_norm = new yreal[data.emitters.size()];
	if(r_delta_norm==0)
		yasc_error("Unable to allocate memory for r_delta_norm!");

	decay = new yreal[data.emitters.size()];
	if(decay==0)
		yasc_error("Unable to allocate memory for decay!");

  for(int n=0; n<data.emitters.size(); n++) {
		hankel[n] = new complex<yreal>[l_max];
		if(hankel[n]==0)
			yasc_error("Unable to allocate memory for hankel[]!");

		int emitter = data.emitters[n];
		if(emitter==id)
			continue;

		valarray<yreal> r_delta(0.0, 3);
		r_delta = rp - data.atoms[emitter]->get_r();
		r_delta_norm[n] = sqrt(r_delta[0]*r_delta[0]+r_delta[1]*r_delta[1]+r_delta[2]*r_delta[2]);
		decay[n] = data.inel_decay(r_delta_norm[n])*4*pi;

		for(int la=0; la<l_max; la++)
      hankel[n][la]=spec_func::hankel(la, data.k_vec*r_delta_norm[n]);    
  }
}


/*!
 * Calculate coordinates for new coordinate system (rotate
 * by polar angle theta and azimuthal angle phi).
 *
 * @author Werner Smekal
 * @date 2000
 *
 * @param theta : polar angle
 * @param phi : azimuthal angle
 */
void Catom::change_coord_system( yreal theta, yreal phi )
{
	rp[0] = cos(theta)*cos(phi)*r[0] + cos(theta)*sin(phi)*r[1] - sin(theta)*r[2];
	rp[1] =           -sin(phi)*r[0] +             cos(phi)*r[1];
	rp[2] = sin(theta)*cos(phi)*r[0] + sin(theta)*sin(phi)*r[1] + cos(theta)*r[2];
}

/*!
 * Return current coordinates of atom.
 *
 * @author Werner Smekal
 * @date 2000
 */
valarray<yreal>& Catom::get_r( void )
{
  return rp;
}


/*!
 * Return factor for zeroth wave.
 *
 * @author Werner Smekal
 * @date 2000
 *
 * @param l : quantum number for angular momentum
 * @param m : magnetic quantum number
 */
complex<yreal> Catom::get_a0( int l, int m )
{
	if( l>l_init+1 )
		yasc_error("Internal: maximum bound for l in atom::a0 exceeded!");

  return a0[l*l+m+l];
}


/*!
 * Calculate zeroth order wave for given direction defined by
 * the polar angle theta and the azimuthal angle phi.
 *
 * @author Werner Smekal
 * @date 2000
 * 
 * @param theta : polar angle
 * @param phi : azimuthal angle
 * @param theta_decay : TODO: dunno
 */
complex<yreal> Catom::psi0( yreal theta, yreal phi, yreal theta_decay )
{
  complex<yreal> psi0=complex<yreal>(0.0, 0.0);
  valarray<yreal> k(0.0, 3);
  yreal dp;

	//if((theta<=-pi/2) || (theta>=pi/2))
	//	yasc_error("Analyzer must be in upper half of sample!");

	yreal k_temp = data.k_vec;
  k[0] = sin(theta)*cos(phi)*k_temp;
  k[1] = sin(theta)*sin(phi)*k_temp;
  k[2] = cos(theta)*k_temp;

  dp = k[0]*rp[0]+k[1]*rp[1]+k[2]*rp[2];

  for(int l=l_init-1; l<=l_init+1; l+=2) {
    if(l<0) continue;
    for(int m=-l; m<=l; m++)
      psi0 += a0[l*l+m+l]*im_pow(l+1)*spec_func::spharm(l, m, theta, phi);
  }

	yreal dist = data.distance2edge(r[2], theta_decay);
  return psi0*data.inel_decay(dist)*complex<yreal>(cos(dp),-sin(dp));
}


/*!
 * Calculate factors a0[l][m] for zeroth order wave. 
 *
 * @author Werner Smekal
 * @date 2000
 */
void Catom::calculate_a0( void )
{
  for(int l=l_init-1; l<=l_init+1; l+=2) {
    if(l<0) continue;
    for(int m=-l; m<=l; m++) {
			a0[l*l+m+l] = complex<yreal>(0,0);

      for(int m_init=-l_init; m_init<=l_init; m_init++)
				a0[l*l+m+l] += ((l==l_init-1) ? data.Rm1 : data.Rp1)*spec_func::Gaunt(l,l_init,1,m,m_init,0);
		}
  }
}


/*!
 * Calculate factors B[l][m] for first order wave. 
 *
 * @author Werner Smekal
 * @date 2000
 */
void Catom::calculate_B()
{
  valarray<yreal> r_delta(0.0, 3);
  yreal theta, phi;
  complex<yreal> temp;

  int l_max=data.l_max;
  int l_max_2=(l_max)*(l_max);
  int l_init_2=(l_init+2)*(l_init+2);

	int num_emitters = data.emitters.size();

  for(int n=0; n<num_emitters; n++) {
		int emitter = data.emitters[n];

    //cout << "Atom rp=" << rp[0] << " " << rp[1] << " " << rp[2] << " " << endl;

    if(emitter==id)
			continue;

		r_delta = rp-data.atoms[emitter]->get_r();

		//cout << "Atom delta=" << r_delta[0] << " " << r_delta[1] << " " << r_delta[2] << " " << endl;

		phi=atan2(r_delta[1], r_delta[0]);
		theta=acos(r_delta[2]/r_delta_norm[n]);    //atan2(sqrt(r_delta[0]*r_delta[0]+r_delta[1]*r_delta[1]), r_delta[2]);

		//cout << "Atom - phi: " << phi*180/pi << " theta: " << theta*180/pi << endl;

		for(int la=0; la<l_max; la++)
			for(int mue=-la; mue<=la; mue++)
				Y[la*la+mue+la]=conj(spec_func::spharm(la, mue, theta, phi));
      
		for(int l=l_init-1; l<=l_init+1; l+=2) {
      if(l<0) continue;

      for(int m=-l; m<=l; m++)
        for(int lp=0; lp<l_max; lp++)
          for(int mp=-lp; mp<=lp; mp++) {
            temp=complex<yreal>(0,0);

            for(int la=0; la<l_max; la++)
              for(int mue=-la; mue<=la; mue++)
                temp+=i_pow(la+lp-l)*spec_func::Gaunt(lp, l, la, mp, m, mue)*
                  Y[la*la+mue+la]*hankel[n][la];

            B[n][l*l+m+l][lp*lp+mp+lp]=temp*decay[n];
          }
		}
  }
}


/*!
 * Calculate factors a1[l][m] for first order wave. 
 *
 * @author Werner Smekal
 * @date 2000
 */
void Catom::calculate_a1()
{
	int num_emitters = data.emitters.size();

	for(int ldp=0; ldp<data.l_max; ldp++)
		for(int mdp=-ldp; mdp<=ldp; mdp++) {
			for(int n=0; n<num_emitters; n++) {
				a1[n][ldp*ldp+mdp+ldp]=complex<yreal>(0,0);

				int emitter = data.emitters[n];
				if(emitter==id)   // beta not equal alpha, emitter doesn't scatter itself
					continue;

				for(int l=l_init-1; l<=l_init+1; l+=2) {
					if(l<0)
						continue;

					for(int m=-l; m<=l; m++)
						a1[n][ldp*ldp+mdp+ldp] += B[n][l*l+m+l][ldp*ldp+mdp+ldp]*data.atoms[emitter]->get_a0(l, m)*data.T_matrix[kind-1][ldp];
				}
			}
		}
}


/*!
 * Calculate first order wave for given direction defined by
 * polar angle theta and azimuthal angle phi and given emitter.
 *
 * @author Werner Smekal
 * @date 2000
 * 
 * @param theta : polar angle
 * @param phi : azimuthal angle
 * @param theta_decay : TODO: dunno
 * @param emitter : defined emitter whose photoelectron is scattered
 */
complex<yreal> Catom::psi1(yreal theta, yreal phi, yreal theta_decay, int emitter)
{
  complex<yreal> psi1=complex<yreal>(0,0), factor;
  yreal dp;
  valarray<yreal> k(0.0, 3);

	//if((theta<=-pi/2) || (theta>=pi/2))
	//	yasc_error("Analyzer must be in upper half of sample!");

	yreal k_temp = data.k_vec;
  k[0] = sin(theta)*cos(phi)*k_temp;
  k[1] = sin(theta)*sin(phi)*k_temp;
  k[2] = cos(theta)*k_temp;

	// cout << "psi1 - phi: " << phi*180/pi << " theta: " << theta*180/pi << endl;
  // cout << "psi1 rp=" << rp[0] << " " << rp[1] << " " << rp[2] << " " << endl;
	// cout << "psi1 k=" << k[0] << " " << k[1] << " " << k[2] << " " << endl;

  dp = k[0]*rp[0]+k[1]*rp[1]+k[2]*rp[2];

  // cout << "psi1 dp=" << dp << endl;

	for(int ldp=0; ldp<data.l_max; ldp++)
		for(int mdp=-ldp; mdp<=ldp; mdp++)
      psi1+=a1[emitter][ldp*ldp+mdp+ldp]*im_pow(ldp+1)*spec_func::spharm(ldp, mdp, theta, phi);
    
	yreal dist = data.distance2edge(r[2], theta_decay);
  return psi1*data.inel_decay(dist)*complex<yreal>(cos(dp),-sin(dp));
}

