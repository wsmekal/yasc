
#include "yascmain.h"

#ifdef HAVE_GSL
  #include "gsl/gsl_sf_legendre.h"
  #include "gsl/gsl_sf_bessel.h"
  #include "gsl/gsl_sf_coupling.h"
#endif

#define EPS 1.0e-10
#define FPMIN 1.0e-30
#define MAXIT 10000
#define XMIN 2.0
#define PI 3.141592653589793
#define RTPIO2 1.2533141
#define NUSE1 5
#define NUSE2 5
static int imaxarg1,imaxarg2;
#define IMAX(a,b) (imaxarg1=(a),imaxarg2=(b),(imaxarg1) > (imaxarg2) ?\
(imaxarg1) : (imaxarg2))
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

namespace spec_func
{
  #define MAX_FACT 171
  static int k_max=4;
  static double f[MAX_FACT] = { 1.0, 1.0, 2.0, 6.0, 24.0 };
  inline double factorial( int k )
  {
    if( k>k_max )
      if( k>=MAX_FACT ) {
        printf( "Exceeding factorial limit: %d\n", MAX_FACT );
        exit( -1 );
      } else { 
        for( int l=k_max+1; l<=k; l++ )
          f[l] = f[l-1]*l;
        k_max=k;
      }
      
    return f[k];
  }

  static int ok_max=4;
  static double of[MAX_FACT] = { 1.0, 1.0, 3.0, 15.0, 105.0 };
  inline double odd_factorial( int k )
  {
    if( k>ok_max )
      if( k>=MAX_FACT ) {
        printf( "Exceeding factorial limit: %d\n", MAX_FACT );
        exit( -1 );
      } else { 
        for( int l=ok_max+1; l<=k; l++ )
          of[l] = of[l-1]*(l+l-1);
        ok_max=k;
      }
      
    return of[k];
  }

  yreal sphbessel_jl( int n, yreal rho );
  yreal sphbessel_yl( int n, yreal rho );

  int idf = 140;
  double *fac = NULL;
  void logfac(void);
  yreal*** c3jz;
  int lmax;

  inline int max( int i, int j )
  {
    return (i>j) ? i : j;
  }
  
  inline int min( int i, int j )
  {
    return (i<j) ? i : j;
  }
}


complex<yreal> spec_func::yspharm( int l, int m, yreal theta, yreal phi )
{
  // calculating associated Legendre polynome
  double Plm=1.0;
  double Ptemp, swap, x;
  const double fourpi = 4.0*M_PI;
  bool m_minus=false;
  
  if( l!=0 ) {
    if( m<0 ) {
      m=-m;
      m_minus = true;
    }
    
    x = cos( theta );
   
    double sqx=1.0-x*x;
    for( int i=0; i<(m/2); i++ )
      Plm *= sqx;
    if( m&1 )
      Plm *= sqrt( sqx );
    
    Plm *= odd_factorial( m );
    //printf( "of(m=%d)=%f\n", m, odd_factorial( m ) ); 
    
    if( l>m ) {
      Ptemp=Plm;
      Plm *= (m+m+1)*x;
    }
    
    for( int i=m+2; i<=l; i++ ) {
      swap = ((i+i-1)*x*Plm-(i+m-1)*Ptemp)/(i-m);
      Ptemp = Plm;
      Plm = swap;
    }
  }
  
  Plm *= m1_pow( m )*sqrt( (l+l+1)*factorial(l-m)/fourpi/factorial(l+m) );
  
  double alpha=m*phi;
  if( m==0 )
    return complex<yreal>( Plm, 0.0 );
  else if( m_minus )
    if( m&1 )
      return complex<yreal>( -Plm*cos(alpha), Plm*sin(alpha) );
    else
      return complex<yreal>( Plm*cos(alpha), -Plm*sin(alpha) );
  else
    return complex<yreal>( Plm*cos(alpha), Plm*sin(alpha) );
}


yreal spec_func::sphbessel_jl( int n, yreal _rho )
{
  long double jl, jn, j0, jtemp, swap, scale, rho;
  int l;
	rho=_rho;

  j0=sin(rho)/rho;
  if( n==0 )
    return j0;
  
  if( n==1 )
    return (-cos(rho)+j0)/rho;
  
  if( n==2 )
    return -3.0*cos(rho)/rho/rho+(3.0/rho/rho/rho-1/rho)*sin(rho);
  
  l=(int)ceil( 5.0*std::max((double)n, (double)rho) );
  l=max( 20, l );
  
  jtemp=500.0;
  jl=400.0;
  
  for(int i=l; i>0; i--) {
    if( i==n )
      jn=jl;
    swap=(i+i+1)*jl/rho-jtemp;
    jtemp=jl;
    jl=swap;
  }
  
  scale=j0/jl;
  
  return scale*jn;
}

yreal spec_func::sphbessel_yl( int n, yreal rho )
{
  double yl, ytemp, swap;
  
  yl=-cos( rho )/rho;
  
  if( n>0 ) {
    ytemp=yl;
    yl=(yl-sin(rho))/rho;
  }

  for( int i=2; i<=n; i++) {
    swap = (i+i-1)*yl/rho-ytemp;
    ytemp=yl;
    yl=swap;
  }
  
  return yl;
}

complex<yreal> spec_func::yhankel( int n, yreal x )
{
  return complex<yreal>(sphbessel_jl(n, x), sphbessel_yl(n, x));
}


#ifdef HAVE_GSL   // use gnu scientific library
  complex<yreal> spec_func::gsl_hankel(int n, yreal x)
  {
    return complex<yreal>(gsl_sf_bessel_jl(n, x), gsl_sf_bessel_yl(n, x));
  }
  
  
  complex<yreal> spec_func::gsl_spharm( int l, int m, yreal theta, yreal phi )
  {
    complex<yreal> y;
  
    bool mlzero=false;
    yreal prefac=1.0;
  
    if( theta<0.0 )
      yasc_error("spharm: theta negativ!");
  
    if( m<0 ) {
      m=-m;
      prefac=m1_pow( m ); //pow((-1.0), (double)m);
      mlzero=true;
    }
  
    y=gsl_sf_legendre_sphPlm(l, m, cos(theta))*complex<yreal>(cos(m*phi), sin(m*phi));
  
    if( mlzero )
      y=prefac*conj(y);
  
    return y;
  }
#endif // HAVE_GSL
  
  
void spec_func::init_C3j( void )
{
  if( !fac ) {
    if(!(fac = new double[idf]))
      yasc_error("Couldn't allocate memory for Clebsch Gordan class!");
  
    logfac();
  }
}


void spec_func::init( int lm )
{
  init_C3j();
  
  lmax=lm;
  
  c3jz = new yreal**[lmax+1];
	for(int l1=0; l1<=lmax; l1++)
    c3jz[l1] = new yreal*[lmax+1];
  
	for(int l1=0; l1<=lmax; l1++)
    for(int l2=0; l2<=lmax; l2++)
      c3jz[l1][l2] = new yreal[lmax+1];

  printf( "Precalculating Wigner constants..." );
  for(int l1=0; l1<=lmax; l1++)
		for(int l2=0; l2<=lmax; l2++)
			for(int l3=0; l3<=lmax; l3++)
        c3jz[l1][l2][l3] = C3j_zero(l1, l2, l3);
  puts( " Done!" );
}

yreal spec_func::Gaunt( int l1, int l2, int l3, int m1, int m2, int m3 )
{
  if ((l1<0) || (l2<0) || (l3<0) || (abs(m1)>l1) || (abs(m2)>l2) || (abs(m3)>l3))
    return 0.0f;
  else {
#ifdef USE_GSL
    yreal temp;  
    if( (temp=gsl_sf_coupling_3j(2*l1,2*l2,2*l3,(-2)*m1,2*m2,2*m3)) == 0.0 )
      return 0.0;
    else
      return m1_pow(m1)*sqrt((double)(2*l1+1)*(2*l2+1)*(2*l3+1))/(2.0*sqrt(pi))*
        gsl_sf_coupling_3j(2*l1,2*l2,2*l3,0,0,0)*temp;
#else
    yreal temp;  
    if( (temp=C3j(l1,l2,l3,-m1,m2,m3)) == 0.0 )
      return 0.0;
    else
      return m1_pow(m1)*sqrt((double)(2*l1+1)*(2*l2+1)*(2*l3+1))/(2.0*sqrt(pi))*
        C3j_zero(l1,l2,l3)*temp;
#endif
	}
}


void spec_func::free_C3j( void )
{
	if( fac )
	  delete[] fac;

	for(int l1=0; l1<=lmax; l1++)
    for(int l2=0; l2<=lmax; l2++)
      delete[] c3jz[l1][l2];

	for(int l1=0; l1<=lmax; l1++)
    delete[] c3jz[l1];

  delete[] c3jz;
}

void spec_func::logfac(void)
{
  fac[0]=fac[1]=0.0;
  for(int i=2; i<idf; i++)
    fac[i]=fac[i-1]+log(static_cast<double>(i-1));
}

double spec_func::C3j_zero(int i1, int i2, int i3)
{
  int i, ip;
  int j1, j2, j3, j4, j5, j6, j7, j8;
  double z;

  if(i3>(i1+i2))
    return 0.0;

  if(i3<abs(i1-i2))
    return 0.0;
  
  i=i1+i2+i3;
  ip=i/2;                                                                    
  if(i!=(2*ip))
    return 0.0;
  
  j8=i1+i2+i3+2;
  if(j8>idf)
    yasc_error("Parameter too high in Clebsch Gordan Class");                                                     
  j1=ip+1;                                                                   
  j2=ip-i1+1;
  j3=ip-i2+1;
  j4=ip-i3+1;
  j5=i1+i2-i3+1;
  j6=i2+i3-i1+1;
  j7=i3+i1-i2+1;
  z=0.5*(fac[j5]+fac[j6]+fac[j7]-fac[j8])+fac[j1]-fac[j2]-fac[j3]-fac[j4];
  z=m1_pow(ip)*exp(z);

  return z;
}

double spec_func::C3j(int i1, int i2, int i3, int m1, int m2, int m3)
{
  int i,izmax,izmin;
  int l1,l2,l3,l4,l5,l6,l7,l8,l9,l10;
  int k1,k2,k3,k4,k5,k6,k7;
  double* ac;
  double abra,gros,accu,sig;

  l4=i1+i2+i3+2;
  if(l4>idf-1)
    yasc_error("Parameter too high in Clebsch Gordan Class");

  if((m1+m2+m3)!=0)
    return 0.0;

  izmax=min(i1+i2-i3,min(i1-m1,i2+m2))+1;
  izmin=max(0,max(i2-i3-m1,i1+m2-i3))+1;

  if(izmax<izmin)
    return 0.0;

  l1=i1+i2-i3+1;
  l2=i3+i1-i2+1;
  l3=i3+i2-i1+1;
  l5=i1+m1+1;
  l6=i1-m1+1;
  l7=i2+m2+1;
  l8=i2-m2+1;
  l9=i3+m3+1;
  l10=i3-m3+1;
  abra=0.5*(fac[l1]+fac[l2]+fac[l3]-fac[l4]+fac[l5]+fac[l6]+
						fac[l7]+fac[l8]+fac[l9]+fac[l10]);
  k1=i3-i2+m1+1;
  k2=i3-i1-m2+1;

  gros=250.0;
  ac = new double[idf];

  for(int j=izmin; j<=izmax; j++) {
		i=j-1;
		k3=l1-i;
		k4=l6-i;
		k5=l7-i;
		k6=k1+i;
		k7=k2+i;
		ac[j]=fac[i+1]+fac[k3]+fac[k4]+fac[k5]+fac[k6]+fac[k7];
		if(ac[j]<gros)
			gros=ac[j];
	}

  accu=0.0;
  sig=m1_pow(izmin);

  for(int j=izmin; j<=izmax; j++) {
		sig=-sig;
		ac[j]=ac[j]-gros;
		accu=accu+sig*exp(-ac[j]);
	}

  delete [] ac;

  return m1_pow(i1-i2-m3)*exp(abra-gros)*accu;
}


/*
	SUBROUTINE SPHARM(l1,l2,l3,m1,m2,m3,Z)
	C	AUTHOR   RICARDO DIEZ MUINO
	C	         BERKELEY, APRIL 1999
	C
	C	SPHARM PERFORMS THE ANGULAR INTEGRAL OF
	C	THREE SPHERICAL HARMONICS:
	C
	C	Z = INT ( (Y(l1,m1))^(*)  Y(l2,m2) Y(l3,m3)) =
	C
	C         = (-1)**(m1) * [1/(4*PI)]**(1/2) *
	C
	C           [(2l1+1) (2l2+1) (2l3+1)]**(1/2) *
	C
	C           ( l1  l2  l3 )   (l1  l2  l3)
	C           (-m1  m2  m3 )   ( 0   0   0)
	C
	C	WARNING: A CALL TO THE SUBROUTINE 'LOGfac' MUST
	C	         HAVE BEEN DONE BEFORE USING 'SPHARM'!
	C
	C	SPHARM USES THE SUBROUTINES 'LOGfac', 'LOGCLE' AND
	C	                            'CLEZER'.
	C
	REAL*8 facTOR,Z,Z1,Z2,PI
	INTEGER l1,l2,l3,m1,m2,m3
	PARAMETER (pi=3.14159265D0)

	C
	C	CHECK INPUT: RETURNS ZERO VALUE FOR WRONG VALUES OF L's AND M's
	C
	if ((l1.lt.0).OR.(l2.lt.0).OR.(l3.lt.0).OR.
	+      (abs(m1).gt.l1).OR.(abs(m2).gt.l2).OR.(abs(m3).gt.l3))
	+  then
	z=0.d0
	else
	factor = ((-1.d0)**(m1))*((2*l1+1)*(2*l2+1)*(2*l3+1))**(0.5d0)
	factor = factor / (2.d0*(pi**(0.5d0)))
	call clezer(l1,l2,l3,z1)
	call logcle(l1,l2,l3,-m1,m2,m3,z2)
	z = factor*z1*z2
	end if

	RETURN
	END


	*******************************************

	BLOCK DATA                                                                
	PARAMETER (IDF=140)                                                       
	REAL*8 fac
	LOGICAL INIT                                                              
	DATA INIT/.FALSE./                                                        
	COMMON/facT/fac(IDF),INIT
	END                                                                       

	*******************************************

	SUBROUTINE LOGfac
	PARAMETER (IDF=140)                                                       
	REAL*8 fac
	LOGICAL INIT                                                              
	COMMON/facT/fac(IDF),INIT
	INIT=.TRUE.                                                               
	fac(1)=0.D0
	DO 10 I=2,IDF                                                             
	fac(I)=fac(I-1)+DLOG(DFLOAT(I-1))
	10 CONTINUE                                                                  
	RETURN                                                                    
	END                                                                       

	*******************************************

	SUBROUTINE LOGCLE(i1,i2,i3,m1,m2,m3,Z)
	C         AUTHOR   A.SALIN     VERSION 1 23/2/77                                
	C        CALCUL DES 3J  ( DEFINITION DE MESSIAH)                                
	C        RESTRICTION: INTEGER VALUES OF MOMENTS ONLY                            
	C        BEFORE THE FIRST UTILIZATION IN A RUN, A CALL LOGfac SHOULD
	C     BE MADE LOGfac CALCULATES LOG((N-1)|) FOR N GOING FROM ONE TO NMAX
	C     THE RESULTS ARE STORED IN THE COMMON facT.
	C        FOR VALUES OF NMAX>IDF, CHANGE PARAMETER IDF.                          
	C                                                                               
	IMPLICIT REAL*8 (A-H,O-Z)                                                 
	PARAMETER(IDF=140)                                                        
	COMMON/facT/fac(IDF),INIT
	LOGICAL INIT                                                              
	DIMENSION AC(IDF)                                                         
	IF(.NOT.INIT) GO TO 11                                                    
	DATA IW/6/                                                                
	l4=i1+i2+i3+2
	IF(l4.GT.IDF) GO TO 1
	IF(m1+m2+m3) 2,3,2
	3 IZMAX=MIN(i1+i2-i3,i1-m1,i2+m2)+1
	IZMIN=MAX(0,i2-i3-m1,i1+m2-i3)+1
	IF(IZMAX-IZMIN) 2,4,4                                                     
	4 l1=i1+i2-i3+1
	l2=i3+i1-i2+1
	l3=i3+i2-i1+1
	l5=i1+m1+1
	L6=i1-m1+1
	l7=i2+m2+1
	l8=i2-m2+1
	l9=i3+m3+1
	l10=i3-m3+1
	ABRA=0.5D0*(fac(l1)+fac(l2)+fac(l3)-fac(l4)+fac(l5)+fac(l6)+fac(l7
	1)+fac(l8)+fac(l9)+fac(l10))
	K1=i3-i2+m1+1
	K2=i3-i1-m2+1
	GROS=250.                                                                 
	DO 8II=IZMIN,IZMAX                                                        
	I=II-1                                                                    
	K3=l1-I
	K4=l6-I
	K5=l7-I
	K6=K1+I                                                                   
	K7=K2+I                                                                   
	AC(II)=fac(I+1)+fac(K3)+fac(K4)+fac(K5)+fac(K6)+fac(K7)
	IF(AC(II).LT.GROS)  GROS=AC(II)                                           
	8 CONTINUE                                                                  
	ACCU=0.D0                                                                 
	SIG=(-1.)**IZMIN                                                          
	DO 9II=IZMIN,IZMAX                                                        
	SIG=-SIG                                                                  
	AC(II)=AC(II)-GROS                                                        
	ACCU=ACCU+SIG*DEXP(-AC(II))                                               
	9 CONTINUE                                                                  
	Z=(-1.)**(i1-i2-m3)*DEXP(ABRA-GROS)*ACCU
	RETURN                                                                    
	11 WRITE(IW,98)                                                              
	98 FORMAT(1X,'WARNING: LOGfac NOT CALLED BEFORE LOGCLE')
	STOP                                                                      
	2 Z=0.D0                                                                    
	RETURN                                                                    
	1 WRITE(IW,99)                                                              
	99 FORMAT(1X,'LOGCLE: VALUE OF PARAMETER TOO HIGH')                          
	RETURN                                                                    
	END                                                                       

	*******************************************

	SUBROUTINE CLEZER(i1,i2,i3,Z)
	C        AUTHOR: A.SALIN    VERSION 1  24/2/77                                  
	C        CALCUL DES 3J (DEFINITION DE MESSIAH) POUR LE CAS DE PROJEC-           
	C     TIONS NULLES DES MOMENTS                                                  
	C        RESTRICTION: INTEGER VALUE OF MOMENTS ONLY                             
	C        RUNS AS LOGCLE                                                         
	C                                                                               
	IMPLICIT REAL*8 (A-H,O-Z)                                                 
	PARAMETER (IDF=140)                                                       
	COMMON/facT/fac(IDF),INIT
	LOGICAL INIT                                                              
	IF(.NOT.INIT) GO TO 11                                                    
	DATA IW/6/                                                                
	IF(i3.GT.(i1+i2)) GO TO 1
	IF(i3.LT.IABS(i1-i2)) GO TO 1
	I=i1+i2+i3
	IP=I/2                                                                    
	IF(I.NE.(2*IP)) GO TO 1                                                   
	J8=i1+i2+i3+2
	IF(J8.GT.IDF) GO TO 2                                                     
	J1=IP+1                                                                   
	J2=IP-i1+1
	J3=IP-i2+1
	J4=IP-i3+1
	J5=i1+i2-i3+1
	J6=i2+i3-i1+1
	J7=i3+i1-i2+1
	Z=0.5D0*(fac(J5)+fac(J6)+fac(J7)-fac(J8))+fac(J1)-fac(J2)-fac(J3)
	1-fac(J4)
	Z=(-1)**IP*DEXP(Z)                                                        
	RETURN                                                                    
	11 WRITE(IW,98)                                                              
	98 FORMAT(1X,'LOGfac HAS NOT BEEN CALLED BEFORE CLEZER')
	STOP                                                                      
	1 Z=0.D0                                                                    
	RETURN                                                                    
	2 WRITE(IW,99)                                                              
	99 FORMAT(1X,'CLEZER: VALUE OF PARAMETER TOO HIGH')                          
	RETURN                                                                    
	END                                                                       
*/
