
#include "yascmain.h"

// class data

inline void ignore(ifstream& file)
{
	file.ignore(1000, '\n');
}

inline bool check(bool com, string message, ifstream& file)
{
	if(com) {
		cout << "Warning: " << message << endl;
		ignore(file);
	}

	return com;
}

Cdata::Cdata( void )
{
	natom = 0;
}

Cdata::~Cdata( void )
{
  spec_func::free_C3j();
  
 	for( int i=0; i<natom; i++ )
		delete atoms[i];

  for( int kind=0; kind<num_kinds; kind++ )
	  delete[] T_matrix[kind];
  delete[] T_matrix;
}

void Cdata::read(void)
{
  yreal x, y, z, R, d;
  int kind, emitter, s;
	string* fn_phase;

	bool com_theta_start = false;
	bool com_theta_end = false;
	bool com_theta_step = false;
	bool com_phi_start = false;
	bool com_phi_end = false;
	bool com_phi_step = false;
	bool com_scantype = false;
	bool com_rotate = false;
	bool com_l_init = false;
	bool com_l_max = false;
	bool com_radma0 = false;
	bool com_radma1 = false;
	bool com_polvec_theta = false;
	bool com_polvec_phi = false;
	bool com_inner_potential = false;
	bool com_electronic_edge = false;
	bool com_imfp = false;
	bool com_energy = false;
	bool com_cluster = false;
	bool com_output = false;
	bool com_analyzer_angle = false;
	bool com_l_num = false;

	// init variables
	k_vec=0.0;
	l_init=-1;
	l_max=0;
	l_num=0;
	num_kinds=0;
	first_run = true;
	T_matrix = new complex<yreal>*[92];
	if(!T_matrix)
		yasc_error("Could not allocate memory for T matrix");

  ifstream input(filename_in.c_str());
	if(!input)
		yasc_error("Couldn't open the input file!");

	while (!input.eof()) {
		string command;
		input >> command;

		if(command == "") {
			ignore(input);
			continue;
		}
		
		// command theta
		if (command == "theta") {
			string keyword;
			input >> keyword;
			if (keyword == "start") {
				if (check(com_theta_start, "theta start was already set and won't be changed!", input)) continue;
				com_theta_start = true;
				input >> theta_start;
				ignore(input);
				continue;
			} else if (keyword == "end") {
				if (check(com_theta_end, "theta end was already set and won't be changed!", input)) continue;
				com_theta_end = true;
				input >> theta_end;
				ignore(input);
				continue;
			} else if (keyword == "step") {
				if (check(com_theta_step, "theta step was already set and won't be changed!", input)) continue;
				com_theta_step = true;
				input >> theta_step;
				ignore(input);
				continue;
			} else
				yasc_error("Unkown keyword after command 'theta'!");
		}

		// command phi
		if (command == "phi") {
			string keyword;
			input >> keyword;
			if (keyword == "start") {
				if (check(com_phi_start, "phi start was already set and won't be changed!", input)) continue;
				com_phi_start = true;
				input >> phi_start;
				ignore(input);
				continue;
			} else if (keyword == "end") {
				if (check(com_phi_end, "phi end was already set and won't be changed!", input)) continue;
				com_phi_end = true;
				input >> phi_end;
				ignore(input);
				continue;
			} else if (keyword == "step") {
				if (check(com_phi_step, "phi step was already set and won't be changed!", input)) continue;
				com_phi_step = true;
				input >> phi_step;
				ignore(input);
				continue;
			} else
				yasc_error("Unkown keyword after command 'phi'!");
		}

		// command azimuthalscan
		if (command == "azimuthalscan") {
			if (check(com_scantype, "The type of the scan was already set and won't be changed!", input)) continue;
			SType = azimuthalscan;
			ignore(input);
			com_scantype = true;
			continue;
		}

		// command polarscan
		if (command == "polarscan") {
			if (check(com_scantype, "The type of the scan was already set and won't be changed!", input)) continue;
			SType = polarscan;
			ignore(input);
			com_scantype = true;
			continue;
		}

		// command polarscan
		if (command == "holoscan") {
			if (check(com_scantype, "The type of the scan was already set and won't be changed!", input)) continue;
			SType = holoscan;
			ignore(input);
			com_scantype = true;
			continue;
		}

		// command rotate
		if (command == "rotate") {
			if (check(com_rotate, "The type of the rotation was already set and won't be changed!", input)) continue;
			com_rotate = true;
			string keyword;
			input >> keyword;
			ignore(input);
			if (keyword == "analyzer") {
				rotation = 0;
				continue;
			} else if (keyword == "sample") {
				rotation = 1;
				continue;
			} else
				yasc_error("Unkown keyword after command 'rotate'!");
		}

		// command output
		if (command == "output") {
			if (check(com_output, "The output filename was already set and won't be changed!", input)) continue;
			com_output = true;
			input >> filename_out;
			ignore(input);
			// open output file
			output.open(filename_out.c_str());
			continue;
		}

		// command l_init
		if (command == "l_init") {
			if (check(com_l_init, "l_init was already set and won't be changed!", input)) continue;
			com_l_init = true;
			input >> l_init;
			ignore(input);
			continue;
		}

		// command radma0
		if (command == "radma0") {
			if (check(com_radma0, "radma0 was already set and won't be changed!", input)) continue;
			com_radma0 = true;
			input >> R >> d;
			ignore(input);
			Rm1=R*complex<yreal>(cos(d), sin(d));
			continue;
		}

		// command radma1
		if (command == "radma1") {
			if (check(com_radma1, "radma1 was already set and won't be changed!", input)) continue;
			com_radma1 = true;
			input >> R >> d;
			ignore(input);
			Rp1=R*complex<yreal>(cos(d), sin(d));
			continue;
		}

		// command l_max
		if (command == "l_max") {
			if (check(com_l_max, "l_max was already set and won't be changed!", input)) continue;
			com_l_max = true;
			input >> l_max;
			ignore(input);
			if ((l_num!=0) && (l_num-1<=l_max))
				yasc_error("l_max is to high");
      spec_func::init( l_max );
			continue;
		}

		// command l_num
		if (command == "l_num") {
			if (check(com_l_num, "l_num was already set and won't be changed!", input)) continue;
			com_l_num = true;
			input >> l_num;
			ignore(input);
			if ((l_max!=0) && (l_num-1<=l_max))
				yasc_error("l_max is to high");
			continue;
		}

		// command kind
		if (command == "kind") {
			int kind_temp;
			input >> kind_temp;
			ignore(input);
			if(!com_l_num)
				yasc_error("Might be a good idea to define l_num before you define any phase shifts!");
			if (kind_temp != num_kinds+1)
				yasc_error("Phaseshifts for different kinds must be defined in correct order!");
			num_kinds++;

			// get memory for T_matrix
			T_matrix[kind_temp-1] = new complex<yreal>[l_num];
			if(!T_matrix[kind_temp-1])
				yasc_error("Could not allocate memory for T matrix");

			yreal delta;
			for(int l=0; l<l_num; l++) {
				input >> delta;
				T_matrix[kind_temp-1][l] = complex<yreal>(-sin(delta)*sin(delta), sin(delta)*cos(delta));
			}
			ignore(input);
			continue;
		}

		// command polvec
		if (command == "polvec") {
			string keyword;
			input >> keyword;
			if (keyword == "theta") {
				if (check(com_polvec_theta, "polvec theta was already set and won't be changed!", input)) continue;
				com_polvec_theta = true;
				input >> theta_e;
				theta_e*=pi/180.0;
				ignore(input);
				continue;
			} else if (keyword == "phi") {
				if (check(com_polvec_phi, "polvec phi was already set and won't be changed!", input)) continue;
				com_polvec_phi = true;
				input >> phi_e;
				phi_e*=pi/180.0;
				ignore(input);
				continue;
			} else
				yasc_error("Unkown keyword after command 'polvec'!");
		}

		// command inner_potential
		if (command == "inner_potential") {
			if (check(com_inner_potential, "V0 was already set and won't be changed!", input)) continue;
			com_inner_potential = true;
			input >> V0;
			ignore(input);
			continue;
		}

		// command electronic_edge
		if (command == "electronic_edge") {
			if (check(com_electronic_edge, "z0 was already set and won't be changed!", input)) continue;
			com_electronic_edge = true;
			input >> z0;
			ignore(input);
			continue;
		}

		// command imfp
		if (command == "imfp") {
			if (check(com_imfp, "imfp was already set and won't be changed!", input)) continue;
			com_imfp = true;
			input >> lambda;
			ignore(input);
			continue;
		}

		// command analyzer_angle
		if (command == "analyzer_angle") {
			if (check(com_analyzer_angle, "analyzer angle was already set and won't be changed!", input)) continue;
			com_analyzer_angle = true;
			input >> theta_anal;
  		theta_anal*=pi/180.0;
			ignore(input);
			continue;
		}

		// command energy
		if (command == "energy") {
			if (check(com_energy, "energy or k_vec was already set and won't be changed!", input)) continue;
			com_energy = true;
			input >> ekin;
			k_vec=sqrt(ekin/3.809777);
			ignore(input);
			continue;
		}

		// command k_vec
		if (command == "k_vec") {
			if (check(com_energy, "energy or k_vec was already set and won't be changed!", input)) continue;
			com_energy = true;
			input >> k_vec;
			ekin = 3.809777*pow(k_vec, 2);
			ignore(input);
			continue;
		}

		// command cluster
		if (command == "cluster") {
			if (check(com_cluster, "The cluster was already defined!", input)) exit(1);
			com_cluster = true;
			input >> natom;
			ignore(input);
			atoms.clear();

			if(!com_l_init)
				yasc_error("l_init must be defined before a cluster is defined");

			if(!com_l_max)
				yasc_error("l_max must be defined before a cluster is defined");

			if(!com_polvec_phi || !com_polvec_theta)
				yasc_error("The polarisation vector must be defined before the cluster!");

			for(int i=0; i<natom; i++) {
				input >> x >> y >> z >> kind >> emitter;
				ignore(input);
        add_atom( x, y, z, kind, emitter );
			}
			continue;
		}

		// command end
		if (command == "end")
			break;

		cout << "Command " << command << " is not known, sorry!" << endl;
		exit(1);
	}

	// check if anything is missing
	if(!com_theta_start || !com_theta_end || !com_theta_step || !com_phi_start || !com_phi_end
		 || !com_phi_step || !com_scantype || !com_rotate || !com_l_init || !com_l_max
		 || !com_radma0 || !com_radma1 || !com_polvec_theta || !com_polvec_phi
		 || !com_inner_potential || !com_electronic_edge || !com_imfp || !com_energy
		 || !com_cluster || !com_output || !com_analyzer_angle || (num_kinds == 0))
		yasc_error("Not everything necessary to run this simulation was defined!");

	// tell user what was read in
	if(!quiet) {
		cout << "-------------------------------------------------------------------------------" << endl;
		cout << "Theta(start, end, step)              : (" << theta_start << ", " << theta_end << ", " << theta_step << ")" << endl;
		cout << "Phi(start, end, step)                : (" << phi_start << ", " << phi_end << ", " << phi_step << ")" << endl;
		cout << "                                     : " << (SType == polarscan ? "Polarscan" : "Azimuthalscan") << endl;
		cout << "Rotate                               : " << (rotation == 0 ? "Analyzer" : "Sample") << endl;
		cout << "Initial l                            : " << l_init << endl;
		cout << "Radial Matrix Element (l-1)          : " << Rm1 << endl;
		cout << "Radial Matrix Element (l+1)          : " << Rp1 << endl;
		cout << "Polarisation Vector(Theta, Phi)      : (" << theta_e*180/pi << ", " << phi_e*180/pi << ")" << endl;
		cout << "Number of phase shifts               : " << l_max << endl;
		cout << "Kinetic energy                       : " << ekin << "eV (=" << k_vec << " 1/A)" << endl;
		cout << "Cluster                              : " << natom << " atom(s) and " << emitters.size() << " emitter(s)" << endl;
		cout << "Inelastic mean free path             : " << lambda << "A" << endl;
		cout << "Inner potential V0                   : " << V0 << "eV" << endl;
		cout << "Electronic edge                      : " << z0 << "A" << endl;
		cout << "-------------------------------------------------------------------------------" << endl;
	}

	// write data to output file
	output << "#------------------------------------------------------------------------------" << endl;
	output << "#Theta(start, end, step)              : (" << theta_start << ", " << theta_end << ", " << theta_step << ")" << endl;
	output << "#Phi(start, end, step)                : (" << phi_start << ", " << phi_end << ", " << phi_step << ")" << endl;
	output << "#                                     : " << (SType == polarscan ? "Polarscan" : "Azimuthalscan") << endl;
	output << "#Rotate                               : " << (rotation == 0 ? "Analyzer" : "Sample") << endl;
	output << "#Initial l                            : " << l_init << endl;
	output << "#Radial Matrix Element (l-1)          : " << Rm1 << endl;
	output << "#Radial Matrix Element (l+1)          : " << Rp1 << endl;
	output << "#Polarisation Vector(Theta, Phi)      : (" << theta_e*180/pi << ", " << phi_e*180/pi << ")" << endl;
	output << "#Number of phase shifts               : " << l_max << endl;
	output << "#Kinetic energy                       : " << ekin << "eV (=" << k_vec << " 1/A)" << endl;
	output << "#Cluster                              : " << natom << " atom(s) and " << emitters.size() << " emitter(s)" << endl;
	output << "#Inelastic mean free path             : " << lambda << "A" << endl;
	output << "#Inner potential V0                   : " << V0 << "eV" << endl;
	output << "#Electronic edge                      : " << z0 << "A" << endl;
	output << "#------------------------------------------------------------------------------" << endl;

	// some post initializations
	l_max++; // XXX hack since l_max is actually l_num in yasc <- change
	phi_e=0.0; // since coordinate system was changed that way
	//theta_anal = theta_e-(pi/2.0-(theta_anal*pi/180.0));
	//phi_anal = 0.0;
	// check_angle(&theta_anal, &phi_anal);

	//cout << "Theta_e " << theta_e*180/pi << endl;
  //cout << "Phi_e " << phi_e*180/pi << endl;
	//cout << "Theta_anal " << theta_anal*180/pi << endl;
	//cout << "Phi_anal " << phi_anal*180/pi << endl;
}

bool Cdata::next_scan(yreal& _theta, yreal& _phi, yreal& _k)
{
  bool condition=true;

  if(first_run) {
    _theta=theta=theta_start;
    _phi=phi=phi_start;
		first_run = false;
    newline = false;
    return true;
	}

  switch(SType)
    {
    case polarscan:
      theta+=theta_step;
      if(theta>theta_end) {
				theta=theta_start;
				phi+=phi_step;
				if(phi>phi_end)
					condition=false;
      }
      break;
    case azimuthalscan:
      phi+=phi_step;
      if(phi>phi_end) {
				phi=phi_start;
				theta+=theta_step;
				if(theta>theta_end)
					condition=false;
      }
      break;
    case holoscan:
      {
        // distribute the measure points uniformly
        // across the half sphere
        yreal holo_phi_step;
      
        if( theta==0 )
          holo_phi_step = 1000.0;
        else
          holo_phi_step = (theta_end>theta_start ? theta_end : theta_start)*theta_step/theta;
  
        phi+=holo_phi_step;
        newline=false;
        if(phi>phi_end) {
          phi=phi_start;
          theta+=theta_step;
          newline = true;
          if(theta>theta_end)
            condition=false;
        }
      }
      break;
    }

  _theta=theta; _phi=phi; _k=k_vec;

  return condition;
}

// electron wave refraction at the surface
// inside the medium the kinetic energy is ekin(k), outside it is ekin-V0
yreal Cdata::refract(yreal theta_out)
{
	//yreal theta_in = acos(sqrt((ekin-V0)/ekin)*cos(pi/2.0-theta_out));
	return theta_out;
	//return asin((ekin+V0)/ekin*sin(theta_out));
  // XXX - why??? think about it??

	//return pi/2-theta_in;
}


// calculate distance to inner edge
// it is assumed that atoms are below inner edge
yreal Cdata::distance2edge(yreal z, yreal theta)
{
	return (z0-z)/cos(theta);
}


yreal Cdata::inel_decay(yreal dist)
{
	return exp(-dist/(2*lambda));
}

bool Cdata::add_atom( yreal x, yreal y, yreal z, int kind, bool emitter )
{
  if((kind>num_kinds) || (kind<1))
    yasc_error("Unkown kind");
  if(z>z0)
    yasc_error("It is not possible to place an atom above the inner edge!");

  // turn x,y,z about z-axis into polvec plane
  x=cos(phi_e)*x-sin(phi_e)*y;
  y=sin(phi_e)*x+cos(phi_e)*y;
  z=z;

  int natom = atoms.size();
  
  Catom* new_atom = new Catom( natom, x, y, z, kind, emitter==1 ? true : false, l_init );
  atoms.push_back( new_atom );
  if( emitter==1 )
    emitters.push_back( natom );
}

