
/*! @page manual Manual
  @section usage Usage
  If YaSC is called without any options, it reads in all necessary data from the default input file @c input.dat.
  Information regarding the simulation is written to the standard output. However, you can pass YaSC three options at the shell
  - <tt>-i input file</tt> tells YaSC to use <tt>input file</tt> instead of the default file @c input.dat
  - @c -q quiet mode, the program doesn't write anything to the standard output
  - @c -? short help
  
  Example: <tt>YaSC -iinput.Ni80 -q</tt>
  YaSC uses the file @c input.Ni80 as the input file and is in quiet mode.

@section input_file_format Input File Format
The input file is a normal text-file. Every line generally contains a keyword and the corresponding parameter
(all in lower case). Empty
lines are ignored. Everything after the keyword and the parameter to the end of the line is ignored.
Keywords can be generally given in any order, with some exceptions (e.g. \b l_max must be given before \b kind).
Everything after the keyword \b end is ignored. The following list specifies all keywords.

- \b output \c filename \n
  The filename of the output file is defined. If this keyword is omitted than \c output.dat will be used.
- <b>theta start</b> \f$\vartheta_{start}\f$ \n
  If the type of scan is set to \b polarscan then all polarscans start at \f$\vartheta=\vartheta_{start}\f$. If the type of
  scan is set to \b azimuthalscan then the first azimuthalscan is at \f$\vartheta=\vartheta_{start}\f$.
  Valid values of \f$\vartheta_{start}\f$ are between \f$0^{\circ}\f$ and \f$89^{\circ}\f$.
- <b>theta stop</b> \f$\vartheta_{stop}\f$ \n
  If the type of scan is set to \b polarscan then every polarscan stops at \f$\vartheta=\vartheta_{stop}\f$. If the type of
  scan is set to \b azimuthalscan then the last azimuthalscan is at \f$\vartheta=\vartheta_{stop}\f$.
  Valid values of \f$\vartheta_{stop}\f$ are between \f$0^{\circ}\f$ and \f$89^{\circ}\f$.
- <b>theta step</b> \f$\Delta \vartheta\f$ \n
  Defines the stepsize for \f$\vartheta\f$ of the scan. Valid values of \f$\Delta \vartheta\f$ are between \f$0^{\circ}\f$ and \f$89^{\circ}\f$. 
- <b>phi start</b> \f$\varphi_{start}\f$ \n
  If the type of scan is set to \b polarscan then the first polarscan is at \f$\varphi=\varphi_{start}\f$. If the type of
  scan is set to \b azimuthalscan then all azimuthalscans start at \f$\varphi=\varphi_{start}\f$.
  Valid values of \f$\varphi_{start}\f$ are between \f$0^{\circ}\f$ and \f$360^{\circ}\f$.
- <b>phi stop</b> \f$\varphi_{stop}\f$ \n
  If the type of scan is set to \b polarscan then the last polarscan is at \f$\varphi=\varphi_{stop}\f$. If the type of
  scan is set to \b azimuthalscan then all azimuthalscans stop at \f$\varphi=\varphi_{stop}\f$.
  Valid values of \f$\varphi_{stop}\f$ are between \f$0^{\circ}\f$ and \f$360^{\circ}\f$.
- <b>phi step</b> \f$\Delta \varphi\f$ \n
  Defines the stepsize for \f$\varphi\f$ of the scan. Valid values of \f$\Delta \varphi\f$ are between \f$0^{\circ}\f$ and \f$360^{\circ}\f$. 
- \b polarscan \n
  The program runs through consecutive polar scans, e.g. the scan starts at (\f$\vartheta_{start}\f$, \f$\varphi_{start}\f$) and
  stops at (\f$\vartheta_{stop}\f$, \f$\varphi_{start}\f$) while \f$\Delta \vartheta\f$ will be added consecutively to \f$\vartheta\f$
  during the scan. \f$\varphi\f$ is unchanged during the scan. Than \f$\Delta \varphi\f$ is added to \f$\varphi\f$ and the next scan
  starts. The photoelectron intensities for (\f$\vartheta\f$,\f$\varphi\f$) will be stored in the output file in this order.
- \b azimuthalscan n
  The program runs through consecutive azimuthal scans, e.g. the scan starts at (\f$\vartheta_{start}\f$, \f$\varphi_{start}\f$) and
  stops at (\f$\vartheta_{start}\f$, \f$\varphi_{stop}\f$) while \f$\Delta \varphi\f$ will be added consecutively to \f$\varphi\f$
  during the scan. \f$\vartheta\f$ is unchanged during the scan. Than \f$\Delta \vartheta\f$ is added to \f$\vartheta\f$ and the next scan
  starts. The photoelectron intensities for (\f$\vartheta\f$,\f$\varphi\f$) will be stored in the output file in this order.
- <b>rotate sample</b> \n
  It is assumed that that the surface normal points to the analyzer. The polarization vector is defined
  by the \b analyzer_angle keyword. The keyword \b polvec is ignored.
  During the scans the analyzer as well as the x-ray
  source is fixed and the sample is rotated. Here rotating the sample by (\f$\vartheta\f$,\f$\varphi\f$) means, that the sample
  surface normal is rotated by \f$\vartheta\f$ to the analyzer, where the surface normal stays in the plane defined
  by the analyzer and the polarization vector. Then the sample is rotated by \f$\varphi\f$ around the surface normal.
- <b>rotate analyzer</b> \n
  Sample and x-ray source are fixed, while the analyzer can be moved around the hemisphere above the sample surface.
  Photoelectron intensities will be calculated for (\f$\vartheta\f$,\f$\varphi\f$) where the surface normal defines the \f$z\f$ axis
  and the \f$x\f$ axis lays in the plane defined by the surface normal and the polarisation vector.
- \b l_init \f$l_i\f$ \n
  Angular momentum of the inital state of the electron.
- \b radma0 \f$R_{l_i-1} \quad \delta_{l_i-1}\f$ \n
  Radial matrix element \f$M_{l_i-1 l_i} = R_{l_i-1}e^{i \delta_{l_i-1}}\f$.
- \b radma1 \f$R_{l_i+1} \quad \delta_{l_i+1}\f$ \n
  Radial matrix element \f$M_{l_i+1 l_i} = R_{l_i+1}e^{i \delta_{l_i+1}}\f$.
- \b k_vec \em k \n
  Length of the wave vector of the photoelectron wave, which determines the kinetic energy of
  the photoelectron (\f$E=\hbar^ 2k^2/2m\f$).
- \b l_num \em l \n
  Number of angular momenta the user provides at the \b kind keyword. This number
  must be the same for every kind defined.
- \b l_max \f$l_{max}\f$ \n
  Highest angular momentum which is taken into account for the calculation. \f$l_{max}\f$ must be lower than \f$l\f$. 
- \b kind \c kind \n
  All phase shifts from 0 up to \em l-1 should follow this keyword. \c kind is the identifier for this kind of atom.
  The identifier must start with 1 and all kinds declared afterwards must have consecutive numbers
  (see \ref example_input_file "example input file").
- \b analyzer_angle \f$\beta\f$ \n
	This is the angle (in degree) between the polarisation vector and the analyzer. This keyword will be ignored if
	<b>rotate analyzer</b> was defined.
- <b>polvec theta</b> \f$\vartheta_{\epsilon}\f$ \n
	Polar angle (in degree) of polarization vector where p-polarisation is always assumed.
- <b>polvec phi</b> \f$\varphi_{\epsilon}\f$ \n
	Azimuthal angle (in degree) of the polarization vector. Defines in fact the plane where sample surface normal and
	analyzer (in case of <b>rotate sample</b>) are located.
- \b inner_potential \f$V_0\f$ \n
	The potential barrier \f$V_0\f$ in eV must be surmounted by the electrons if they cross the surface.
- \b electronic_edge  \f$z_0\f$ \n
	It is assumed that the potential barrier \f$V_0\f$ is effective above the surface to the electronic edge \f$z_0\f$ (in Angstrom).
	It actually only influences the length of the path the electrons travel through the solid (and are scattered
	inelastically). \f$z_0\f$ must be greater or equal 0.
- \b imfp \f$\lambda_i\f$ \n
	Inelastic mean free path in Angstrom. 
- \b cluster \c natoms \n
	The position of all atoms in this cluster and the atoms itself will be defined after this keyword.
	\c natoms is the number of atoms the cluster consists of. For every atom a set of numbers must be provided by 
	the user after the \b cluster keyword (\f$x \quad y \quad z \quad kind \quad emitter\f$). Here, \f$x\f$, \f$y\f$ and \f$z\f$
	define the position of this atom, \f$kind\f$ defines the kind of the atom (where \f$kind\f$ relates to the numbers
	provided at the \b kind keyword) and \c emitter is set to 1 if the atom is an emitter, 0 if not (for
	an example see \ref example_input_file "example input file").
- \b end \n
	Tells YaSC to ignore everything after this keyword.

\subsection example_input_file Example Input File
\verbatim
output output.dat

theta start 0
theta end 89
theta step 1
phi start 0
phi end 0
phi step 1

polarscan
rotate sample

l_init 1
radma1 0.028450 2.6651
radma0 0.0070469 -0.4441

k_vec 12.92
l_max 12
l_num 20
kind 1
   -.4441  -2.1000   2.6651   1.6086
    .9919    .6383    .4264    .2905
    .2011    .1370    .0920    .0618
    .0396    .0223    .0106    .0042
    .0014    .0004    .0001    .0000
analyzer_angle 60
polvec theta 30
polvec phi 0

inner_potential 0
electronic_edge 0.0
imfp 10.0

cluster 5
  0 0 0 1 0
  0 0 -3.52 1 0
  0 0 -7.04 1 0
  0 0 -10.56 1 0
  0 0 -14.08 1 1
end
\endverbatim

\section output_file_format Output File Format
The name of the output file is defined by the \b output keyword in the input file. The first part
of the output file contains all information of the simulation in clear text. This part is also
commented out with '#', so that normally these lines should be ignored by plotting programs. The second
part is the data part, where the first two numbers define the polar and azimuthal angle, \f$\vartheta\f$ and
\f$\varphi\f$, while the third number is the intensity of the direct wave and the forth number is the
intensity of the resulting wave, where direct and scattered wave were added by the amplitude.

\subsection example_output_file Example Output File
\verbatim
#------------------------------------------------------------
#Theta(start, end, step)         : (45, 45, 1)
#Phi(start, end, step)           : (0, 359, 1)
#                                : Azimuthalscan
#Rotate                          : Analyzer
#Initial l                       : 0
#Radial Matrix Element (l-1)     : (0, 0)
#Radial Matrix Element (l+1)     : (-0.00556766, -0.018283)
#Polarisation Vector(Theta, Phi) : (30, 0)
#Number of phase shifts          : 12
#Kinetic energy                  : 477.898eV (=11.2 1/A)
#Cluster                         : 4 atom(s) and 1 emitter(s)
#Inelastic mean free path        : 10.343A
#Inner potential V0              : 0eV
#Electronic edge                 : 0A
#------------------------------------------------------------
45 0 6.47443e-06 7.05709e-06
45 1 6.4737e-06 7.05321e-06
45 2 6.47154e-06 7.04159e-06
45 3 6.46793e-06 7.02224e-06
45 4 6.46289e-06 6.99517e-06
...
45 355 6.4564e-06 6.96046e-06
45 356 6.46288e-06 6.99517e-06
45 357 6.46793e-06 7.02224e-06
45 358 6.47154e-06 7.04159e-06
45 359 6.4737e-06 7.05321e-06
\endverbatim
*/