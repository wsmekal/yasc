
/*! @page theory Theory
@section single_crystal Model of Electron Transport in Single Crystals

In XPS the surface of a crystal is irradiated with electromagnetic radiation. This radiation excites
atoms and eventually electrons are emitted (photo effect). These photoelectrons are scattered by
other atoms (potentials) inside the solid and finally leave the crystal. In quantum mechanics this
process is described by the perturbation of the inital state @f$ \left| \phi^a_i \right\rangle @f$
which gets excited by a perturbation potential @f$ \hat{\Gamma} @f$ (incident light). This direct
wave is then scattered by other atoms. This latter process is described by the
operator @f$ \hat{t}_{\alpha} @f$. Eventually the final wavefunction @f$ | \phi^a_f \rangle @f$ is

\f{eqnarray*}
\arraycolsep100pt
| \phi^a_f \rangle & = & \underbrace{\hat{G}_0 \hat{\Gamma} \left| \phi^a_i \right>}_{\textrm{\scriptsize direct wave}}
 + \underbrace{\sum_{\alpha \neq a} \hat{G}_0 \hat{t}_{\alpha} (\hat{G}_0 \hat{\Gamma} \left| \phi^a_i \right>
 )}_{1^{st}\hspace{0.2em}\textrm{\scriptsize order of scattering}}\\
& & + \underbrace{\sum_{\alpha \neq a, \beta \neq \alpha} \hat{G}_0 \hat{t}_{\beta} \hat{G}_0 \hat{t}_{\alpha}
  (\hat{G}_0 \hat{\Gamma} \left| \phi^a_i \right> )}_{2^{nd}\hspace{0.2em}\textrm{\scriptsize order of scattering}} 
+ \ldots
\f}

where @f$ \hat{G}_0@f$ is the propagator (Green function), \em a is the excited atom and @f$ \alpha@f$ and @f$ \beta@f$ are atoms
where scattering takes place.

\subsection single_scattering Single Scattering
The final wave function @f$ \phi_f ( \vec{r} )@f$ is in fact a sum over different waves
@f$ \varphi_{\alpha}^i ( \vec{r}-\vec{R}_{\alpha})@f$ emerging from different sites
@f$ \vec{R}_{\alpha}@f$ where @f$i@f$ stands for the order of scattering.
@f[
\phi_f ( \vec{r} ) = \sum_{\alpha, i} \varphi_{\alpha}^i ( \vec{r} -
\vec{R}_{\alpha})
@f]
The function @f$ \varphi_{\alpha}^i \left( \vec{r} \right)@f$ can be developed in the region in which the potential
(e.g. muffin tin potential) is flat (or zero) in terms of spherical harmonics @f$Y_{lm} \left( \Omega_{\vec{r}} \right)@f$
and the Hankel function @f$h^+_l \left( kr \right)@f$ to
@f[
\varphi_{\alpha}^i \left( \vec{r} \right) = \sum_{lm} \left( a^{\alpha}_{lm} \right)^i h^+_l \left( kr \right)
Y_{lm} \left( \Omega_{\vec{r}} \right)
@f]
@f$h^+_l (kr)@f$ (or @f$h^{\left( 1 \right)}_l (kr)@f$) is the Hankel function or Spherical Bessel function of the
third kind according to the definitions of Abramowitz and Stegun~\cite{abramsteg}. The asymptotic behaviour of @f$h^+_l (kr)@f$ is
@f[
h^+_l (kr) \stackrel{r \rightarrow \infty}{\longrightarrow} (-i)^{l+1} \frac{e^{ikr}}{kr}
@f]
For the calculation of the photoelectron intensity one needs to evaluate the term
@f$h^+_l \left( k \left| \vec{r}-\vec{R}_{\alpha} \right| \right) Y_{lm} \left( \Omega_{\vec{r}-\vec{R}_{\alpha}} \right)@f$
for @f$ \vec{r}@f$ much larger than @f$ \vec{R}_{\alpha}@f$
@f[
h^+_l \left( k \left| \vec{r}-\vec{R}_{\alpha} \right| \right) Y_{lm} \left( 
\Omega_{\vec{r}-\vec{R}_{\alpha}} \right) \stackrel{r \gg R_{\alpha}}{\longrightarrow}
\frac{e^{ikr}}{kr} e^{-i\vec{k} \vec{R}_{\alpha}} \left( -i \right)^{l+1} Y_{lm} 
\left( \Omega_{\vec{r}} \right)
@f]
where @f$ \frac{e^{ikr}}{kr}@f$ will be ignored since it is just a constant factor for azimuthal or polar scans.

\subsubsection{Photoemission}
The inital core orbital @f$ \phi_i^{\alpha}(\vec{r})@f$ of atom @f$ \alpha@f$ from which emission takes place is given by
the usual expansion in radial and angular funtions
@f[
\phi_i^{\alpha}(\vec{r}) = \sum_{lm} \frac{1}{r} \chi_l (r)
 Y_{lm} ({\Omega_{\vec{r}}})
@f]
The incident light can be described by a perturbation potential @f$ \hat{\Gamma}@f$
@f[
\hat{\Gamma} := \mathcal{C} \hat{\epsilon} \cdot \hat{r}
@f]
where @f$ \hat{\epsilon}@f$ is the polarization vector and @f$ \mathcal{C}@f$ a constant. For further calculations
it is assumed that the @f$z@f$ axis points along the polarization vector @f$ \hat{\epsilon}@f$. Hence, 
@f[
\hat{\epsilon} \cdot \hat{r} = \epsilon r Y_{10} \left( \Omega_{\vec{r}} \right)
@f]
This assumption is valid since we only consider linear polarization. In case of circular or elliptical polarization 
or unpolarized light the polarization vector would not fix a direction in space.
Further it is assumed that the potential of the emitting atom is spherical.

The corresponding Greenfunction @f$ \hat{G}_{\alpha}@f$ is defined as:
\f{eqnarray*}
\left\langle \vec{r} \right| \hat{G}_{\alpha} \left| \vec{r}\hspace{1pt}' \right\rangle & = &
G_{\alpha} \left( \vec{r}, \vec{r}\hspace{1pt}' \right) \\ 
& = & \left( -ik \right) \sum_{lm}
Y^*_{lm} \left( \Omega_{\vec{r}\hspace{0.5pt}'} \right) Y_{lm} \left( \Omega_{\vec{r}} \right)
e^{i\delta_l} \left[ \overline{f^+_l} \left( k r_> \right) \overline{R_l} \left(
kr_< \right) \right]
\f}
where @f$ \overline{f^+_l} \left( kr \right)@f$ and
@f$ \overline{R_l} \left( kr \right)@f$ are the irregular and regular solutions of the potential
which have the asymptotic behaviour
@f[
\overline{f^+_l} \left( kr \right) \stackrel{r \rightarrow \infty}{\longrightarrow}
h^+_l \left( kr \right)
@f]
@f[
\overline{R_l} \left( kr \right) \stackrel{r \rightarrow \infty}{\longrightarrow}
\frac{\sin{\left( kr-\frac{l \pi}{2} + \delta_l \right)}}{kr}
@f]
The wave function is calculated outside the potential region, i.e. in the asymptotic region in
which the potential is either flat ot takes zero value.

\subsubsection{Direct Wave: Zeroth Order of Scattering}
The direct wave @f$ \hat{G}_{\alpha} \hat{\Gamma} \left| \phi^{\alpha}_i \right\rangle@f$ can now be 
evaluated at @f$ \vec{r}@f$ (we assume that @f$ \left| \vec{r} \right| \gg \left| \vec{r}\hspace{1pt}' \right|@f$)
\f{eqnarray*}
\varphi^0_{\alpha} \left( \vec{r} \right) & = & \left\langle \vec{r} \right| \hat{G}_{\alpha}
\hat{\Gamma} \left| \phi^{\alpha}_i \right\rangle = \int d\vec{r}\hspace{1pt}' \left\langle \vec{r}
\right| \hat{G}_{\alpha} \left| \vec{r}\hspace{1pt}' \right\rangle \left\langle \vec{r}\hspace{1pt}' \right|
\hat{\Gamma} \left| \phi^{\alpha}_i \right\rangle \\ & = &
\int d\vec{r}\hspace{1pt}' \left( -ik \right) \sum_{lm} Y^*_{lm} \left( \Omega_{\vec{r}\hspace{0.5pt}'} \right)
Y_{lm} \left( \Omega_{\vec{r}} \right) e^{i\delta_l} h^+_l \left( kr \right)
\overline{R_l} \left( kr' \right) \\ & &
\cdot \mathcal{C} \epsilon_{r'} Y_{10} \left( \Omega_{\vec{r}\hspace{0.5pt}'} \right) \sum_{l'm'}
\frac{1}{r'} \chi_{l'} \left( r' \right) Y_{l'm'} \left( \Omega_{\vec{r}\hspace{0.5pt}'} \right)\\
& = & \sum_{lm} Y_{lm} \left( \Omega_{\vec{r}} \right) h^+_l \left( kr \right)
\left[ \left( -ik \right) e^{i \delta_l} \mathcal{C} \epsilon \right] \\
& & \cdot \sum_{l'm'} \int d\vec{r}\hspace{1pt}' {r'}^2 \overline{R_l} \left( kr' \right)
\chi_{l'} \left( r' \right) {\sf C} \left( l,l',1;m,m',0 \right)
\f}
where
\f{eqnarray*}
{\sf C} \left( l_1, l_2, l_3; m_1, m_2, m_3 \right) & = & \int d\Omega \left( Y^{m_1}_{l_1} \right)^*
Y^{m_2}_{l_2} Y^{m_3}_{l_3} \\
& = & \frac{1}{\sqrt{4 \pi}} \left( -1 \right)^{m_1} \left[ \left( 2l_1+1 \right) \left( 2l_2+1 \right)
\left( 2l_3+1 \right) \right]^{\frac{1}{2}}\\
& & \cdot \left( {l_1 \atop 0}{l_2 \atop 0}{l_3 \atop 0} \right)
\left( {l_1 \atop -m_1}{l_2 \atop m_2}{l_3 \atop m_3}\right) 
\f}
is an angular coefficient that is an angular integration of three spherical harmonics which can be related
to the 3j-coefficients @f$ \left( {l_1 \atop m_1}{l_2 \atop m_2}{l_3 \atop m_3}\right)@f$. This integral is called
the Gaunt integral~\cite{gaunt}, since Gaunt has derived an analytical formula to calculate its value. With the definition
of the radial matrix element
@f[
M_{ll'} := \left( -ik \right) e^{i \delta_l} \mathcal{C} \epsilon \int dr' {r'}^2 \overline{R_l} 
\left( kr' \right) \chi_{l'} \left( r' \right)
@f]
we obtain
\f{equation}
\label{eqn:direct_wave}
\varphi^0_{\alpha} \left( \vec{r} \right) = \sum_{lm} Y_{lm} \left( \Omega_{\vec{r}} \right)
h^+_l \left( kr \right) \sum_{l'm'} {\sf C} \left( l,l',1;m,m',0 \right) M_{ll'}
\f}
Since the coefficient
@f$ {\sf C} \left( l_1,l_2,l_3;m_1,m_2,m_3 \right)@f$ is only different from zero if @f$m_1=m_2+m_3@f$,
the sum over @f$m'@f$ is actually not necessary. In our case we
could set @f$m'=m@f$ and ignore the sum over @f$m'@f$.
If we define the matrix
@f[
\left( a^{\alpha}_{lm} \right)^0 := \sum_{l'm'} {\sf C} \left( l,l',1;m,m',0 \right) M_{ll'}
@f]
we can rewrite Eq.~(\ref{eqn:direct_wave})
\f{equation}
\label{eqn:direct_wave_vector}
\varphi^0_{\alpha} \left( \vec{r} \right) = \sum_{lm} \left( a^{\alpha}_{lm} \right)^0 
h^+_l \left( kr \right) Y_{lm} \left( \Omega_{\vec{r}} \right)
\f}
\label{sec:direct-wave:-zeroth}

If there is emission from more than one site, it depends on the considered problem, if the wave amplitudes
(coherent) or the wave intensities (incoherent) are added. For example, if you study photoemission from the
outer orbitals of a molecule, which are degenerated in energy, the resulting spectra can be approximated 
as the coherent sum of the photoemission from the different orbits involved
@f[
\psi^0 \left( \vec{r} \right) = \sum_{\alpha} \varphi^0_{\alpha} \left( \vec{r} - \vec{R}_{\alpha} \right)
@f]
On the other hand, the core emission in solid-state materials can be approximated as an incoherent
emission process, since the splitting of the energy levels of the core orbitals is negligable
@f[
\left| \psi^0 \left( \vec{r} \right)\right|^2 = \sum_{\alpha}
\left| \varphi^0_{\alpha} \left( \vec{r} - \vec{R}_{\alpha} \right) \right|^2
@f]

\subsubsection{First Order of Scattering}

The partial wave method~\cite{schiff68} instructs us
to expect an outgoing spherical wave
@f$i^l h^+_l \left( kr \right) Y_{lm} \left( kr \right)@f$ proportional to each regular
spherical wave incident on the potential
\f{equation}
\label{eqn:outgoing_wave}
i^l j_l \left( kr \right) Y_{lm} \left( \Omega_{\vec{r}} \right) \longrightarrow
T_l \left( k \right) i^l h^+_l \left( kr \right) Y_{lm} \left( \Omega_{\vec{r}} \right) 
\f}
The complex proportionality factor, @f$T_l (k)@f$, which is derived from
the partial-wave phase shift @f$ \delta_l \left( k \right)@f$, has both a scattering amplitude and a wave
phase shift
@f[
T_l \left( k \right) = i \left( \sin \delta_l \right) e^{i \delta_l} = \frac{e^{i \delta_l} - e^{-i \delta_l}}{2}
e^{i \delta_l} = \frac{e^{2i \delta_l}-1}{2}
@f]
If a spherical wave from a source at the origin is incident on the potential centered at @f$ \vec{R}@f$ the expansion
in spherical harmonics has been derived by Nozawa~\cite{nozawa}. If the spherical
wave emanates from the origin, we may expand it around @f$ \vec{R}@f$ as
\f{equation}
\label{eqn:expanded_wave}
i^l h^+_l \left( kr \right) Y_{lm} \left( \Omega_{\vec{r}} \right) = \sum_{l'' m''} G_{lml''m''} i^{l''} j_{l''} \left(
k \left| \vec{r} - \vec{R} \right|
\right) Y_{l''m''} \left( \Omega_{\vec{r}- \vec{R}} \right)
\f}
where
\f{eqnarray*}
G_{lml''m''} & = & \sum_{l'm'} 4 \pi i^{l'} h_{l'} \left( kR \right) Y_{l'm'}^* \left( \Omega_{\vec{R}} \right)\\
& & \int Y_{lm} \left( \Omega_{\vec{R}} \right) Y_{l'm'} \left( \Omega_{\vec{R}} \right)
Y^*_{l''m''} \left( \Omega_{\vec{R}} \right) d\vec{R} \\
& = & \sum_{l'm'} 4 \pi i^{l'} h_{l'} ( k \vec{R} ) Y^*_{l'm'}
\left( \Omega_{\vec{R}} \right) {\sf C} \left( l'', l, l'; m'', m, m' \right)
\f}
This is the so-called Nozawa's origin-shift addition theorem.

\begin{figure}[htb]
\begin{center}
\epsfig{file=figures/scatter_vectors.eps, width=6cm}
\end{center}
\caption{Definition of the vectors used in our scattering equations. The polarization vector is represented by
@f$ \hat{\epsilon}@f$. @f$ \vec{R}_{\alpha}@f$ and @f$ \vec{R}_{\beta}@f$ are positions of atoms, the detector axis lies along @f$ \vec{r}@f$.}
\label{fig:scatter_vectors}
\end{figure}
If the direct wave [Eq.~(\ref{eqn:direct_wave_vector})] emanates from @f$R_{\alpha}@f$ and is scattered at a potential
centered at @f$R_{\beta}@f$ the scattered wave can be written [see Eq.~(\ref{eqn:outgoing_wave}) and
Eq.~(\ref{eqn:expanded_wave})] as
\f{eqnarray}
\label{eqn:scattered_wave}
\varphi_{\beta}^1 \left( \vec{r} \right) & = & \sum_{lm} \left( a_{lm}^{\alpha} \right)^0 i^{-l}
\sum_{l''m''} G_{lml''m''} i^{l''} T_{l''} \left( k \right) h_{l''}^+ \left( k \left| \vec{r} - \vec{R}_{\beta} \right|
\right) Y_{l''m''} \left( \Omega_{\vec{r} - \vec{R}_{\beta}} \right) \nonumber\\
& = & \sum_{lm} \left( a_{lm}^{\alpha} \right)^0 i^{-l} \sum_{l''m''} \sum_{l'm'} 4 \pi i^{l'} {\sf C}
\left( l'',l,l';m'',m,m' \right) \\
& & \times h_{l'}^+ \left( k \left| \vec{R}_{\beta} - \vec{R}_{\alpha} \right| \right) Y_{l'm'}^* 
\left( \Omega_{\vec{R}_\beta - \vec{R}_{\alpha}} \right) i^{l''} T_{l''} \left( k \right) h_{l''}^+ \left( kr \right)
Y_{l''m''} \left( \Omega_{\vec{R}_{\beta}} \right) \nonumber
\f}
If we combine some terms to the matrix
\f{eqnarray*}
B_{l''m''lm} & = & \sum_{lm} \sum_{l''m''} 4 \pi i^{l'-l+l''} {\sf C} \left( l'',l,l';m'',m,m' \right) \\
& & \times h_{l'}^+ \left( k \left| \vec{R}_{\beta} - \vec{R}_{\alpha} \right| \right)
Y_{l'm'}^* \left( \Omega_{\vec{R}_{\beta}- \vec{R}_{\alpha}} \right)
\f}
we can rewrite Eq.~(\ref{eqn:scattered_wave})
@f[
\varphi_{\beta}^1 \left( \vec{r} \right)  =  \sum_{lm} \left( a_{lm}^{\alpha} \right)^0
\sum_{l''m''} B_{l''m''lm} T_{l''} \left( k \right) h_{l''}^+ \left( k \left| \vec{r} - \vec{R}_{\beta} \right|
\right) Y_{l''m''} \left( \Omega_{\vec{r} - \vec{R}_{\beta}} \right)
@f]
Again, if we introduce a matrix @f$ \left( a_{l''m''}^{\beta} \right)^1@f$
@f[
\left( a_{l''m''}^{\beta} \right)^1 = \sum_{lm} \left(a_{lm}^{\alpha} \right)^0 B_{l''m''lm}
T_{l''} \left( k \right)
@f]
the scattered wave is finally
\f{equation}
\varphi_{\beta}^1 \left( \vec{r} \right)  =  \sum_{l''m''} \left( a_{l''m''}^{\beta} \right)^1
h_{l''}^+ \left( k \left| \vec{r} - \vec{R}_{\beta} \right| \right)
Y_{l''m''} \left( \Omega_{\vec{r} - \vec{R}_{\beta}} \right)
\f}

\subsection{Multiple Scattering}
If multiple scattering is considered the formalism of 2@f$^{nd}@f$, 3@f$^{rd}@f$, @f$ \ldots@f$ order of scattering
is exactly the same as for the 1@f$^{st}@f$ order of scattering with the distinction that the incident wave
is a (multiple) scattered wave itself.
@f[
  \left( a^{\beta}_{l''m''} \right)^n = \sum_{\alpha \neq \beta} \sum_{lm} \left( a_{lm}^{\alpha} \right)^{n-1} B_{l''m''lm}
  T_{l''}^{\beta} \left( k \right)
@f]
\f{equation}
  \label{eqn:multi_scat}
  \varphi_{\beta}^{n} \left( \vec{r} \right) = \sum_{l''m''} \left( a_{l''m''}^{\beta} \right)^n 
  h_{l''}^+ \left( k \left|\vec{r} - \vec{R}_{\beta} \right| \right) Y_{l''m''} \left(
  \Omega_{\vec{r} - \vec{R}_{\beta}} \right)  
\f}
where
\f{eqnarray*}
B_{l''m''lm} & = & \sum_{l'm'} 4\pi i^{l'-l+l''} {\sf C} \left( l'',l,l';m'',m,m' \right) \\
&& h_{l''}^+ \left( k \left|\vec{R}_{\beta} - \vec{R}_{\alpha} \right| \right) Y_{l''m''} \left(
  \Omega_{\vec{R}_{\beta} - \vec{R}_{\alpha}} \right)  
\f}
and @f$ \alpha@f$ stands for an emitting atom or a previous scatterer.
Here @f$ \vec{R}_{\beta}@f$ is the position where the potential is located and @f$ \vec{R}_{\alpha}@f$
the position of the scatterer or emitter, where the incident wave comes from.

The final wave function of one emitter at @f$ \vec{R}_{\alpha}@f$ up to the nth order of scattering
is 
\f{equation}
  \label{eqn:final_multi_scat}
  \psi^{n} \left( \vec{r} \right) = \sum_{\beta} \sum_{lm} \left( A_{lm}^{\alpha ; \beta} \right)^n 
  h_{l}^+ \left( k \left|\vec{r} - \vec{R}_{\beta} \right| \right) Y_{lm} \left(
  \Omega_{\vec{r} - \vec{R}_{\beta}} \right)  
\f}
where
\f{eqnarray*}
  \left( A^{\alpha ; \beta}_{lm} \right)^n & = & \left( a_{lm}^{\alpha} \right)^0 + \left(a_{lm}^{\beta} \right)^1
  + \ldots + \left( a_{lm}^{\beta} \right)^n \\
  & = & \sum_{j=1}^n \left( a_{lm}^{\beta} \right)^j + \left( a_{lm}^{\alpha} \right)^0
\f}
and @f$ \beta@f$ stands for every atom in the considered cluster.

\subsubsection{Effects of Multiple Scattering}
\label{sec:effects-mult-scatt}
It is important that multiple scattering is considered if the emitters are deep inside the solid and
if there are linear chains of atoms toward the surface. As shown by Tong et al.~\cite{tongprb32}, multiple forward
scattering is responsible for a defocusing effect which disperses the forward scattering peak: the peak
gets weaker and narrower which could also be observed in experiments. This could be understood
classically since only electrons with very small initial deflection could pass many scatterers and
contribute to the forward scattering peak~\cite{fonda}.

This effect is illustrated, using a classical picture, in Fig.~\ref{fig:effects_ms}.

FIGURE EFFECTS_MS

\caption{Classical description of focusing and defocusing. (a) shows the forward focusing due to a single
scattering event. In (b) a trajectory scattered into forward direction by the first scatterer is deflected
out of the forward direction by the second scatterer. Still, a trajectory slightly further off axis can
contribute to the forward scattering peak. (c) illustrates that, as more atoms are added to the chain,
it is increasingly more difficult to achieve the conditions of forward focusing: most of the trajectories
eventually get scattered away from the forward direction~\cite{fonda}.}
\label{fig:effects_ms}

Considering this, single scattering cluster calculations can only provide the exact position of the forward
peak but fail to estimate the intensity of this peak. Calculations~\cite{fadjes57} have shown, that the defocusing
effect is more effective the higher the energy of the photoelectrons and/or the shorter the interatomic
distances. Besides, it appears that light atoms defocus less than heavy ones (see Fig.~\ref{fig:ecs_si} and 
Fig.~\ref{fig:ecs_au} for comparison).
On the other hand multiple scattering must be considered for non-collinear (bent) atomic chains
if the kinetic energy of the electrons are low, due to the fact the contributions from larger scattering angles
become more significant in those energy regions~\cite{fonda}.

Tables \ref{tab:prop_scat_factor_high} and 
\ref{tab:prop_scat_factor_low} summarize the properties of electron-atom
scattering and of multiple scattering at low and high energies.\\
\begin{table}
\caption{Properties of the scattering factor and multiple scattering for high energies (@f$>500@f$eV). Lines marked with
(PD) are properties considering photoelectron diffraction, others are valid in general~\cite{fonda}.}
\label{tab:prop_scat_factor_high}
\begin{center}
\begin{tabular}{lp{10cm}}
\hline
\hline
\multicolumn{2}{l}{\raisebox{-1.2ex}{\bf Scattering factor}}\\
\hspace{0.5cm} & @f$ \rightarrow@f$ strongly peaked in forward direction\\
\hspace{0.5cm} & @f$ \rightarrow@f$ @f$s@f$-like behaviour in backscattering\\
\hspace{0.5cm} & @f$ \rightarrow@f$ information on atoms in front of the emitter, good to determine bond directions for adsorbed molecules
or low index directions in epitaxial layers (PD)\\
\hline
\multicolumn{2}{l}{\raisebox{-1.2ex}{\bf Multiple scattering (PD)}}\\
\hspace{0.5cm} & @f$ \rightarrow@f$ less important in general\\
\hspace{0.5cm} & @f$ \rightarrow@f$ important in long chains of collinear atoms where it causes defocusing\\
\hspace{0.5cm} & @f$ \rightarrow@f$ single scattering calculations good off chains of atoms\\
\hline \hline
\end{tabular}
\end{center}
\end{table}
\begin{table}
\caption{Properties of the scattering factor and multiple scattering for low energies (@f$50-400@f$eV).  Lines marked with
(PD) are properties considering photoelectron diffraction, others are valid in general~\cite{fonda}.}
\label{tab:prop_scat_factor_low}
\begin{center}
\begin{tabular}{lp{10cm}}
\hline
\hline
\multicolumn{2}{l}{\raisebox{-1.2ex}{\bf Scattering factor}}\\
\hspace{0.5cm} & @f$ \rightarrow@f$ more isotropic, back- and side-scattering more important\\
\hspace{0.5cm} & @f$ \rightarrow@f$ @f$s@f$-like behaviour and good signal in backscattering\\
\hspace{0.5cm} & @f$ \rightarrow@f$ first and higher order interference effects more important (PD)\\
\hspace{0.5cm} & @f$ \rightarrow@f$ information on atoms behind or beside the emitter (PD)\\
\hline
\multicolumn{2}{l}{\raisebox{-1.2ex}{\bf Multiple scattering (PD)}}\\
\hspace{0.5cm} & @f$ \rightarrow@f$ more important in this energy region\\
\hspace{0.5cm} & @f$ \rightarrow@f$ important over large angular range\\
\hspace{0.5cm} & @f$ \rightarrow@f$ single scattering still useful as a first approximation\\
\hline \hline
\end{tabular}
\end{center}
\end{table}


*/