 /*!
  @mainpage YASC - Yet Another Single sCattering program
 
  @section news News
    20060203: Released the first version of YaSC to the internet.
 
  @section overview Overview
    YaSC is an acronym for Yet Another Single sCattering program, and calculates angular intensity distributions
    of photoelectrons in single crystals taking the first order of scattering into consideration. Multiple 
    scattering is not considered in the moment.
    
    There are several multiple scattering cluster programs available: Multiple Scattering Calculation of
    Diffraction @ref overview_references "(MSCD)",
    Electron Diffraction in Atomic Clusters @ref overview_references "(EDAC)" and others. Altough these programs are very advanced, fast and
    comprehensive, many problems arise when amorphous clusters (that is, randomly distributed atoms within the cluster
    limit) should be simulated. This is not surprising, since these programs were not written with this kind of
    problem in mind.
  
    MSCD for example refuses to calculate photoelectron intensities for amorphous clusters if the order of
    scattering is higher than one. This is due to the geometric optimization of this program. Since MSCD
    won't find any geometric similarities, it fails in the analyzing phase, except for first order scattering.
    If one constrains oneself to single scattering, the next problem is the limit of atoms
    considered by MSCD. This limit was hardcoded to 300 atoms, which is enough to simulate photoelectron diffraction
    in single crystals. One big advantage of MSCD is, that the source code of this program is available and so the latter
    problem could be solved, but only to run into the next problem: altough there is no geometric optimization
    done for single scattering, the program still allocates memory for this process. Hence it was not possible
    to simulate more than about 600 atoms on a workstation with 512MB. Since implementing a workaround for this problem
    would have necessitated a major rewrite of
    the (mainly undocumented) code, the decision was made not to use MSCD for this thesis.   
  
    EDAC, which represents the next generation of multiple scattering cluster programs after MSCD, introduced
    itself much more useful for this project. There were no limits on how much atoms could be used. It was also possible
    to calculate photoelectron intensities for amorphous clusters to higher orders of scattering. But even more
    than MSCD does EDAC need a lot of memory (especially for amorphous clusters) and the size of the cluster
    was very limited.
  
    In order to be able to simulate very large clusters, which was considered important to simulate
    amorphous materials, it was decided to write a single scattering cluster program. This program was not
    designed to be fast, but to save memory and therefore allow to simulate clusters of the size of about 2000 atoms.
    This program, which was christened YaSC (Yet another Single scattering Cluster program), was written according to
    the theory outlined on the @ref theory "Theory" page. YaSC is a single scattering cluster program, which
    does not account higher order of scattering. Inelastic effects and
    refraction of the signal electrons at the surface are implemented in
    YaSC, while reflection of the electrons at the surface is neglected. Correlated vibrational effects
    and instrumental angular averaging
    are not taken into account yet, but there is no problem to implement them.
    YaSC is a command line interface tool, i.e.
    it doesn't have any graphical user interface. The cluster data and all necessary parameters have to be given in an input file.
    YaSC reads this file, carries out the calculation and writes the result into an output file. YaSC is written
    in C++ and should run on any operating system which supports this language and the standard template library (STL),
    but was only tested on Linux so far.
    A comprehensive \ref manual "manual" is also available.
  
    You can find more information about the background of scattering of electrons in single crystals
    in the chapter \ref theory "Theory".
   
    The format of the input and output files of YaSC may be looked up in the \ref manual "Manual".

  @subsection overview_references
    - MSCD, Multiple Scattering Calculation of Diffraction - http://www.sitp.lbl.gov/index.php?content=/mscdpack/mscdpack.html
    - EDAC, Electron Diffraction in Atomic Clusters - http://electron.lbl.gov/~ccpgaabf/web/software/edac/
 
  @section screenshot Screenshots
 
 
  @section download Download
    The download contains the source of YaSC, muffin tin potentials and radial matrix elements for a certain
    number of elements. A manual which describes the format of the input and output file can be found \ref manual "here".
    
    - yasc20060203: <a href="downloads/yasc20060203.zip">download</a> - initial release of YaSC

  
  @section install Installation
    Makefiles are provided for Linux/Mac OSX/Windows-Cygwin.
    - Download and unzip the YaSC package
    - Cd into the YaSC directory
    - Run "make"
      Parameters for make:
      - USE_GSL=1 : if you want to use the GSL library for some functions (Spherical Harmonics,
        Hankel function, etc.) instead of builtin functions
      - USE_GREEN=1 : if you want to use the Greens library for some functions (Spherical Harmonics,
        Hankel function, etc.) instead of builtin functions
    
  @section todo TODO
    - use LUA as frontend for YaSC
    - More tests about certain features of YaSC
    - include temparature effects
    - include instrumental angular averaging
    - add small atom approximation
    - better docmentation of code
    - allow parallel processing of code for double core or multi processor systems
    - multiple scattering of photoelectrons :)

  @section license License
    For the moment YaSC is distributed under the <a href="http://www.gnu.org/copyleft/gpl.html">GNU General Public License</a> license.
  
  
  @section links Links
    - MSCD, Multiple Scattering Calculation of Diffraction - http://www.sitp.lbl.gov/index.php?content=/mscdpack/mscdpack.html
    - EDAC, Electron Diffraction in Atomic Clusters - http://electron.lbl.gov/~ccpgaabf/web/software/edac/
    - GNU Scientific Library - http://www.gnu.org/software/gsl/
    - Greens Library - http://www.mpi-hd.mpg.de/personalhomes/kovalp/software/greens/
    - Doxygen - http://www.doxygen.org
 
  This homepage was made and the source code was documented with the help of doxygen (http://www.doxygen.org).
  */
