#ifndef __GREENS_TPCS_HPP
#define __GREENS_TPCS_HPP

#include <math.h>
#include <cstring>

#include "greens_tpcsnl.hpp"
#include "greens_tpcsrl.hpp"

//
//
// Two Photon Cross Section Genaral Routine
//
double greens_two_photon_cs(const char* framework, const char* polarization,
                            double E_gamma, const int digits);

#endif
