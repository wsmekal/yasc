#include "greens_wnj.hpp"
/*
   #
   #  This procedure determines whether j1, j2, and j3 form an
   #  angular momentum coupling triangle. A boolean variable of either
   #  true of false is returned.
   #
*/
//
// integer arguments
//
bool Racah_IsTriangle(int j1, int j2, int j3)
{
  if ((!((j1+j2+j3)>=0)) ||
     (j3 > j1 + j2)  || (j1 > j2 + j3)  || (j2 > j3 + j1)) return(false);
  return(true);
}
//
// integer and half-integer
//
bool Racah_IsTriangle(double j1, double j2, double j3){

  if  ((!greens_IsNonNegInteger(j1+j2+j3)) ||
      (j3 > j1 + j2)  || (j1 > j2 + j3)  || (j2 > j3 + j1)) return(false);
  return(true);}
//
// Racah_istriangle() name is exact as Racah package
//
bool Racah_istriangle(double j1, double j2, double j3)
{return(Racah_IsTriangle(j1,j2,j3));}

/*!  
     This procedure is a sub-procedure for the numerical computation
     of 6j-symbols in Racah_computew6j(). It calculates the Delta factor
     of three angular momenta a,b,c as given in R.D. Cowan, "The Theory
     of Atomic Structure and Spectra", University of California Press,
     Berkeley, Los Angeles, London, 1981, Eq. (5.24).
     It is assumed that a,b,c represent angular momenta with
     Racah_istriangle(a,b,c) = true. A numerical value is returned.
*/

double Racah_computeDelta(double a, double b, double c){
   double res;
   res = factorial(a+b-c) * factorial(a-b+c) * 
         factorial(-a+b+c) / factorial(a+b+c+1);
   res = sqrt(res); 
   return(res);}

//
//
//
int greens_angular_l(int kappa)
/*
     Corresponding to
   
             / -kappa-1    if  kappa < 0
        l = <
             \    kappa       if  kappa > 0
*/
{  
  if (kappa > 0) return(kappa);
  if (kappa < 0) return(-kappa-1);
  greens_ERROR(_str_greens_angular_l, _str_wrong_kappa); 
  return(0); 
}

const char* _str_wrong_T = "wrong T";


int greens_angular_l(int kappa, int T)
/*!
   greens_angular_l(kappa, T), where T = [large, small]
*/
{  
  if (T == 0) return(greens_angular_l_large(kappa));
  if (T == 1) return(greens_angular_l_small(kappa));
  greens_ERROR(_str_greens_angular_l, _str_wrong_T); return(0); 
}
//
//
//
int greens_angular_l_fortran(int kappa, int T)
/*!
   greens_angular_l_fortran(kappa, T), where T = +1 (Large) or -1 (Small)
*/
{  
//--------------------------------------------------------------------
// Returns the angular momentum quantum number l for a given \kappa 
// quantum number and superscript T which can be +1 (Large) and -1 (Small)
// components are represented
//
// Calls: 
//--------------------------------------------------------------------
  int res;
  
  if ((T!=-1) && (T!=+1)) greens_ERROR(_str_greens_angular_l, _str_wrong_T); 
  if (kappa == 0) greens_ERROR(_str_greens_angular_l, _str_wrong_kappa); 
      
  res = int(abs(kappa)); 
      if ((T==+1) && (kappa<0)) return(res-1);
      if ((T==-1) && (kappa>0)) return(res-1);
  return(res);
}

//
//
//
const char* _str_greens_invert_T = "greens_invert_T";

int greens_invert_T(int T)
/*!
   greens_invert_T([large, small]) =  [small, large];
*/
{  
  if (T == 0) return(1);
  if (T == 1) return(0);
  greens_ERROR(_str_greens_invert_T, _str_wrong_T); return(0); 
}

//
//
//
int greens_angular_l_large(int kappa)
/*!
     Corresponding to
   
             /  kappa    if  kappa > 0
        l = <
             \ -kappa-1  if  kappa < 0
*/
{  
  if (kappa > 0) return(kappa);
  if (kappa < 0) return(-kappa-1);
  greens_ERROR(_str_greens_angular_l, _str_wrong_kappa); 
  return(0); 
}
//
//
//
int greens_angular_l_small(int kappa)
/*!
     Corresponding to
   
             /  kappa-1  if  kappa > 0
        l = <
             \ -kappa    if  kappa < 0
*/
{  
  if (kappa > 0) return(kappa-1);
  if (kappa < 0) return(-kappa);
  greens_ERROR(_str_greens_angular_l, _str_wrong_kappa); 
  return(0); 
}

const char* _str_something_is_wrong = "something is wrong";
const double wnj_lexact = 1e-14;

double greens_w3j (int j1, int j2, int j3, 
                  int m1, int m2, int m3)
/*! 
    Calculates Wigner 3j-symbol
    Translated from Maple-package Racah
*/
{
  double res, sum; 
  int k, kmax, kmin, absm1, absm2, absm3;

  absm1=m1; absm2=m2; absm3=m3;
  if (absm1<0) absm1=-absm1; 
  if (absm2<0) absm2=-absm2; 
  if (absm3<0) absm3=-absm3;
  
  if ((m1+m2!=-m3) || !((j1+j2>=j3)&(j2+j3>=j1)&(j1+j3>=j2))) {return(0.0);};
  if ((j1<absm1) || (j2<absm2) || (j3<absm3)) {return(0.0);};

  res =sqrt(
       factorial(j1+j2-j3)*factorial(j1-j2+j3)*factorial(-j1+j2+j3)*factorial(j1-m1)*
       factorial(j1+m1)*factorial(j2-m2)*factorial(j2+m2)*factorial(j3-m3)*factorial(j3+m3)/
       factorial(j1+j2+j3+1))*pow_minus(int(j1-j2-m3));
       
       kmax = min(j1+j2-j3, j1-m1, j2+m2);
       kmin = max(0, j2-j3-m1, j1-j3+m2);
       
       sum = 0.0;
       for (k=kmin; k<=kmax; k++){
         sum=sum+pow_minus(k)/
	 (factorial(k)*factorial(j1+j2-j3-k)*factorial(j1-m1-k)*
          factorial(j2+m2-k)*factorial(j3-j2+m1+k)*factorial(j3-j1-m2+k));}
       res=res*sum; 
       if (abs(res)>wnj_lexact) return(res);
       return (0.0);
}
//
// w3j integer and half-integer
//
double greens_w3j (double j1, double j2, double j3, 
                  double m1, double m2, double m3)
{
  int kmax, kmin;
  double sum, res; 
  
  if (!greens_IsInteger(j1+j1) || !greens_IsInteger(j2+j2) ||
      !greens_IsInteger(j3+j3) || !greens_IsInteger(m1+m1) ||
      !greens_IsInteger(m2+m2) || !greens_IsInteger(m3+m3)) 
      greens_ERROR(_str_greens_w3j, _str_args_maybe_integer_or_halfinteger); 
  
    
  if (!greens_IsNonNegInteger(j1+j1) || !greens_IsNonNegInteger(j2+j2) ||
      !greens_IsNonNegInteger(j3+j3) || (m1+m2+m3 !=0) || !((j1+j2>=j3)&(abs(j1-j2)<=j3)) || 
      !greens_IsNonNegInteger(j1+m1) || !greens_IsNonNegInteger(j1-m1) ||
      !greens_IsNonNegInteger(j2+m2) || !greens_IsNonNegInteger(j2-m2) ||
      !greens_IsNonNegInteger(j3+m3) || !greens_IsNonNegInteger(j3-m3))     
     {return(.0);};

  
  res =pow_minus(int(j1-j2-m3))*sqrt(
       factorial(int(j1+j2-j3))*factorial(int(j1-j2+j3))*
       factorial(int(-j1+j2+j3))*
       factorial(int(j1-m1))*factorial(int(j1+m1))*
       factorial(int(j2-m2))*factorial(int(j2+m2))*
       factorial(int(j3-m3))*factorial(int(j3+m3))/
       factorial(int(j1+j2+j3+1)));

       
       kmax = min(int(j1+j2-j3), int(j1-m1), int(j2+m2));
       kmin = max(0, int(j2-j3-m1), int(j1-j3+m2));
       sum = .0;
       for (int k=kmin; k<=kmax; k=k+1){
         sum=sum+pow_minus(k)/
	    (factorial(k)*factorial(int(j1+j2-j3-k))*
	     factorial(int(j1-m1-k)) * factorial(int(j2+m2-k))*
             factorial(int(j3-j2+m1+k)) * factorial(int(j3-j1-m2+k)));}
       res=res*sum; 
       if (abs(res)>wnj_lexact) return(res);
       return (.0);
}

//
//  w6j  integer and half-integer
//
double greens_w6j(double j1, double j2, double j3, 
                 double l1, double l2, double l3)
{
/* #  performs a numerical evaluation of a Wigner 6-j symbol
   #  if all arguments are given numerically */

   double res, sum;
   double kmax, kmin;
   int k;
      
   if (!greens_IsNonNegInteger(j1+j1) || !greens_IsNonNegInteger(j2+j2) ||
       !greens_IsNonNegInteger(j3+j3) || !greens_IsNonNegInteger(l1+l1) ||
       !greens_IsNonNegInteger(l2+l2) || !greens_IsNonNegInteger(l3+l3)) 
       greens_ERROR(_str_greens_w6j, _str_args_maybe_integer_or_halfinteger); 

      
   if (!Racah_IsTriangle(j1,j2,j3) ||
       !Racah_IsTriangle(j1,l2,l3) ||
       !Racah_IsTriangle(j2,l1,l3) ||
       !Racah_IsTriangle(l1,l2,j3)) return(0.0);

/*    #  numerical evaluation taken from Racah program:*/   

      res = Racah_computeDelta(j1,j2,j3)
          * Racah_computeDelta(j1,l2,l3)
          * Racah_computeDelta(l1,j2,l3)
          * Racah_computeDelta(l1,l2,j3);
      //
      kmin = max(j1+j2+j3, j1+l2+l3, l1+j2+l3, l1+l2+j3);
      kmax = min(j1+j2+l1+l2, j2+j3+l2+l3, j3+j1+l3+l1);
      //
      sum = 0.0;
      if (kmin <= kmax) {
         for(k=int(kmin); k<=kmax; k++)
         { sum = sum + pow_minus(k) * factorial(k+1)  /
                   (  factorial(k-j1-j2-j3) * factorial(k-j1-l2-l3)
                    * factorial(k-l1-j2-l3) * factorial(k-l1-l2-j3)
                    * factorial(j1+j2+l1+l2-k) * factorial(j2+j3+l2+l3-k)
                    * factorial(j3+j1+l3+l1-k) );}}
                    
    else {fprintf(stderr, "%s\n", _str_something_is_wrong);};

      res = res*sum;
      if (abs(res)>wnj_lexact) return(res);
      return (0.0);}

//
//  w9j symbol  integer and half-integer
//

double greens_w9j(double j11, double j12, double j13, 
                 double j21, double j22, double j23,
                 double j31, double j32, double j33)
{
/* #
   #  performs a numerical evaluation of a Wigner 9-j symbol
   #  if all arguments are given numerically
   #*/
   double res;
   double kmin, kmax, j, k, w1, w2, w3;

   if (!greens_IsNonNegInteger(j11+j11) || !greens_IsNonNegInteger(j12+j12) ||
       !greens_IsNonNegInteger(j13+j13) || !greens_IsNonNegInteger(j21+j21) ||
       !greens_IsNonNegInteger(j22+j22) || !greens_IsNonNegInteger(j23+j23) ||
       !greens_IsNonNegInteger(j31+j31) || !greens_IsNonNegInteger(j32+j32) ||
       !greens_IsNonNegInteger(j33+j33)) 
       greens_ERROR(_str_greens_w9j, _str_args_maybe_integer_or_halfinteger); 


      //  further error checking will be done in greens_evalf_w6j
      if (!Racah_IsTriangle (j11, j12, j13) ||
          !Racah_IsTriangle (j21, j22, j23) ||
          !Racah_IsTriangle (j31, j32, j33) ||
          !Racah_IsTriangle (j11, j21, j31) ||
          !Racah_IsTriangle (j12, j22, j32) ||
          !Racah_IsTriangle (j13, j23, j33)){
            // the arguments have to fulfill the angular momentum coupling conditions:
          return(0.0);};
/*
      #
      #  numerical evaluation taken from Racah program:
      #
*/

      kmin = max(abs(j11-j33),abs(j12-j23),abs(j32-j21));
      kmax = min(j11+j33,j12+j23,j32+j21);
      k    = kmin;
      res  = 0.0;
      if  (kmin <= kmax){
         for(j=kmin+kmin; j<=(kmax+kmax); j=j+2.0){
	   w1 = greens_w6j(j11,j21,j31,j32,j33,k); if (w1 == 0.0) continue;
	   w2 = greens_w6j(j12,j22,j32,j21,k,j23); if (w2 == 0.0) continue;
	   w3 = greens_w6j(j13,j23,j33,k,j11,j12); if (w3 == 0.0) continue;
           res = res + pow_minus(int(j))*(j+1)*w1*w2*w3;
           k = k + 1.0;}}
   else fprintf(stderr, "%s\n", _str_something_is_wrong); 
      
      if (abs(res)>wnj_lexact) return(res);
      return (0.0);}
//
// Clebsch-Gordan symbol  integer and half-integer
//
double greens_ClebschGordan(double j1, double m1, double j2, 
                           double m2, double J, double M)
{
  if (!greens_IsInteger(j1 +j1) || !greens_IsInteger(j2 +j2) ||
      !greens_IsInteger(J  +J) || !greens_IsInteger(m1 +m1) ||
      !greens_IsInteger(m2 +m2) || !greens_IsInteger(M  +M)) 
      greens_ERROR(_str_greens_ClebschGordan, _str_args_maybe_integer_or_halfinteger); 
 
  return(pow_minus(int(j1-j2+M))*sqrt(J+J+1)*greens_w3j(j1,j2,J,m1,m2,-M));
}

//
// w3j without checking
//
double _greens_w3j (double j1, double j2, double j3, 
                   double m1, double m2, double m3)
{
  int kmax, kmin;
  double sum, res; 
   
  if (!greens_IsNonNegInteger(j1+j1) || !greens_IsNonNegInteger(j2+j2) ||
      !greens_IsNonNegInteger(j3+j3) || (m1+m2+m3 !=0) || 
      !((j1+j2>=j3)&&(abs(j1-j2)<=j3)) || 
      !greens_IsNonNegInteger(j1+m1) || !greens_IsNonNegInteger(j1-m1) ||
      !greens_IsNonNegInteger(j2+m2) || !greens_IsNonNegInteger(j2-m2) ||
      !greens_IsNonNegInteger(j3+m3) || !greens_IsNonNegInteger(j3-m3))     
     {return(.0);};

  
  res =pow_minus(int(j1-j2-m3))*sqrt(
       _factorial(int(j1+j2-j3))*
       _factorial(int(j1-j2+j3))*_factorial(int(-j1+j2+j3))
      *_factorial(int(j1-m1))*_factorial(int(j1+m1))
      *_factorial(int(j2-m2))*_factorial(int(j2+m2))
      *_factorial(int(j3-m3))*_factorial(int(j3+m3))/
       _factorial(int(j1+j2+j3+1)));

       
       kmax = min(int(j1+j2-j3), int(j1-m1), int(j2+m2));
       kmin = max(0, int(j2-j3-m1), int(j1-j3+m2));
       sum = .0;
       for (int k=kmin; k<=kmax; k=k+1){
         sum=sum+pow_minus(k)/(_factorial(k)*_factorial(int(j1+j2-j3-k))
	                     * _factorial(int(j1-m1-k)) * _factorial(int(j2+m2-k))
			     * _factorial(int(j3-j2+m1+k)) * _factorial(int(j3-j1-m2+k)));}
       res=res*sum; 
       if (abs(res)>wnj_lexact) return(res);
       return (.0);
}

//
// Clebsch-Gordan symbol without cheching
//
double _greens_ClebschGordan(double j1, double m1, double j2, 
                            double m2, double J, double M)
{
  return(pow(-1.0,j1-j2+M)*sqrt(J+J+1)*_greens_w3j(j1,j2,J,m1,m2,-M));
}


//
// w6j without checking
//

double _greens_w6j(double j1, double j2, double j3, 
                  double l1, double l2, double l3)
{
/* #  performs a numerical evaluation of a Wigner 6-j symbol
   #  if all arguments are given numerically */

   double res, sum;
   double kmax, kmin, k;
     
   if (!Racah_IsTriangle(j1,j2,j3) ||
       !Racah_IsTriangle(j1,l2,l3) ||
       !Racah_IsTriangle(j2,l1,l3) ||
       !Racah_IsTriangle(l1,l2,j3)) return(0.0);

/*    #  numerical evaluation taken from Racah program:*/   

      res = Racah_computeDelta(j1,j2,j3)
          * Racah_computeDelta(j1,l2,l3)
          * Racah_computeDelta(l1,j2,l3)
          * Racah_computeDelta(l1,l2,j3);
      //
      kmin = max(j1+j2+j3, j1+l2+l3, l1+j2+l3, l1+l2+j3);
      kmax = min(j1+j2+l1+l2, j2+j3+l2+l3, j3+j1+l3+l1);
      //
      sum = 0.0;
      if (kmin <= kmax) {
         for(k=kmin; k<=kmax; k=k+1.0)
         { sum = sum + pow(-1.0,k) * _factorial(int(k+1))  /
                   (  _factorial(int(k-j1-j2-j3)) * _factorial(int(k-j1-l2-l3))
                    * _factorial(int(k-l1-j2-l3)) * _factorial(int(k-l1-l2-j3))
                    * _factorial(int(j1+j2+l1+l2-k)) * _factorial(int(j2+j3+l2+l3-k))
                    * _factorial(int(j3+j1+l3+l1-k)) );}}
                    
    else {fprintf(stderr, "%s\n", _str_something_is_wrong);};

      res = res*sum;
      if (abs(res)>wnj_lexact) return(res);
      return (0.0);}


//
// w9j without checking
//

double _greens_w9j(double j11, double j12, double j13, 
                  double j21, double j22, double j23,
                  double j31, double j32, double j33)
{
/* #
   #  performs a numerical evaluation of a Wigner 9-j symbol
   #  if all arguments are given numerically
   #*/
   double res;
   double kmin, kmax, j, k, w1, w2, w3;

      //  further error checking will be done in greens_evalf_w6j
      if (!Racah_IsTriangle (j11, j12, j13) ||
          !Racah_IsTriangle (j21, j22, j23) ||
          !Racah_IsTriangle (j31, j32, j33) ||
          !Racah_IsTriangle (j11, j21, j31) ||
          !Racah_IsTriangle (j12, j22, j32) ||
          !Racah_IsTriangle (j13, j23, j33)){
            // the arguments have to fulfill the angular momentum coupling conditions:
          return(0.0);};
/*
      #
      #  numerical evaluation taken from Racah program:
      #
*/

      kmin = max(abs(j11-j33),abs(j12-j23),abs(j32-j21));
      kmax = min(j11+j33,j12+j23,j32+j21);
      k    = kmin;
      res  = 0.0;
      if  (kmin <= kmax){
         for(j=kmin+kmin; j<=kmax+kmax; j=j+2.0){
	   w1 = _greens_w6j(j11,j21,j31,j32,j33,k); if (w1 == 0.0) continue;
	   w2 = _greens_w6j(j12,j22,j32,j21,k,j23); if (w2 == 0.0) continue;
	   w3 = _greens_w6j(j13,j23,j33,k,j11,j12); if (w3 == 0.0) continue;
           res = res + pow_minus(int(j))*(j+1)*w1*w2*w3;
           k = k + 1.0;}}
   else fprintf(stderr, "%s\n", _str_something_is_wrong); 
      
      if (abs(res)>wnj_lexact) return(res);
      return (0.0);
}
