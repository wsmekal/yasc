#include "greens_mdclass.hpp"
//
//
//
void print(spinor2_col s2) 
{
  fprintf(greens_stdout, "\n/ %#.12E \\\n\\ %#.12E /\n", s2.L, s2.S); 
  fflush(greens_stdout);
}
//
//
//
void print(spinor2_row s2) 
{
  fprintf(greens_stdout, "< %#.12E, %#.12E >\n", s2.L, s2.S);
  fflush(greens_stdout);
}
//
//
//
spinor2_row htranspose(spinor2_col sv){return(spinor2_row(sv.L, sv.S));}
//
//
//
double operator *(spinor2_row sh, spinor2_col sv) 
{
  return(sv.L*sh.L+sv.S*sh.S);
}
//
// last_res  version for matrix_2x2
//
// matrix_2x2 last_mres =matrix_2x2(0,0,0,0);
//
void print(matrix_2x2 m)
{
  fprintf(greens_stdout, "\n| %#.12E, %#.12E |\n| %#.12E, %#.12E |\n", 
  m.e[0][0], m.e[0][1], m.e[1][0], m.e[1][1]); fflush(greens_stdout);
}
//
//
//
bool operator ==(matrix_2x2 a, matrix_2x2 b)
{
  return((a.e[0][0]==b.e[0][0])&&(a.e[0][1]==b.e[0][1])&&
         (a.e[1][0]==b.e[1][0])&&(a.e[1][1]==b.e[1][1]));
}
//
//
//
matrix_2x2 operator +(matrix_2x2 a, matrix_2x2 b)
{ 
  matrix_2x2 c; 
  for (int i=0; i<2; i++){
    for (int j=0; j<2; j++){ 
      c.e[i][j]=a.e[i][j]+b.e[i][j];}}
  return(c);
}
//
//
//
matrix_2x2 operator -(matrix_2x2 a, matrix_2x2 b)
{ 
  matrix_2x2 c; 
  for (int i=0; i<2; i++){
    for (int j=0; j<2; j++){
      c.e[i][j]=a.e[i][j]-b.e[i][j];}}
  return(c);
}
//
//
//
matrix_2x2 operator *(double a, matrix_2x2 b)
{ 
  matrix_2x2 c;
  for (int i=0; i<2; i++){
    for (int j=0; j<2; j++){
      c.e[i][j]=a*b.e[i][j];}} 
  return(c);
}
//
//
//
matrix_2x2 operator *(matrix_2x2 a, double b)
{ 
  matrix_2x2 c; 
  for (int i=0; i<2; i++){
    for (int j=0; j<2; j++){
      c.e[i][j]=a.e[i][j]*b;}} 
  return(c);
}
//
//
//
matrix_2x2 operator /(matrix_2x2 a, double b)
{ 
  matrix_2x2 c; 
  for (int i=0; i<2; i++){
    for (int j=0; j<2; j++){
      c.e[i][j]=a.e[i][j]/b;}} 
  return(c);
}
//
//
//
matrix_2x2 operator *(matrix_2x2 a, matrix_2x2 b)
{ 
  matrix_2x2 c; int k; 
  for (int i=0; i<2; i++){
    for (int j=0; j<2; j++){
      k=0; 
      do {c.e[i][j]=c.e[i][j]+a.e[i][k]*b.e[k][j]; k=k+1;} while(k<2);}}
  return(c);
}
//
//
//
spinor2_col operator *(matrix_2x2 a, spinor2_col b)
{ 
  spinor2_col c; 
  c.L=a.e[0][0]*b.L+a.e[0][1]*b.S; 
  c.S=a.e[1][0]*b.L+a.e[1][1]*b.S;
  return(c);
}
//
//
//
spinor2_row operator *(spinor2_row sh, matrix_2x2 mb)
{ 
  spinor2_row sc; 
  sc.L=sh.L*mb.e[0][0]+sh.S*mb.e[1][0]; 
  sc.S=sh.L*mb.e[0][1]+sh.S*mb.e[1][1];
  return(sc);
}
//
//
//
matrix_2x2 operator *(spinor2_col sv, spinor2_row sh)
{ 
  matrix_2x2 m; m.e[0][0]=sv.L*sh.L; m.e[0][1]=sv.L*sh.S; 
  m.e[1][0]=sv.S*sh.L; m.e[1][1]=sv.S*sh.S;
  return(m);
}
//
//
//
matrix_2x2 div_mem (matrix_2x2 a, matrix_2x2 b)
{ 
  matrix_2x2 c; 
  for(int i=0; i<2; i++){
    for (int j=0; j<2; j++) {
      c.e[i][j]=a.e[i][j]/b.e[i][j];};}
  return(c);
}
//
//
//
matrix_2x2 abs_mem(matrix_2x2 a)
{ 
  matrix_2x2 c; 
  for(int i=0; i<2; i++){
    for(int j=0; j<2; j++){
      c.e[i][j]=abs(a.e[i][j]);}}
  return(c);
}
//
//
//
double max_mem(matrix_2x2 a)
{ 
  double m=a.e[0][0]; 
  for(int i=0; i<2; i++){
    for (int j=0; j<2; j++){
      if (a.e[i][j]>m) m=a.e[i][j];}}
    return(m);
}
//
//
//
matrix_2x2 last_mdres(0.0, 0.0, 0.0, 0.0);
