#ifndef __GREENS_MDCLASS_HPP
#define __GREENS_MDCLASS_HPP

#include "greens_service.hpp"


/*! 
   Represents a vertical 2-spinor
   used by greens_radial_spinor() function
   L and S are Large and Small components of the spinor
*/
class spinor2_col {
  /*!
     L and S are Large and Small components of a spinor   
  */    
  public: double L, S; //large and small component (vertical spinor)
  spinor2_col(double a=(double)(0.0), double b=(double)(0.0)) { L=a; S=b; }
  spinor2_col operator =(spinor2_col a) {this->L=a.L; this->S=a.S; return(*this);}};

/*! 
   Represents a horizontal 2-spinor
   used by greens_radial_spinor() function
*/
class spinor2_row {
  /*!
     L and S are Large and Small components of a spinor   
  */  
  public: double L, S; //large and small component of the horizontal spinor
  spinor2_row(double a=(double)(0.0), double b=(double)(0.0)) {L=a; S=b;}
  spinor2_row operator =(spinor2_row a) {this->L=a.L; this->S=a.S; return(*this);}};

//!
// Prints a vertical spinor
//  
  void print(spinor2_col s2);

/*!
 Prints a horizontal spinor
*/  
  void print(spinor2_row s2);

/*! 
   Does a transposition of a vertical 2-spinor to horizontal and 
   conjugates the members
*/
  spinor2_row htranspose(spinor2_col sv);

/*! 
   Does  a multiplication of the horizontal * vertical
   result is a double number
*/
  double  operator *(spinor2_row sh, spinor2_col sv);

/*! 
   Represents a 2x2-matrix 
   used by greens_radial_Green_matrix() function
*/

class matrix_2x2 
{
  public: double e[2][2];     //matrix elemnts
                   
  matrix_2x2(double a11=0.0, double a12=0.0, double a21=0.0, double a22=0.0) { 
    e[0][0]=a11; e[0][1]=a12; e[1][0]=a21; e[1][1]=a22; }
  
  matrix_2x2 operator =(matrix_2x2 a) {
    this->e[0][0]=a.e[0][0]; this->e[0][1]=a.e[0][1];
    this->e[1][0]=a.e[1][0]; this->e[1][1]=a.e[1][1];
    return(*this);}
  matrix_2x2 operator =(double a) {
    this->e[0][0]=a; this->e[0][1]=0.0;
    this->e[1][0]=0.0; this->e[1][1]=a;
    return(*this);}
};
//
//
//
void print(matrix_2x2 m);


bool operator ==(matrix_2x2 a, matrix_2x2 b);
  
matrix_2x2 operator +(matrix_2x2 a, matrix_2x2 b);
matrix_2x2 operator -(matrix_2x2 a, matrix_2x2 b);

matrix_2x2 operator *(double a, matrix_2x2 b);
matrix_2x2 operator *(matrix_2x2 a, double b);

matrix_2x2 operator /(matrix_2x2 a, double b);
matrix_2x2 operator *(matrix_2x2 a, matrix_2x2 b);
spinor2_col operator *(matrix_2x2 a, spinor2_col b);
spinor2_row operator *(spinor2_row sh, matrix_2x2 mb);

matrix_2x2 operator *(spinor2_col sv, spinor2_row sh);

matrix_2x2 div_mem (matrix_2x2 a, matrix_2x2 b);

matrix_2x2 abs_mem(matrix_2x2 a);

double max_mem(matrix_2x2 a);

extern matrix_2x2 last_mdres;

#endif
