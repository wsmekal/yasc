#include "greens.h"

double funct (double x){return(sin(x));}
//
// --------- Main Program ----------------------------------------
//
int main(void)
{  
  double a=0.0, E_T;
  double b=2.5; short int i = 0;
  
  printf("%e %e\n", greens_constant_au_E0, Pi);
  
  print(greens_isinf(b));
  
  print(Pi);
  print(greens_dexact);
  print(_str_debug_reason);
  
  print(matrix_2x2(0,1,1,1));  
  
  print(greens_Gauss_Legendre_Int(funct, a, b, 6));

/*  
  print(greens_radial_spinor(2,-1, 2.3));
  print(greens_radial_orbital(2, 0, 2.3));
  print(greens_radial_Green_matrix(-0.25,-1, 2.3, 2.5));
  print(greens_radial_Green_function(-0.25, 0, 2.3, 2.5)); 
                                                          */
  
  greens_set_nuclear_charge(100.0);
  E_T = -greens_energy(1,-1);
  
  print(greens_TPCSN_lw_circ(E_T/2.0 * 1.10, 3));
  print(greens_TPCSR_lw_circ(E_T/2.0 * 1.10, 3));  
  
  return 0;
}

