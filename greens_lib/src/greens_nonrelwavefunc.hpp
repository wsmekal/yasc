#ifndef __GREENS_NONRELWAVEFUNC_HPP
#define __GREENS_NONRELWAVEFUNC_HPP

#include "greens_specfunc.hpp"

/*!
  The function returns energy of a bound state of
  the nonrelativistic Coulomb problem 
*/
double greens_energy(int n);

/*!
  The function returns the nonrelativistic radial bound orbital P_nl(r)
  for a given energy level n and symmetry (angular momentum ) l
  
  Usual many-body definition for radial orbitals is supported. 
  It binds to definition which is usual in one-electron problem
  as R_nl(r)=P_nl(r)/r
  
  Nuclear charge can be defined by using 
  greens_set_nuclear_charge() function
*/
double greens_radial_orbital(int n, int l, double r);

/*!
  The function returns the nonrelativistic radial free orbital P_El(r)
  for a given energy E and symmetry (angular momentum ) l
  
  Usual many-body definition for radial orbitals is supported. 
  It binds to definition which is usual in one-electron problem
  as R_El(r)=P_El(r)/r
  
  The function is "energy-normalized" 
  \int_0^{\infty} P_{E,l} P_{E',l}\, dr = delta(E-E')
  
  Nuclear charge can be defined by using 
  greens_set_nuclear_charge() function
*/
double greens_radial_orbital(double E, int l, double r);

#endif
