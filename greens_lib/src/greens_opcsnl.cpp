#include "greens_opcsnl.hpp"
//
// one photon cross section sigma_1 from state (n,l,m)
// to the state (n_f),  by polarisation of light  lambda={-1,0,1}
// nonrelativistic int wave approx [sigma_1] cm^2
//
double greens_nonrel_OPCS(int n_f, int lambda, int n, int l, int m)
{
 int absm=m; if (m<0) absm=-m;
 double sum;
 
 if (n_f<1)  greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_n);
 if (n<1)    greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_n);
 if (l<0)    greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_l);
 if (n<=l)   greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_n_or_l);
 if (absm>l) greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_m);

 sum=0.0;
 for (int l_f=0; l_f<n_f; l_f++){ 
   for(int m_f=-l_f; m_f<=l_f; m_f++){ 
     sum=sum+pow(greens_nonrel_bound_bound_me(n_f,l_f,m_f,lambda,n,l,m), 2);}};

 last_dres=sum*abs(greens_energy(n_f)-greens_energy(n))*4.0*pow(Pi, 2)*
              pow(greens_constant_cgs_hbar, 3)/
             (pow(greens_constant_cgs_me, 2) * greens_constant_cgs_c * 
              pow(greens_constant_cgs_e,  2));

 return(last_dres);

}

//
// one photon cross section sigma_1 from state (n,l,m)
// to the state (E_f),  by polarisation of light  lambda={-1,0,1}
// nonrelativistic int wave approx [sigma_1] cm^2
//
double greens_nonrel_OPCS(double E_f, int lambda, int n, int l, int m, int digits)
{

 int absm=m; if (m<0) absm=-m;
 
 if (E_f<0.0) greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_energy);
 if (n<1) greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_n);
 if (l<0) greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_l);
 if (n<=l) greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_n_or_l);
 if (absm>l) greens_ERROR(_str_greens_nonrel_OPCS, _str_wrong_m);

 double sum=0.0; 

 for (int l_f=l-1; l_f<=l+1; l_f++){ 
   for(int m_f=-l_f; m_f<=l_f; m_f++){
     sum=sum+pow(greens_nonrel_free_bound_me(E_f,l_f,m_f,lambda,n,l,m,digits), 2);}};
 
 last_dres=sum*abs(E_f-greens_energy(n))*4.0*pow(Pi, 2)*pow(greens_constant_cgs_hbar, 3)/
          (pow(greens_constant_cgs_me, 2)*greens_constant_cgs_c*pow(greens_constant_cgs_e,  2));


 return(last_dres);
}

