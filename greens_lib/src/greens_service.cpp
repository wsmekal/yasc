//
// Redirection file <-> terminal
//
#include "greens_service.hpp"

#ifdef so_lib
  int errorbreak = NoBreak; 
#else 
  int errorbreak = Break; 
#endif


FILE *greens_stdin=stdin;
FILE *greens_stdout=stdout;
char *_str_stdout;
FILE *greens_stderr=stderr;

//
// ------------------- strings ----------------------------------------
//
const char* _str_Digits="Digits =";

//
// ------------------- greens_ERROR ------------------------------------
//
// ------------------- Names of the functions--------------------------
const char* _str_greens_message = "#Dirac message";
const char* _str_greens_L = "greens_L";
const char* _str_greens_F2 = "greens_F2";
const char* _str_greens_nonrel_radial_me = "greens_nonrel_radial_me";
const char* _str_greens_cKummerM = "greens_cKummerM";
const char* _str_greens_rKummerM = "greens_rKummerM";
const char* _str_greens_spherical_j = "greens_spherical_j";
const char* _str_greens_Clenshaw_Curtis_Int = "greens_Clenshaw_Curtis_Int";
const char* _str_greens_Gauss_Legendre_Polar_Int=
                              "greens_Gauss_Legendre_Polar_Int";
const char* _str_set_Digits = "set_Digits";
const char* _str_greens_add_new_argument = "greens_add_new_argument";
const char* _str_greens_energy_width_H_non = "greens_energy_width_H_non";
const char* _str_greens_lifetime_H_non = "greens_lifetime_H_non";
const char* _str_greens_A_n1l1n2l2 = "greens_A_n1l1n2l2";
const char* _str_greens_A_nl = "greens_A_nl";
const char* _str_greens_nonrel_OPCS = "greens_nonrel_OPCS";
const char* _str_greens_TPCSN = "greens_TPCSN";
const char* _str_greens_w3j = "greens_w3j";
const char* _str_greens_w6j = "greens_w6j";
const char* _str_greens_w9j = "greens_w9j";
const char* _str_greens_angular_l = "greens_angular_l";
const char* _str_greens_ClebschGordan = "greens_ClebschGordan";
const char* _str_greens_reduced_me    = "greens_reduced_me";
const char* _str_greens_Ylm = "greens_Ylm";
const char* _str_greens_radial_Green_function = "greens_radial_Green_function";
const char* _str_greens_radial_Green_function_Drake = 
                                     "greens_radial_Green_function_Drake";
const char* _str_radial_Green_matrix="radial_Green_matrix";
const char* _str_radial_Green_matrix_Drake="radial_Green_matrix_Drake";
const char* _str_radial_spinor = "radial_spinor";
const char* _str_greens_energy = "greens_energy";
const char* _str_greens_TPCSR = "greens_TPCSR";
const char* _str_greens_xi = "greens_xi";


// ------------------- description of the errors-----------------------
const char* _str_greens_error = "#greens error";
const char* _str_ERROR = "#ERROR";
const char* _str_wrong_args  = "wrong arg(s)";
const char* _str_wrong_r  = "wrong r";
const char* _str_singularity = "singularity";
const char* _str_wrong_n = "wrong n";
const char* _str_wrong_E = "wrong E";
const char* _str_wrong_energy = "wrong energy";
const char* _str_wrong_kappa = "wrong kappa";
const char* _str_wrong_coords = "wrong coord(s)";
const char* _str_wrong_l = "wrong l";
const char* _str_wrong_n_or_l = "wrong n or l";
const char* _str_wrong_m = "wrong m";
const char* _str_wrong_Digits = "wrong Digits";
const char* _str_wrong_nuclear_charge = "wrong nuclear charge";
const char* _str_wrong_Raum =  "wrong Raum";
const char* _str_wrong_Order = "wrong Order";
const char* _str_wrong_state = "wrong state";
const char* _str_routine_error = "routine error";
const char* _str_wrong_Begin_order="wrong Begin order";
const char* _str_wrong_Begin_Raum="wrong Begin Raum";
const char* _str_args_maybe_integer_or_halfinteger = 
                                  "args maybe integer or halfinteger";
const char* _str_wrong_electric = "wrong electric";
const char* _str_wrong_magnetic = "wrong marnetic";
const char* _str_wrong_lambda = "wrong lambda";

//
// ------------------- greens_WARNING -----------------------------------------
//
const char* _str_greens_warning = "#Dirac warning";
const char* _str_WARNING = "#WARNING";
const char* _str_precision_not_reached = "precision not reached";
const char* _str_all_methods_wrong = "all methods wrong";
const char* _str_debug_reason = "debug reason";
const char* _str_Debug = "Debug";
const char* _str_main = "main";
const char* _str_wrong_method = "wrong method";
const char* _str_too_much_records="too_much_records";
const char* _str_perhaps_series_not_convergent="perhaps series not convergent";
const char* _str_final_state_lay_down = "final state lay down";


//
// ------------------- Digits variable and respective function ---------------
//
int ngreens_save_Digits=10; 
int Digits=10;
double greens_save_dexact=1e-10;
double greens_dexact=1e-10;

// ------------------- Dirac physical data ---------------------------------

double greens_save_nuclear_charge=1.0;
const char* _str_set_nuclear_charge="set_nuclear_charge";

//-------------------- Last error ---------------------------------------
//-------------------- Last correctness of the method ------------------
//
double      last_derr = 0.0;   // double error estimation
bool        last_corr = true;  // correctness 
bool        last_att  = false; // attention (last_corr and 
                               // last_err cannot be estimate)
//
//-------------------- Last results ------------------------------------------
dcomplex    last_cdres;
double      last_dres;

double greens_get_last_res(void){return(last_dres);}
double greens_get_last_err(void){return(last_derr);}

bool   greens_get_last_corr(void){return(last_corr);}
//
//-------------------- greens_MESSAGE ----------------------------------------
//

int warningbreak = NoBreak;
int printlevel = 0; int _greens_Err_Code = 0;
int save_errlevel, save_warnlevel, save_msglevel, 
    save_errorbreak, save_warningbreak;

int warnlevel = Print; int msglevel = Print; int errlevel = Print;

void greens_set_warnlevel(int w){
  fprintf(greens_stdout, "#warnlevel set to %d\n", w);
  warnlevel = w;}
  
int greens_get_warnlevel(void){return(warnlevel);}

//
//
void writeto(char *_str){greens_stdout=fopen(_str, "w+"); _str_stdout=_str;}
void writeto(int term) {if (term==terminal) 
{if (greens_stdout!=stdout) fclose(greens_stdout); greens_stdout=stdout;}};

void appendto(char *_str){greens_stdout=fopen(_str, "a+"); _str_stdout=_str;}

// integer ?
bool greens_IsEven(long z){return(!(z&1));}
bool greens_IsOdd(long  z){return(z&1);}

//
//
//
bool greens_IsInteger(double z){return(((z-ceil(z))==0.0));}
bool greens_IsPosInteger(double z){return(((z-ceil(z))==0.0)&&z>0.0);}
bool greens_IsNonNegInteger(double z){return(((z-ceil(z))==0.0)&&z>=0.0);}
bool greens_IsNonPosInteger(double z){return(((z-ceil(z))==0.0)&&z<=0.0);}
bool greens_IsEven(int z){return(!(z&1));}
bool greens_IsOdd(int  z){return(z&1);}

bool greens_IsPositive(double z){return(z>0.0);}
bool greens_IsNegative(double z){return(z<0.0);}
bool greens_IsComplex(dcomplex z){return(z.im!=0.0);}
bool greens_IsComplex(double z){return(false);}
//
// print write and writeln functions
//
void print(bool z) {if (z) fprintf(greens_stdout, "true\n"); 
                  else fprintf(greens_stdout, "false\n"); 
		       fflush(greens_stdout);}
void writeln(bool z) {if (z) fprintf(greens_stdout, "true\n"); 
                    else fprintf(greens_stdout, "false\n"); 
		         fflush(greens_stdout);}
void write(bool z) {if (z) fprintf(greens_stdout, "true "); 
                  else fprintf(greens_stdout, "false "); 
		       fflush(greens_stdout);}

void print(dcomplex a) {char sr[2], si[2]; sr[0]=43; sr[1]=0; 
                             si[0]=43; si[1]=0;
if (a.re<0) sr[0]=45; if (a.im<0) si[0]=45; 
fprintf(greens_stdout, "%s%#.10Ee%s%#.10Ee*I\n",sr,double(abs(a.re)),si,
                                                   double(abs(a.im))); 
fflush(greens_stdout);}

void writeln(dcomplex a) {print(a);}

void write(dcomplex a) {char sr[2], si[2]; sr[0]=43; sr[1]=0; 
si[0]=43; si[1]=0; if (a.re<0) sr[0]=45; if (a.im<0) si[0]=45; 
fprintf(greens_stdout, "%s%#.10Ee%s%#.10Ee*I ",sr,
        double(abs(a.re)),si,double(abs(a.im))); 
fflush(greens_stdout);}

void print(double a){fprintf(greens_stdout, "%#.10E\n",a);
                     fflush(greens_stdout);}
void writeln(double a){fprintf(greens_stdout, "%#.10E\n",a);
                       fflush(greens_stdout);}
void write(double a){fprintf(greens_stdout, "%#.10E ",a);
                     fflush(greens_stdout);}

void print(float a){fprintf(greens_stdout, "%#.8f\n",a);fflush(greens_stdout);}
void writeln(float a){fprintf(greens_stdout, "%#.8f\n",a);
                      fflush(greens_stdout);}
void write(float a){fprintf(greens_stdout, "%#.8f ",a);fflush(greens_stdout);}

void print(int a){fprintf(greens_stdout, "%d\n",a);fflush(greens_stdout);}
void writeln(int a){fprintf(greens_stdout, "%d\n",a);fflush(greens_stdout);}
void write(int a){fprintf(greens_stdout, "%d ",a);fflush(greens_stdout);}

void print(unsigned int a){fprintf(greens_stdout, "%u\n",a);
                           fflush(greens_stdout);}
void writeln(unsigned int a){fprintf(greens_stdout, "%u\n",a);
                             fflush(greens_stdout);}
void write(unsigned int a){fprintf(greens_stdout, "%u ",a);
                           fflush(greens_stdout);}

void print(long int a){fprintf(greens_stdout, "%ld\n",a);fflush(greens_stdout);}
void writeln(long int a){fprintf(greens_stdout, "%ld\n",a);
                         fflush(greens_stdout);}
void write(long int a){fprintf(greens_stdout, "%ld ",a);fflush(greens_stdout);}

void print(long long a){fprintf(greens_stdout, "%lld\n",a);
                        fflush(greens_stdout);}
void writeln(long long a){fprintf(greens_stdout, "%lld\n",a);
                          fflush(greens_stdout);}
void write(long long a){fprintf(greens_stdout, "%lld ",a);
                        fflush(greens_stdout);}

void print(unsigned long a){fprintf(greens_stdout, "%lu\n",a);
                            fflush(greens_stdout);}
void writeln(unsigned long a){fprintf(greens_stdout, "%lu\n",a);
                              fflush(greens_stdout);}
void write(unsigned long a){fprintf(greens_stdout, "%lu ",a);
                            fflush(greens_stdout);}

void print(unsigned long long a){fprintf(greens_stdout, "%llu\n",a);
                                 fflush(greens_stdout);}
void writeln(unsigned long long a){fprintf(greens_stdout, "%llu\n",a);
                                   fflush(greens_stdout);}
void write(unsigned long long a){fprintf(greens_stdout, "%llu ",a);
                                 fflush(greens_stdout);}

void print(short a){fprintf(greens_stdout, "%hi%s",a,"\n"); 
                            fflush(greens_stdout);}
void writeln(short a){fprintf(greens_stdout, "%hi\n",a); 
                      fflush(greens_stdout);}
void write(short a){fprintf(greens_stdout, "%hi ",a); fflush(greens_stdout);}

void print(unsigned short a){fprintf(greens_stdout, "%ho\n",a); 
                             fflush(greens_stdout);}
void writeln(unsigned short a){fprintf(greens_stdout, "%ho\n",a); 
                               fflush(greens_stdout);}
void write(unsigned short a){fprintf(greens_stdout, "%ho ",a); 
                             fflush(greens_stdout);}

void print(char *a)   {fprintf(greens_stdout, "%s\n",a); fflush(greens_stdout);}
void writeln(char *a) {fprintf(greens_stdout, "%s\n",a); fflush(greens_stdout);}
void write(char *a)   {fprintf(greens_stdout, "%s ",a); fflush(greens_stdout);}

void print(const char* _str)   {fprintf(greens_stdout, "%s\n", _str); 
                                fflush(greens_stdout);}
void writeln(const char* _str) {fprintf(greens_stdout, "%s\n", _str); 
                                fflush(greens_stdout);}
void write(const char* _str)   {fprintf(greens_stdout, "%s ", _str); 
                                fflush(greens_stdout);}

void print(void){fprintf(greens_stdout, "\n"); fflush(greens_stdout);}
void writeln(void){fprintf(greens_stdout, "\n"); fflush(greens_stdout);}
void write(void){fprintf(greens_stdout, " "); fflush(greens_stdout);}


void ERROR(const char* _str_){
  if (errlevel==Print) 
    {fprintf(greens_stdout, "%s: %s\n", _str_ERROR, _str_); 
    fflush(greens_stdout);}
  if (errorbreak==Break) exit(1);}

//
void greens_ERROR(const char* _str_proc, const char* _str_err){
  if (errlevel==Print) 
    {fprintf(greens_stdout, "%s: %s: %s\n", _str_greens_error, 
    _str_proc, _str_err);
     fflush(greens_stdout);}
  if (errorbreak==Break) exit(1);
}

//
//
//
void WARNING(const char* _str_)
{if (warnlevel==Print) {
   fprintf(greens_stdout, "%s: %s\n", _str_WARNING, _str_); 
   fflush(greens_stdout);};
 if (warningbreak==Break) exit(1);}

//
//
//
void greens_WARNING(const char* _str_proc, const char* _str_warn)
{if (warnlevel==Print) {
   fprintf(greens_stdout, "%s: %s: %s\n", 
           _str_greens_warning, _str_proc, _str_warn);
   fflush(greens_stdout);};
 if (warningbreak==Break) exit(1);}

//
//-------------------- greens_MESSAGE ----------------------------------------
//
void greens_MESSAGE(const char *msg_str)
{if (msglevel==Print) {
   fprintf(greens_stdout, "%s: %s\n", _str_greens_message, msg_str);
   fflush(greens_stdout);};
}

//
// ------------------- Digits variable and respective function ---------------
//

int greens_set_Digits(int d)
  {if (d>0) { Digits=d; greens_dexact=pow(10.0,(double)-Digits); }
 else  {write(_str_Digits); writeln(Digits); 
        greens_ERROR(_str_set_Digits, _str_wrong_Digits); }
  if (msglevel==Print) {fprintf(greens_stdout, 
                               "#Digits is changed to %d\n", d); 
  fflush(greens_stdout);} return(1);}

int greens_get_Digits(void){return(Digits);}

//
//-------------------- Set nuclear charge --------------------------------
// (variable greens_save_nuclear_charge);
//

void greens_set_nuclear_charge(double charge)
{ if (charge<0.0) {greens_ERROR(_str_set_nuclear_charge, 
                   _str_wrong_nuclear_charge);} 
 else greens_save_nuclear_charge=charge;
  if (msglevel==Print) {
    fprintf(greens_stdout, "#Nuclear charge is changed to %f\n", 
            greens_save_nuclear_charge); 
    fflush(greens_stdout);}}
/*! 

*/
double greens_get_nuclear_charge(void){return(greens_save_nuclear_charge);}

//
// Save levels to save_*? variabels
//
void greens_save_mwe_levels(void){save_warnlevel=warnlevel; 
                                  save_errlevel=errlevel; 
save_msglevel=msglevel;}

//
// Load levels from save_*? variabels
//
void greens_load_mwe_levels(void){warnlevel=save_warnlevel; 
                                  errlevel=save_errlevel; 
msglevel=save_msglevel;}
//
//
// Save Digits
//
void greens_save_Digits(void){ngreens_save_Digits=Digits; }
void greens_save_Digits(int digits){ngreens_save_Digits=Digits; 
                                     greens_set_Digits(digits);}
//
// Load Digits
//
void greens_load_Digits(void){Digits=ngreens_save_Digits; 
                              greens_set_Digits(Digits);}

