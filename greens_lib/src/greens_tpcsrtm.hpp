#ifndef __GREENS_TPCSRTM_HPP
#define __GREENS_TPCSRTM_HPP

#include <math.h>

#include "greens_specfunc.hpp"
#include "greens_relwavefunc.hpp"
#include "greens_ylm.hpp"
#include "greens_wnj.hpp"
#include "greens_tpcsrm.hpp"
#include "greens_tpadrm.hpp"

//
// Two photon total ionization cross section 
// [E_p] photon energy au
// [TPCSRTM] = cm^4 sec
// 
double greens_TPCSRTM(double E_p, int lmax, int electr, int magn, int dig);

double greens_TPCSRTM_lw_circ(double E_p, int lambda_1, int lambda_2, int dig);


/*!
 Two photon total ionization cross section 
 circular polarized light (left/right)
*/ 
double greens_TPCSRTM_circ(double E_p, int lmax, int electr, int magn, 
                        int lambda_1, int lambda_2, int digits);

#endif
