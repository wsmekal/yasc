#include "greens_relwavefunc.hpp"
//
// ---------- energy of hydrogen atom relativistic case -----------------------
//
double greens_energy(int n, int kappa)
{ 
  if (n<1) greens_ERROR(_str_greens_energy, _str_wrong_n);
  if ((kappa<-n)||(kappa>n-1)||(kappa==0)) 
    greens_ERROR(_str_greens_energy, _str_wrong_kappa);
  
  double t3 = greens_save_nuclear_charge*greens_save_nuclear_charge/
              greens_constant_au_E0;
  double t7 = sqrt(kappa*kappa-t3);
  double t9 = (n-fabs(kappa)+t7);
         t9 = t9*t9;
  return(greens_constant_au_E0*(1.0/sqrt(1.0+1.0/t9*t3)-1.0));
}

//
// ---------- Coulomb radial wave function (bound) --------------------------
//
//  (P_{n kappa}(r))
//  (Q_{n kappa}(r))  many-body (Grant) definition
//
spinor2_col greens_radial_spinor(int n, int kappa, double r)
{
  if (n<1) greens_ERROR(_str_radial_spinor, _str_wrong_n);
  if ((kappa<-n)||(kappa>n-1)||(kappa==0)) 
    greens_ERROR(_str_radial_spinor, _str_wrong_kappa);
  if (r<0.0) greens_ERROR(_str_radial_spinor, _str_wrong_coords);
  if (greens_isnan(r)!=0) greens_ERROR(_str_radial_spinor, _str_wrong_coords);
  
    spinor2_col res; 
    if (r==0.0) {res.L=0.0; res.S=0.0; return(res);}
    double charge = greens_save_nuclear_charge;
    double c1 = greens_dexact/charge;
    double r1=r; if (r<c1) r1=c1;
    
    double t4  = charge*charge;
    double t5  = t4*greens_Constant_powalpha2;
    double t6  = fabs(kappa);
    double t7  = kappa*kappa;
    double t9  = sqrt(t7-t5);
    double t11 = n-t6+t9; t11 = t11*t11;
    double t14 = 1.0+1.0/t11*t5;
    double t16 = 1.0-1.0/t14;
    double t19 = sqrt(t16);
    double t20 = t19*greens_constant_powalphaminus1;
    double t21 = sqrt(t20);
    double t24 = t9+t9;
    double t25 = t24+1.0;
    double t26 = GAMMA(t25);
    double t29 = 1.0/t26*greens_constant_alpha*t21*
                 t16/greens_constant_powalpha2*sqrt2;
    double t31 = GAMMA(t24+n-t6+1.0);
    double t32 = sqrt(t14);
    double t33 = 1.0/t32;
    double t34 = 1.0+t33;
    double t36 = n-t6;
    double t37 = GAMMA(1.0+t36);
    double t42 = greens_constant_alpha*charge;
    double t48 = sqrt(1.0/(t42-t19*kappa)/charge*
                 greens_constant_powalphaminus1/t37*t34*t31);
    double t49 = r1*t20;
    double t50 = t49+t49;
    double t52 = pow(t50,t9-1.0);
    double t54 = exp(-t49);
    double t56 = KummerM(-n+1+t6,t25,t50);
    double t57 = t56*t36;
    double t61 = KummerM(-t36,t25,t50);
    double t62 = t61*(kappa-1.0/t19*t42);
    double t70 = sqrt(1.0/t34*(1.0-t33));
    double t71 = t54*t52*t48*t29*r1;
      res.L    = (t57+t62)*t71;
      res.S    = (t57-t62)*t71*t70;

      return(res);
}  
//
// ----------------- radial Coulomb spinor (free) ------------------------------
//
//  (P_{E kappa}(r))
//  (Q_{E kappa}(r))  multiphoton (Grant) definition
//
spinor2_col greens_radial_spinor(double E_non, int kappa, double r)
{ 
  if (E_non<0.0) greens_ERROR(_str_radial_spinor, _str_wrong_energy);
  if (kappa==0) greens_ERROR(_str_radial_spinor, _str_wrong_kappa);
  if (r<0.0) greens_ERROR(_str_radial_spinor, _str_wrong_coords);
  if (greens_isnan(r)!=0) greens_ERROR(_str_radial_spinor, _str_wrong_coords);
     
  spinor2_col res; 
  if (r==0.0) {res.L=0.0; res.S=0.0; return(res);}
  double E, charge, c1, r1, t6, t10, t11, t12, t20, t24, t26, t27, t28, t31, 
         t34, t37, t39, t40, t42, t43, t44, t45, t49, t50, t53, t54, t76, t77;
      
  E = E_non + greens_constant_au_E0;

  charge  = greens_save_nuclear_charge;
  c1 = greens_dexact/charge;
  r1=r; if (r<c1) r1=c1;

  t6  = E*E;
  t10 = sqrt(greens_Constant_powalpha4*t6-1.0);
  t11 = sqrt(t6*greens_constant_powalpha4-1.0)/greens_constant_alpha;
  t12 = sqrt(t11);
  t20 = 1.0/t10;
  t24 = exp(t20*E*charge*greens_Constant_powalpha3*Pi2);
  t26 = t24/sqrtPi*t12;
  t27 = kappa*kappa;
  t28 = charge*charge;
  t31 = sqrt(t27-t28*greens_Constant_powalpha2);
  t34 = t20*E*charge*greens_Constant_powalpha3;
  dcomplex t35     = t31+I*t34;
  dcomplex t36     = GAMMA(t35);
  t37 = abs(t36);
  t39 = t31+t31+1.0;
  t40 = GAMMA(t39);
  t42 = 1.0/t40*t37;
  t43 = greens_Constant_powalpha2*E;
  t44 = abs(t43);
  t45 = 1.0/t44;
  t49 = sqrt((t43+1.0)*t45*t43);
  t50 = r1*t11;
  t53 = pow(t50+t50, t31-1.0);
  dcomplex t55 = exp(-I*t50);
  dcomplex t61 = sqrt(1.0/t35*(-kappa+I*t20*charge*greens_Constant_alpha));
  dcomplex t65 = KummerM(t31+1.0+I*t34,t39, I*(t50+t50));
  dcomplex t67 = t65*t35*t61*t55;
  t76  = sqrt((t43-1.0)*t45*t43);
  t54  = t53*t42*t26*r1;
  t77  = t54+t54;
  res.L = -Re(t67)*t49*t77;
  res.S =  Im(t67)*t76*t77;
  return(res);
}
