#include "greens_tpcsnlds.hpp"
//
// two photon cross section long wave direct summation circular polarized light
//
// [E_p]                   = aeu
// [greens_TPCSN_lwds_circ] = cm^4 sec

const char* _str_greens_TPCSN_lwds_circ="greens_TPCSN_lwds_circ";

double greens_TPCSN_lwds_circ(double E_p, int digits)
{
  double coeff, E_f, err, local_exact, me, smd;
  
  coeff = TwoPi*greens_constant_cgs_c*greens_constant_alpha*E_p*E_p/
          (greens_constant_cgs_a0*pow(greens_constant_cgs_F0, 2));
  
  local_exact=pow(10.0, -digits);
    
  E_f = greens_energy(1)+E_p+E_p; 
  if (E_f<=0.0) greens_ERROR(_str_greens_TPCSN_lwds_circ, _str_wrong_energy);
  
  last_derr=local_exact; me=0.0; 
  for (long n_nu=2; ; n_nu++){
    smd=greens_nonrel_radial_free_bound_me(E_f, 2, n_nu, 1, digits)*
        greens_nonrel_radial_bound_bound_me(n_nu, 1, 1, 0)/(greens_energy(1)-greens_energy(n_nu)+E_p);
    
//    print("greens_TPCSN_lwds_circ");
//    print(smd);
    
    me=me+smd;
    if (me==0) {err=1.0;} else err=abs(smd/me);
    if (err<local_exact) break;
   };
    
  last_dres=coeff*2.0/15.0*pow(abs(me), 2); last_corr=true;
  return(last_dres);
}

//
// two photon cross section long wave direct summation circilar polarized light
// with energy levels Gamma spontan and Gamma induziert
// because of this cross section depend to the intensity of light
//
// [E_p]                   = aeu
// [F]                     = cm^{-2}sec^{-1}
// [greens_TPCSN_lwds_circ] = cm^4 sec
//
double greens_TPCSN_lwds_circ(double E_p, double F, int digits)
{
  double coeff, E_f, E_i, E_nu, err, local_exact, 
              A_spontan, A_induced, induced_detune;
              
  dcomplex me, smd;
  
  coeff = TwoPi*greens_constant_cgs_c*greens_constant_alpha*E_p*E_p/
          (greens_constant_cgs_a0*pow(greens_constant_cgs_F0, 2));
          
  
  local_exact=pow(10.0, -digits);

  E_i = greens_energy(1);
  E_f = E_i+2*E_p; 
  
  if (E_f<=0.0) greens_ERROR(_str_greens_TPCSN_lwds_circ, _str_wrong_energy);
  
  last_derr=local_exact; me=0.0; 
  for (long n_nu=2; ; n_nu++){
    print("greens_TPCSN_lwds_circ_F");
    
    E_nu      = greens_energy(n_nu);
    A_spontan = greens_energy_width_H_non(n_nu, 1);
    
    induced_detune   = pow(A_spontan, 2)/(pow(A_spontan, 2) + pow(E_i-E_nu+E_p, 2)); 
    write("induced_detune"); print(induced_detune);
    induced_detune=1.0;    
    A_induced = greens_nonrel_OPCS(E_f, 1, n_nu, 1, 1, digits)*F*induced_detune;
                    
    A_induced = A_induced*pow(greens_constant_cgs_hbar, 3)/greens_constant_cgs_me/
                          pow(greens_constant_cgs_e, 4);
    
    write("aeu"); write(A_spontan); print(A_induced);
    
    smd=greens_nonrel_radial_free_bound_me(E_f, 2, n_nu, 1, digits)*
        greens_nonrel_radial_bound_bound_me(n_nu, 1, 1, 0)/
       (E_i-E_nu+0.5*I*(A_spontan+A_induced)+E_p);

//    print(smd);

    me=me+smd;
    if (me==0) {err=1.0;} else err=abs(smd/me);
    if (err<local_exact) break;
   };
    
  last_dres=coeff*2.0/15.0*pow(abs(me), 2); last_corr=true;
  return(last_dres);
}

//
// two photon cross section long wave direct summation circilar polarized light
// with energy levels Gamma spontan and Gamma induziert
// because of this cross section depend to the intensity of light
//
// [E_photon]                 = aeu
// [Intensity]                = cm^{-2} W
// [greens_TPCSN_2I_lwds_circ] = cm^4 W^{-1}
//

double greens_TPCSN_2I_lwds_circ(double E_photon, double Intensity, int digits)
{
  double E_photon_SI=E_photon*0.436009164840371e-17;
 
  double Flux = Intensity/E_photon_SI;    // [Flux] = cm^{-2} sec^{-1} 
  last_dres=greens_TPCSN_lwds_circ(E_photon, Flux, digits)/E_photon_SI; 
  return(last_dres);
}

//
// two photon cross section long wave direct summation linear polarized light
// with energy levels Gamma spontan and Gamma induziert
// because of this cross section depend to the intensity of light
//
// [E_p]                   = aeu
// [F]                     = cm^{-2}sec^{-1}
// [greens_TPCSN_lwds_lin]  = cm^4 sec
//
double greens_TPCSN_lwds_lin(double E_p, double F, int digits)
{
  double coeff, E_f, E_i, E_nu, err, local_exact, 
              A_spontan, A_induced, induced_detune;
              
  dcomplex me1, smd1, me2, smd2;
  
  coeff = TwoPi*greens_constant_cgs_c*greens_constant_alpha*E_p*E_p/
          (greens_constant_cgs_a0*pow(greens_constant_cgs_F0, 2));
          
  
  local_exact=pow(10.0, -digits);

  E_i = greens_energy(1);
  E_f = E_i+2*E_p; 
  
  if (E_f<=0.0) greens_ERROR(_str_greens_TPCSN_lwds_circ, _str_wrong_energy);
  
  last_derr=local_exact; me1=0.0; 
  for (long n_nu=2; ; n_nu++){
    
    E_nu      = greens_energy(n_nu);
    A_spontan = greens_energy_width_H_non(n_nu, 1);

    induced_detune   = pow(A_spontan, 2)/(pow(A_spontan, 2) + pow(E_i-E_nu+E_p, 2)); 
    induced_detune=1.0;
    A_induced = greens_nonrel_OPCS(E_f, 1, n_nu, 1, 1, digits)*F*induced_detune;
                    
    A_induced = A_induced*pow(greens_constant_cgs_hbar, 3)/greens_constant_cgs_me/
                          pow(greens_constant_cgs_e, 4);
    
    smd1=greens_nonrel_radial_free_bound_me(E_f, 0, n_nu, 1, digits)*
         greens_nonrel_radial_bound_bound_me(n_nu, 1, 1, 0)/
         (E_i-E_nu+0.5*I*(A_spontan+A_induced)+E_p);

    me1=me1+smd1;
    if (me1 == 0.0) {err=1.0;} else err=abs(smd1/me1);
    if (err<local_exact) break;
   };

  last_derr=local_exact; me2=0.0; 
  for (long n_nu=2; ; n_nu++){
    
    E_nu      = greens_energy(n_nu);
    A_spontan = greens_energy_width_H_non(n_nu, 1);

    induced_detune = pow(A_spontan, 2)/(pow(A_spontan, 2) + pow(E_i-E_nu+E_p, 2)); 
    induced_detune = 1.0;    
    A_induced = greens_nonrel_OPCS(E_f, 1, n_nu, 1, 1, digits)*F*induced_detune;
                    
    A_induced = A_induced*pow(greens_constant_cgs_hbar, 3)/greens_constant_cgs_me/
                          pow(greens_constant_cgs_e, 4);

    smd2=greens_nonrel_radial_free_bound_me(E_f, 2, n_nu, 1, digits)*
         greens_nonrel_radial_bound_bound_me(n_nu, 1, 1, 0)/
       (E_i-E_nu+0.5*I*(A_spontan+A_induced)+E_p);


    me2=me2+smd2;
    if (me2==0) {err=1.0;} else err=abs(smd2/me2);
    if (err<local_exact) break;
   };
    
  last_dres  = coeff*(pow(abs(me1),2.0)/9.0+
              4.0*pow(abs(me2),2.0)/45.0); last_corr = true;
  return(last_dres);
}

//
// two photon cross section long wave direct summation circilar polarized light
// with energy levels Gamma spontan and Gamma induziert
// because of this cross section depend to the intensity of light
//
// [E_photon]                 = aeu
// [Intensity]                = cm^{-2} W
// [greens_TPCSN_2I_lwds_lin]  = cm^4 W^{-1}
//

double greens_TPCSN_2I_lwds_lin(double E_photon, double Intensity, int digits)
{
  double E_photon_SI=E_photon*0.436009164840371e-17;
 
  double Flux = Intensity/E_photon_SI;    // [Flux] = cm^{-2} sec^{-1} 
  last_dres=greens_TPCSN_lwds_lin(E_photon, Flux, digits)/E_photon_SI; 
  return(last_dres);
}
