#ifndef __GREENS_TPCSRL_HPP
#define __GREENS_TPCSRL_HPP

#include <math.h>

#include "greens_const.hpp"
#include "greens_service.hpp"
#include "greens_mdclass.hpp"
#include "greens_numint.hpp"
#include "greens_relwavefunc.hpp"
#include "greens_relgreenfunc.hpp"

//
//
// Two Photon Cross Section Relativistic case circular polarization length wave approximation
//
double greens_TPCSR_lw_circ(double Ep, int digits);
//
//
// Two Photon Cross Section Relativistic case linear polarization length wave approximation
//
double greens_TPCSR_lw_lin(double Ep, int digits);

#endif
