#include "greens_nonrelwavefunc.hpp"
//
// ---------- energy of hydrogen atom nonrelativistic case ------------------------
//
double greens_energy(int n)
{return(-greens_save_nuclear_charge*greens_save_nuclear_charge/(n*n)*0.5);}
//
// ---------- Coulomb radial wave function (bound) ---------------------------------
//    P_{nl}(r)  many-body definition
// 
const char *_str_greens_radial_orbital = "greens_radial_orbital";
//
//
//
double greens_radial_orbital(int n, int l, double r)
{
  if (n<1) greens_ERROR(_str_greens_radial_orbital, _str_wrong_n);
  if ((l>=n)||(l<0)) greens_ERROR(_str_greens_radial_orbital, _str_wrong_l);
  if (r<0.0) greens_ERROR(_str_greens_radial_orbital, _str_wrong_coords);
  
  double t2 = GAMMA(n+l+1.0);
  double t3 = n-l-1.0;
  double t4 = GAMMA(1.0+t3);
  double t7 = 1.0/n;
  double t9 = sqrt(t7/t4*t2);
  double t12 = GAMMA((l<<1)+2.0);
  double t15 = t7*greens_save_nuclear_charge;
  double t16 = sqrt(t15);
  double t20 = t7*r*greens_save_nuclear_charge;
  double t21 = exp(-t20);
  double t22 = t20+t20;
  double t23 = pow(t22,(double)l);
  double t26 = KummerM(-t3,(l<<1)+2.0,t22);
  return((r+r)*t26*t23*t21*t16*t15/t12*t9);
} 

//
// ----------------- radial Coulomb wave function (free state) ---------------------------------
//    P_{El}(r)  many-body definition
//
double greens_radial_orbital(double E, int l, double r)
{
  if (E<0) greens_ERROR(_str_greens_radial_orbital, _str_wrong_energy);
  if (l<0) greens_ERROR(_str_greens_radial_orbital, _str_wrong_l);
  if (r<0) greens_ERROR(_str_greens_radial_orbital, _str_wrong_coords);

  double res, Twopowcharge2divE, charge, sqrtl2E, b, argpowl;
  dcomplex a, z;
  
  charge = greens_save_nuclear_charge; 
  Twopowcharge2divE=(charge+charge)*charge/E;
  sqrtl2E=sqrt(E+E); a=I*charge/sqrtl2E+l+1; 
  b=l+l+2; argpowl=sqrtl2E*(r+r);
  z=I*argpowl; 
  
  res=1; 
  for(long s=2; s<=(l+l); s=s+2){
    res=res*sqrt(s*s+Twopowcharge2divE)*0.5;}
  res=res+res; 
  res=res*Re(KummerM(a,b,z)*exp(-z*0.5))*
             pow(argpowl, (double)l)*r*sqrt(charge);
  res=res/sqrt(1.0-exp(-Pi*(charge+charge)/sqrtl2E))/factorial(l+l+1);
  return(res);
}
