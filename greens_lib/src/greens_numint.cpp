#include "greens_numint.hpp"

/*!
  Variable controls the precision of one-dimensional integration.   
*/
double Int_dexact        = 1.0e-4;

/*!
  Variable controls the precision of two-dimensional integration.   
*/
double Double_Int_dexact = 1.0e-4;


/*!
  Constant arrays Gauss_Legendre_x* and Gauss_Legendre_A*  are knotes and 
  weigths for Gauss-Legendre Quadrature (integration procedures 
  use these constants)
*/
const double Gauss_Legendre_x6[3] =
{.2386191860831969086305017216807119354186, 
 .6612093864662645136613995950199053470064, 
 .9324695142031520278123015544939946091348};

const double Gauss_Legendre_A6[3] =
{.4679139345726910473898703439895509948119, 
 .3607615730481386075698335138377161116623, 
 .1713244923791703450402961421727328935256};


const double Gauss_Legendre_x12[6] = 
{.1252334085114689154724413694638531299834, 
 .3678314989981801937526915366437175612564, 
 .5873179542866174472967024189405342803691, 
 .7699026741943046870368938332128180759849, 
 .9041172563704748566784658661190961925376, 
 .9815606342467192506905490901492808229602};
 
const double Gauss_Legendre_A12[6]= 
{.2491470458134027850005624360429512108304, 
 .2334925365383548087608498989248780562582, 
 .2031674267230659217490644558097983765093, 
 .1600783285433462263346525295433590717940, 
 .1069393259953184309602547181939962242681, 
 .4717533638651182719461596148501706028575e-1};


const double Gauss_Legendre_x24[12] = 
{.6405689286260562608504308262474503859100e-1, 
 .1911188674736163091586398207570696318404, 
 .3150426796961633743867932913198102407865, 
 .4337935076260451384870842319133497124524, 
 .5454214713888395356583756172183723700108, 
 .6480936519369755692524957869107476266697, 
 .7401241915785543642438281030999784255233, 
 .8200019859739029219539498726697452080761,  
 .8864155270044010342131543419821967550873, 
 .9382745520027327585236490017087214496548, 
 .9747285559713094981983919930081690617412, 
 .9951872199970213601799974097007368118746};
 
const double Gauss_Legendre_A24[12] = 
{.1279381953467521569740561652246953718516, 
 .1258374563468282961213753825111836887272, 
 .1216704729278033912044631534762624256408, 
 .1155056680537256013533444839067835597483, 
 .1074442701159656347825773424466062187347, 
 .9761865210411388826988066446424715961996e-1, 
 .8619016153195327591718520298374249254892e-1, 
 .7334648141108030573403361525311673342663e-1, 
 .5929858491543678074636775850010866449288e-1, 
 .4427743881741980616860274821133879761722e-1, 
 .2853138862893366318130781595188461246774e-1, 
 .1234122979998719954680566707004104371351e-1};


const double Gauss_Legendre_x48[24] = 
{.3238017096286936203332224315213444204596e-1, 
 .9700469920946269893005395585362452015274e-1, 
 .1612223560688917180564373907834976947744, 
 .2247637903946890612248654401746922774386, 
 .2873624873554555767358864613167976878516, 
 .3487558862921607381598179372704079161343, 
 .4086864819907167299162254958146332864599, 
 .4669029047509584045449288616507985092368,  
 .5231609747222330336782258691375085262892, 
 .5772247260839727038178092385404787728540, 
 .6288673967765136239951649330699946520249, 
 .6778723796326639052118512806759090588500, 
 .7240341309238146546744822334936652465851, 
 .7671590325157403392538554375229690536226, 
 .8070662040294426270825530430245384459730, 
 .8435882616243935307110898445196560498709, 
 .8765720202742478859056935548050967545616, 
 .9058791367155696728220748356710117883123, 
 .9313866907065543331141743801016012677200, 
 .9529877031604308607229606660257183432085, 
 .9705915925462472504614119838006600573024, 
 .9841245837228268577445836000265988305892, 
 .9935301722663507575479287508490741183566, 
 .9987710072524261186005414915631136400889};
 
const double Gauss_Legendre_A48[24] = 
{.6473769681268392250302493873659155355222e-1, 
 .6446616443595008220650419365770506572595e-1, 
 .6392423858464818662390620182551540891825e-1, 
 .6311419228625402565712602275023331807781e-1, 
 .6203942315989266390419778413759851853905e-1, 
 .6070443916589388005296923202782042112952e-1, 
 .5911483969839563574647481743351915749005e-1, 
 .5727729210040321570515023468471712802090e-1, 
 .5519950369998416286820349519168884507615e-1, 
 .5289018948519366709550505626358704992761e-1, 
 .5035903555385447495780761908633484382286e-1, 
 .4761665849249047482590662363995437971679e-1, 
 .4467456085669428041944858731251546816608e-1, 
 .4154508294346474921405882138197724795111e-1, 
 .3824135106583070631721725463291918682525e-1, 
 .3477722256477043889254861708203210975906e-1, 
 .3116722783279808890206581793335260177220e-1, 
 .2742650970835694820007397951934251782662e-1, 
 .2357076083932437914051932753460431847444e-1, 
 .1961616045735552781446176232392755881727e-1, 
 .1557931572294384872817667815494853810146e-1, 
 .1147723457923453948959337878236443703081e-1, 
 .7327553901276262102384266202470287692867e-2, 
 .3153346052305838632677103783318813127819e-2};

const double Gauss_Legendre_x96[48] =
{.1627674484960296957913456369523845356205e-1, 
 .4881298513604973111195820426127505708095e-1, 
 .8129749546442555899447126297475285877829e-1, 
 .1136958501106659209112080954143280806255, 
 .1459737146548969419891073333433264604743, 
 .1780968823676186027594026251764915149144, 
 .2100313104605672036028471857122957751442, 
 .2417431561638400123279319037406276703116, 
 .2731988125910491414872721557245647944482, 
 .3043649443544963530239297792952033181512, 
 .3352085228926254226163256248056286721871, 
 .3656968614723136350308955939954731772312, 
 .3957976498289086032850002431351641597122, 
 .4254789884073005453648192035699979448148, 
 .4547094221677430086356761480863078130040, 
 .4834579739205963597684056093638952554713, 
 .5116941771546676735855097454288543032494, 
 .5393881083243574362268025973255606381745, 
 .5665104185613971684042501950834626323107, 
 .5930323647775720806835557550437582131736, 
 .6189258401254685703863692869878627973859, 
 .6441634037849671067984123500633015623031, 
 .6687183100439161539525572075727741890245, 
 .6925645366421715613442457698182425594541, 
 .7156768123489676262251441480576248552976, 
 .7380306437444001328511657310964971052297, 
 .7596023411766474987029704411196216158534, 
 .7803690438674332176036044558893177480202, 
 .8003087441391408172287961439713276832165, 
 .8194003107379316755389996242244780351353, 
 .8376235112281871214943028167646928569795, 
 .8549590334346014554627869698935692606428, 
 .8713885059092965028737747644930427147232, 
 .8868945174024204160568774333937861588320, 
 .9014606353158523413192326973096868378409, 
 .9150714231208980742058844661504215420871, 
 .9277124567223086909646904726965463869058, 
 .9393703397527552169318573794316152183553, 
 .9500327177844376357560989484438634878097, 
 .9596882914487425393000680258847347411873, 
 .9683268284632642121736593664436510583926, 
 .9759391745851364664526010342418799091984, 
 .9825172635630146774470458318825481179450, 
 .9880541263296237994807627724184511945505, 
 .9925439003237626245718922977016011408769, 
 .9959818429872092906503990848793566005174, 
 .9983643758631816777241494395267193633876, 
 .9996895038832307668276901057843655192815};
 
const double Gauss_Legendre_A96[48] = 
{.3255061449236316624196141829728573148731e-1, 
 .3251611871386883598720549144777835466900e-1, 
 .3244716371406426936401278844884585833337e-1, 
 .3234382256857592842877483882894327042810e-1, 
 .3220620479403025066866711455723251850392e-1, 
 .3203445623199266321813897747021116299450e-1, 
 .3182875889441100653475373988553225349789e-1, 
 .3158933077072716855802074616996416012504e-1, 
 .3131642559686135581278426671506312817565e-1, 
 .3101033258631383742324977997063263684555e-1, 
 .3067137612366914901422883035620427628555e-1, 
 .3029991542082759379408876420650091424690e-1, 
 .2989634413632838598438807579440006047240e-1, 
 .2946108995816790597043633218285839605325e-1, 
 .2899461415055523654267878127968135782752e-1, 
 .2849741106508538564559951294580446534544e-1, 
 .2797000761684833443981857658890962933674e-1, 
 .2741296272602924282342108748876743330905e-1, 
 .2682686672559176219805672870900188856875e-1, 
 .2621234073567241391345796402729526929901e-1, 
 .2557003600534936149879716811394246566169e-1, 
 .2490063322248361028838217406105323352347e-1, 
 .2420484179236469128226734210290636041607e-1, 
 .2348339908592621984223614896863524467576e-1, 
 .2273706965832937400134553176951537806728e-1, 
 .2196664443874434919475938417887027895395e-1, 
 .2117293989219129898763007943149960961723e-1, 
 .2035679715433332459550755956545709988485e-1, 
 .1951908114014502240985660092264305158752e-1, 
 .1866067962741146738486004936252854783442e-1, 
 .1778250231604526082443594042190769982855e-1, 
 .1688547986424517243711984029508469096303e-1, 
 .1597056290256229132035382991738257816173e-1, 
 .1503872102699493817686914183264386716957e-1, 
 .1409094177231486012891774675764976167465e-1, 
 .1312822956696157263523886275910473793385e-1, 
 .1215160467108832187929261085758495469993e-1, 
 .1116210209983850883332979468020654088039e-1, 
 .1016077053500840227221076817446873146164e-1, 
 .9148671230783382692121475353993341864866e-2, 
 .8126876925698723108276791033335751288503e-2, 
 .7096470791153987883437356657846134869097e-2, 
 .6058545504235978130281469410746551368807e-2, 
 .5014202742927527836680968516818269789487e-2, 
 .3964554338444697362818678085835789949876e-2, 
 .2910731817935108429911706716178604260129e-2, 
 .1853960788946999924752366959110367159086e-2, 
 .7967920655519996041558885543468287767644e-3};


const double Gauss_Legendre_x192[96] = 
{.8159862838095286144725462139639069417953e-2, 
 .2447741525523520549315998396275543703868e-1, 
 .4078844847403864527738161700471161497873e-1, 
 .5708861829151205770128136652529275534972e-1, 
 .7337358339796853757389944640916407306382e-1, 
 .8963900653327159661982075744412597493565e-1, 
 .1058805556420003998801369668227608797547, 
 .1220939050272288030156827800270712588354, 
 .1382747365026108985189998820297988949641, 
 .1544187405424662298791849492833017711920, 
 .1705216174295583655043303941593980967689, 
 .1865790784002611385477991734258359927522, 
 .2025868467868075545434961993931731829567, 
 .2185406591563171457463379882822287870549, 
 .2344362664462984090903798119956503335466, 
 .2502694350963239034900637063721692546353, 
 .2660359481755766015698183178924267381737, 
 .2817316065059671905423468209843094949039, 
 .2973522297805231965735868566629289431926, 
 .3128936576767520662584319958241016115810, 
 .3283517509646816774505792608815984988552, 
 .3437223926092831692859114825625485259196, 
 .3590014888669824774342224742254436304790, 
 .3741849703759685350105230464587518828582, 
 .3892687932400077517555031425089793962497, 
 .4042489401054761136151635308892818541580, 
 .4191214212313220512518038667756912041020, 
 .4338822755516751088216675844448257342365, 
 .4485275717308174030575108927536054415102, 
 .4630534092102368967759482341181049927917, 
 .4774559192474836198483994125790000448051, 
 .4917312659465521538697583777912979860473, 
 .5058756472795159536497884562071850828030, 
 .5198852960991414086385045279429992435024, 
 .5337564811422119498589125315046376378817, 
 .5474855080232949822198280472393228303823, 
 .5610687202186869675605952774257932221603, 
 .5745025000402745997622328877199732384712, 
 .5877832695990526990513018873113859219336, 
 .6009074917580422075109537861843085193868, 
 .6138716710743544910676773343071987005705, 
 .6266723547301510440942257233984525426476, 
 .6393061334522506504939562614901835802044, 
 .6517696424201390789275344473475307627373, 
 .6640595621621394789116155447266717735354, 
 .6761726194395047980470490663027340757745, 
 .6881055881181967577927191283377691877319, 
 .6998552900281192051475475113416285596691,
 .7114185958095769994814961263379359995620, 
 .7227924257467349966987265710195043999019, 
 .7339737505878551560441256578153760564760, 
 .7449595923520933172914643282748365702356, 
 .7557470251226407768848703807442226875482, 
 .7663331758259994299497462391583613191591, 
 .7767152249971829400510635555749103882418, 
 .7868904075306401492697565606090533478551, 
 .7968560134167007467203242051792673782971, 
 .8066093884633470731986553853368341706471, 
 .8161479350031198524218644920022155347918, 
 .8254691125849696045538228486220810181695, 
 .8345704386508695147393385041118965143377, 
 .8434494891970095976588225636632541588878, 
 .8521038994193961183039219550285385614139, 
 .8605313643436843991581167574853415078517, 
 .8687296394390773650062052096541977483003, 
 .8766965412161264494836028717904298117049, 
 .8844299478082758137705466992072068184155, 
 .8919277995369952102259633387959994252406, 
 .8991880994603512666000259568610843341137, 
 .9062089139048714766238190927436893069985, 
 .9129883729805597709004952516054175732943, 
 .9195246710789272245880472369639541600095, 
 .9258160673539062609128154511394388728217, 
 .9318608861855216719820172701963348824203, 
 .9376575176261969636700289673768231736961, 
 .9432044178295800403958793265082118863341, 
 .9485001094617782425446726957406749853802, 
 .9535431820948995063904708574256973816797,
 .9583322925828043949981517329007647283695, 
 .9628661654189837492787994912503303260603, 
 .9671435930764901577246260169273176155117, 
 .9711634363298709597893756810891852774527, 
 .9749246245590810488762592665419202760624, 
 .9784261560354051061458091997963068761790, 
 .9816670981895111523937086705610449994840, 
 .9846465878619336764167001102060533232386, 
 .9873638315366433759878785288998682729433, 
 .9898181055591399025125907309048818331563, 
 .9920087563423258732188356447321776632971, 
 .9939352005680611362729976453293205517811, 
 .9955969254054171881427829799848504824253, 
 .9969934888089658276499713406258740571241, 
 .9981245201227147822147993855489200890770, 
 .9989897220082228180635279012706708485988, 
 .9995888803394138541093349296043198363725, 
 .9999219686591948443225892454883059020862};

 
const double Gauss_Legendre_A192[96] = 
{.1631936345670150508355623929347408572401e-1, 
 .1631501703507201515824817210238839731430e-1, 
 .1630632534941829998274048170492021479886e-1, 
 .1629329071464257785160185083292899264662e-1, 
 .1627591660232748068189234778679602565273e-1, 
 .1625420763981144928166232902304545813971e-1, 
 .1622816960895631013944081316803140312932e-1, 
 .1619780944460736197339177074554857230433e-1, 
 .1616313523274638217456553266231248085303e-1, 
 .1612415620833804506520547322754074161623e-1, 
 .1608088275287032554860887339963451061437e-1, 
 .1603332639158954322984235013839656217039e-1, 
 .1598149979043078341497460420826008458222e-1, 
 .1592541675264451252875776745840061284126e-1, 
 .1586509221512028640526162020266562206948e-1, 
 .1580054224440853058129963261059207883199e-1, 
 .1573178403244145213710425099997850464762e-1, 
 .1565883589195422276120398725391176489101e-1, 
 .1558171725160765254549632202027091878176e-1, 
 .1550044865081365352085059342061593182745e-1, 
 .1541505173426487110205508422732437218374e-1, 
 .1532554924616994040247847083367135871768e-1, 
 .1523196502419590278248468769118197215575e-1, 
 .1513432399311939599056635198463322626250e-1, 
 .1503265215818830882160301087474154974650e-1, 
 .1492697659819566833198447984901299762671e-1, 
 .1481732545826760429607097933074645456916e-1, 
 .1470372794236731174222873844805855251083e-1, 
 .1458621430551700804926100230937412246859e-1, 
 .1446481584573995619537692547529470254395e-1, 
 .1433956489572470031198777092644870137077e-1, 
 .1421049481421373368383892660234475016706e-1, 
 .1407763997711889273569915932555556407369e-1, 
 .1394103576836584333464095632054564179010e-1, 
 .1380071857047009789665883850857893546906e-1, 
 .1365672575484707329799543649929898295884e-1, 
 .1350909567185877043631007883710086256264e-1, 
 .1335786764059972644620390859976830684722e-1, 
 .1320308193842496002934176412517348077179e-1, 
 .1304477979022269909350163852581689206940e-1, 
 .1288300335743474788966926602597999839523e-1, 
 .1271779572682741807446091689663610796109e-1, 
 .1254920089901601458978069148872322544819e-1, 
 .1237726377674593292623165356797660773746e-1, 
 .1220203015293348920546093199411789557536e-1, 
 .1202354669846966856400385119887345268777e-1, 
 .1184186094979004053271591814062852784944e-1, 
 .1165702129621415246785292850221956081617e-1, 
 .1146907696705777358968578140016645512806e-1, 
 .1127807801852142281099299668499554587816e-1, 
 .1108407532035867328136178184595931241240e-1, 
 .1088712054232778542665459065331423080377e-1, 
 .1068726614043027822182015353540760952019e-1, 
 .1048456534294010549872985896280296482133e-1, 
 .1027907213622716026310745432162761304873e-1, 
 .1007084125037888528666258758886660387133e-1, 
 .9859928144623822671848493861984866435604e-2, 
 .9646388992560988688871555468912997126577e-2, 
 .9430280667199013005803355133957722975937e-2, 
 .9211660725809033543826169770456870062427e-2, 
 .8990587394595389693608817807139738832806e-2, 
 .8767119553188207673350963346520280819652e-2, 
 .8541316718962022605943311383449197890170e-2, 
 .8313239031184632745769256416255033703619e-2, 
 .8082947235000432632340753774352780960231e-2, 
 .7850502665252524421764359380945043834760e-2, 
 .7615967230147961172624843647609493787815e-2, 
 .7379403394770533802095458002934854488515e-2, 
 .7140874164445576807569565961274594144285e-2, 
 .6900443067961339719780742657597359873340e-2, 
 .6658174140651556266610738434308650153018e-2, 
 .6414131907343948572275024532121755529305e-2, 
 .6168381365179540674609447146559300952774e-2, 
 .5920987966307842199944703122938377212947e-2, 
 .5672017600463228374204949713345915916620e-2, 
 .5421536577428234926249655845130451698223e-2, 
 .5169611609390087300715718380707856209578e-2, 
 .4916309793197733507646532723103110758378e-2, 
 .4661698592528197474902387832779176738779e-2, 
 .4405845819973666428314679521354990590453e-2, 
 .4148819619065216099139199471247325070089e-2, 
 .3890688446257093249913484957609354777303e-2, 
 .3631521052910285400855749274436804402331e-2, 
 .3371386467342533589805998887772171551312e-2, 
 .3110353977068882106680359244940865140838e-2, 
 .2848493111476581340970402160549378085262e-2, 
 .2585873625444375300279098768422968652184e-2, 
 .2322565485048108214442097210742048281436e-2, 
 .2058638858115252727715815764802190681829e-2, 
 .1794164116955045626355485504393804442754e-2, 
 .1529211875019241493105816998527578217357e-2, 
 .1263853132096728285868613118121799066797e-2, 
 .9981598380233457554121719304217043205670e-3, 
 .7322075666250185328339640106023408617060e-3, 
 .4660944855912469603944432530616908484125e-3, 
 .2002510147471267303712312438711059129865e-3};


/*!
  _G_Leg_Orders is array of possible orders for the Gauss-Legendre 
  Quadratures. Only for these orders the knotes and weigths are available.
*/
const int     _G_Leg_Orders[GLegMax_order] = {6, 12, 24, 48, 96, 192};
/*!
  _G_Leg_xs is array of possible arrays with knotes for the 
  Gauss-Legendre Quadratures.
*/
const double *_G_Leg_xs[GLegMax_order] ={
  Gauss_Legendre_x6, Gauss_Legendre_x12, Gauss_Legendre_x24,
  Gauss_Legendre_x48, Gauss_Legendre_x96, Gauss_Legendre_x192};
/*!
  _G_Leg_As is array of possible arrays with weigths for 
  the Gauss-Legendre Quadratures.
*/
const double *_G_Leg_As[GLegMax_order] ={
  Gauss_Legendre_A6, Gauss_Legendre_A12, Gauss_Legendre_A24,
  Gauss_Legendre_A48, Gauss_Legendre_A96, Gauss_Legendre_A192};

//
// -------------- real integrand Gauss_Legendre_Int -----------------------------------------
//
/*!
  Strings for error and warning messages 
*/
const char* _str_greens_integral_GL="greens_integral_GL";
const char* _str_greens_integral_GL_order="greens_integral_GL_order";
const char* _str_nan_integrand = "nan integrand";

/*!
  b_nan_integrand indicates whether the integrand was NaN in a 
  integration procedure.
  Must be initialised to false by a user appropriate integration-procedure 
  at the beginning of a calculation.
*/
bool b_nan_integrand = false;

/*!
  Procedure is used for calculate a one-dimensional integral sum for
  a defined integrand (funct), integration limits, Order and Raum 
  (numbers of pieces,
  to which the integration interval is divided and a Gauss-Legendre 
  quadrature of order Order applied)
  the knotes and weigths must be given also. Procedure is used by the 
  user-appropriate integration functions.
*/
double greens_Gauss_Legendre(double (*funct)(double),
                            const double Xmin, const double Xmax, 
		            const int Order, const long Raum,
			    const double *_x, const double *_A)
{
  double res2=0.0; double SubX=(Xmax-Xmin)/(Raum+Raum);
  double x_min, x_max, addx, xi, Xmin2=Xmin*0.5, f;
  
  x_min=Xmin2; x_max=Xmin2+SubX;
  for(long xr=1; xr<=Raum; xr++){ addx=(x_max+x_min); 
    for(long i=0; i<(Order>>1); i++){
      xi=(addx+SubX*_x[i]); f=_A[i]*funct(xi); 
      if (greens_isnan(f)==0) {res2=res2+f;} 
    else { greens_WARNING(_str_greens_integral_GL, _str_nan_integrand);
	   b_nan_integrand = true;};
	
      xi=(addx-SubX*_x[i]); f=_A[i]*funct(xi); 
      if (greens_isnan(f)==0) {res2=res2+f;} 
    else { greens_WARNING(_str_greens_integral_GL, _str_nan_integrand);
	   b_nan_integrand = true;}};
    x_max=x_max+SubX; x_min=x_min+SubX;};
  return(res2*SubX);
}

/*!
  An user-appropriate procedure for the one-dimensional numerical integration.
  The integrand, integration limits as well as desired Order and Raum must be 
  given.
*/
double greens_integral_GL_order(double (*funct)(double), const double Xmin, 
                                                         const double Xmax, 
                                        const int Order, const long Raum)
{
  b_nan_integrand = false;
  const double *_A, *_x;

  bool bnotfound=true;
  for(int i_order=0; i_order<GLegMax_order; i_order++)
  {
   if (Order==_G_Leg_Orders[i_order]){
     bnotfound=false; _A=_G_Leg_As[i_order]; _x=_G_Leg_xs[i_order]; break;}};
   
  if (bnotfound) 
    greens_ERROR(_str_greens_integral_GL_order, _str_wrong_Order);
  if (Raum<1) 
    greens_ERROR(_str_greens_integral_GL_order, _str_wrong_Raum);  

  return(greens_Gauss_Legendre(funct, Xmin, Xmax, Order, Raum, _x, _A));
}

//
//---------------------- double  greens_integral_GL -------------------
//
//
/*! 
  Variable _GLeg_Piece controls the size of a piese to which the infinite 
  interval 0..\infty is divided.
  The variable must be setted before calling up a integration in infinitely 
  limits, if not the later value will be used. The variable usually 
  controlled by a routine which calculates a matrix element.
*/
double _GLeg_Piece = 5.0;

/*!
  The variables are used internally in the integration 
  routines and must be not setted before.
*/
long _GLeg_Begin_Raum=1; long _GLeg_Begin_order=0;
long _GLeg_Last_Raum; int _GLeg_Last_order;
//
//
// 
/*!
  An one-dimensional quadrature in finite limits Xmin, Xmax.
  The procedure increased the order of the Gauss-Legendre method and, by case,
  the number of pieces to which the integration interval is divided.
  The desired precision must be given to procedure in form of a number 
  of right digits.

  This is the appropriate to the user routine.
*/
double greens_integral_GL(double (*funct)(double), 
                          const double Xmin, const double Xmax, 
			  const int digits)
{
  double res1, res2; int order;
  Int_dexact=pow(10.0, (double)-digits);
  
  if ((_GLeg_Begin_order<0)||(_GLeg_Begin_order>GLegMax_order-2))
  {greens_ERROR(_str_greens_integral_GL, _str_wrong_Begin_order);};

//  cur_dfunct=funct; cur_Xmin=Xmin; cur_Xmax=Xmax; 
//  cur_Order=_G_Leg_Orders[_GLeg_Begin_order]; cur_Raum=1;
//  cur_A = _G_Leg_As[_GLeg_Begin_order]; cur_x=_G_Leg_xs[_GLeg_Begin_order];

  res1=greens_Gauss_Legendre(funct, Xmin, Xmax, 
                            _G_Leg_Orders[_GLeg_Begin_order], 1, 
                            _G_Leg_xs[_GLeg_Begin_order], 
			    _G_Leg_As[_GLeg_Begin_order]);
  
  bool b_not_reach=true;
  for(long Raum=1;; Raum++){
    for(order=_GLeg_Begin_order+1; order<GLegMax_order; order++){

      res2 = greens_Gauss_Legendre(funct, Xmin, Xmax, 
                                  _G_Leg_Orders[order], Raum, 
                                  _G_Leg_xs[order], _G_Leg_As[order]);

      if (res2==0.0) {last_derr=abs(res1);}
    else {last_derr=abs((res2-res1)/res2);}
      
      if (last_derr<Int_dexact) {
        b_not_reach=false; last_dres=res2; last_corr=true; break;}; 
      res1=res2; }; 
    if (!b_not_reach) break; };
  
  _GLeg_Last_order=order;
    
  last_dres=res2; last_corr=(last_derr<0.1);  

  if (b_not_reach) {print(_str_greens_integral_GL); 
                    write(Xmin); writeln(Xmax);
   greens_WARNING(_str_greens_integral_GL, _str_precision_not_reached);}
  if (!last_corr) {greens_WARNING(_str_greens_integral_GL, 
                                  _str_wrong_method);}

  return(last_dres);
}
//
// int(funct, x=Xmin..infinity)
//

/*!
  An one-dimensional quadrature when a low limit is finite 
  Xmin and high limit is infinite.
  The procedure increased the order of the Gauss-Legendre 
  method and, by case,
  the number of pieces to which the integration interval is divided.
  The desired precision must be given to procedure in form of 
  a number of right digits.
  The variable _GLeg_Piece controls the largeness of a piece to 
  which the infinite integration interval is divided.

  This is the appropriate to the user routine.
*/
double greens_integral_GL(double (*funct)(double), const double Xmin, 
                             /* double Xmax=infinity */ 
			        const int digits)
{
  double err, res1, res2, rmin, rmax;
  bool bbreak;
  _GLeg_Begin_Raum=1; _GLeg_Begin_order=0;
    
  res2=0; err=0; rmin=Xmin; rmax=rmin+_GLeg_Piece;
  for (long i=1;;i++){
  
   res1=greens_integral_GL(funct, rmin, rmax, digits);

   res2=res2+res1; 
   if (res2==0.0) {err=(res2*err+res1*last_derr);}
 else {err=(res2*err+res1*last_derr)/res2;} rmin=rmax;
   rmax=rmax+_GLeg_Piece; 
   if (res2==0.0) {bbreak=abs(res1)<Int_dexact;} 
 else {bbreak=abs(res1/res2)<Int_dexact;}

   _GLeg_Begin_order=_GLeg_Last_order-1;
   if (_GLeg_Begin_order>GLegMax_order-2) _GLeg_Begin_order=GLegMax_order-2;
   _GLeg_Begin_Raum=_GLeg_Last_Raum-1; 
   if (_GLeg_Begin_Raum<1) _GLeg_Begin_Raum=1;
      
   if (bbreak) break;}

  last_corr=true; last_derr=err; last_dres=res2; return(last_dres);
}

/*!
  An one-dimensional quadrature over half-infinite interval 0..\infty.
  The procedure increased the order of the Gauss-Legendre method and, by case,
  the number of pieces to which the integration interval is divided.
  The desired precision must be given to procedure in 
  form of a number of right digits.
  The variable _GLeg_Piece controls the largeness of a piece 
  to which the infinite integration interval is divided.

  This is the appropriate to the user routine.
*/
double greens_integral_GL(double (*funct)(double), const int digits)
{return(greens_integral_GL(funct,0.0,digits));}

//
// -------------- Double_Int with Gauss-Legendre method --------------------
//
//
//
/*!
  Procedure is used for calculating a two-dimensional integral sum over 
  finite square surface for
  a defined integrand (funct), integration limits, Orders and Raums 
  (numbers of pieces,
  to which the integration interval is divided and a Gauss-Legendre 
  quadrature of order Order applied)
  the knotes and weigths must be given also. Procedure is used by the 
  user-appropriate integration functions.
*/
double greens_integral_GL(double (*funct)(double, double), 
                                       const double Xmin, const double Xmax, 
				       const double Ymin, const double Ymax,
				       const int OrderX , const int OrderY ,
				       const long RaumX , const long RaumY ,
				       const double *_x , const double *_y ,
				       const double *_wx, const double *_wy)
{
  double res2, SubX, SubY, addx, addy, wxy, resx, x_min, x_max, y_min, 
  y_max, yi, xi; 
  res2=0.0; SubX=(Xmax-Xmin)/2.0/RaumX; SubY=(Ymax-Ymin)/2.0/RaumY;
  
  for(long yr=1; yr<=RaumY; yr++){
    y_min=Xmin+(yr-1)*SubY*2.0; y_max=Xmin+yr*SubY*2.0; 
    addy=(y_max+y_min)*0.5;
    for(int j=0; j<(OrderY>>1); j++){
      yi=addy+SubY*_y[j]; resx=0.0;
      for(long xr=1; xr<=RaumX; xr++){
        x_min=Xmin+(xr-1)*(SubX+SubX); x_max=Xmin+xr*(SubX+SubX); 
	addx=(x_max+x_min)*0.5; 
	for(int i=0; i<(OrderX>>1); i++){
	  xi=addx+SubX*_x[i]; wxy=_wx[i]*_wy[j]; resx=resx+wxy*funct(xi, yi);
          xi=addx-SubX*_x[i]; resx=resx+wxy*funct(xi, yi);}}; 
      res2=res2+resx;
      yi=addy-SubY*_y[j]; resx=0.0;
      for(long xr=1; xr<=RaumX; xr++){
        x_min=Xmin+(xr-1)*(SubX+SubX); x_max=Xmin+xr*(SubX+SubX); 
	addx=(x_max+x_min)*0.5;
        for(int i=0; i<(OrderX>>1); i++){
        xi=addx+SubX*_x[i]; wxy=_wx[i]*_wy[j]; resx=resx+wxy*funct(xi, yi);
        xi=addx-SubX*_x[i]; resx=resx+wxy*funct(xi, yi);}}; 
      res2=res2+resx;}}; 

  return(res2*SubX*SubY);
}

/*!
  An two-dimensional quadrature over a finite square.
  The procedure increases the order of the Gauss-Legendre method and, by case,
  the number of pieces to which the integration interval is divided.
  The desired precision must be given to procedure in 
  form of a number of right digits.

  This is the appropriate to the user routine.
*/
double greens_integral_GL(double (*funct)(double, double), 
                                       const double Xmin, const double Xmax, 
                                       const double Ymin, const double Ymax, 
				       const int digits)
{ 
  int Begin_order;
  double res1, res2, ret_err;
  int OrderX, OrderY;  long RaumX, RaumY;
  const double *_x, *_y, *_wx, *_wy;
  
  bool bbreak; 
  Double_Int_dexact=pow(10.0,(double)-digits);
 
  Begin_order=2;     
  OrderX = _G_Leg_Orders[Begin_order];  OrderY = _G_Leg_Orders[Begin_order];
  RaumX=1; RaumY=1;  
  _x = _G_Leg_xs[Begin_order]; _y = _G_Leg_xs[Begin_order];
  _wx = _G_Leg_As[Begin_order]; _wy = _G_Leg_As[Begin_order];


  res1=greens_integral_GL(funct, Xmin, Xmax, Ymin, Ymax,
                                       OrderX, OrderY, 
				       RaumX, RaumY, _x, _y, _wx, _wy);
  
// durchversuch x-axes

  for (RaumX=1;; RaumX++) {
    for (int order=Begin_order+1; order<GLegMax_order; order++){
      OrderX = _G_Leg_Orders[order]; _x = _G_Leg_xs[order]; 
      _wx = _G_Leg_As[order]; 

      res2=greens_integral_GL(funct, Xmin, Xmax, Ymin, Ymax,
                                       OrderX, OrderY, RaumX, RaumY, 
				       _x, _y, _wx, _wy);

      
      last_derr=abs((res1-res2)/res2); bbreak=(last_derr<=Double_Int_dexact); 
      res1=res2;
    if (bbreak) break; } if (last_derr<Double_Int_dexact) break;}


// durchversuch y-axes

  for (RaumY=1;;RaumY++){
    for (int order=Begin_order+1; order<GLegMax_order; order++){
      OrderY = _G_Leg_Orders[order]; _wy = _G_Leg_As[order]; 
      _y  = _G_Leg_xs[order];

      res2=greens_integral_GL(funct, Xmin, Xmax, Ymin, Ymax,
                                       OrderX, OrderY, RaumX, RaumY, _x, _y, 
				       _wx, _wy);
          
    ret_err=abs((res1-res2)/res2); bbreak=(ret_err<=Double_Int_dexact); 
    res1=res2;
    if (bbreak) break; 
  } if (ret_err<Double_Int_dexact) {break;};
  }

  if (printlevel==DebugPrint)
  {
  writeln(_str_greens_integral_GL);
  write("RaumX = "); write(RaumX); write("RaumY = "); writeln(RaumY); 
  write("OrderX = "); write(OrderX); write("OrderY = "); writeln(OrderY);
  write("last_derr = "); writeln(last_derr);
  }

  last_corr=true; last_derr=max(last_derr, ret_err); last_dres=res2;
  return(last_dres);
}

/*!
  The variables are used internally for representing a start Orders and Raums 
  for  two-dimensional routines.
*/
int _GLegPolar_Begin_orderR = 0; /*[0..GLegMax_order-1]*/
int _GLegPolar_Begin_orderP = 0; /*[0..GLegMax_order-1]*/

long _GLegPolar_Begin_RaumR = 1; /* 1... */
long _GLegPolar_Begin_RaumP = 1; /* 1... */

int _GLegPolar_Last_orderR = 0; /*[0..GLegMax_order-1]*/
int _GLegPolar_Last_orderP = 0; /*[0..GLegMax_order-1]*/

long _GLegPolar_Last_RaumR = 1; /* 1... */
long _GLegPolar_Last_RaumP = 1; /* 1... */


/*!
  Procedure is used for calculating a two-dimensional integral 
  sum over a sector region for
  a defined integrand (funct), integration limits, Orders and Raums
   (numbers of pieces,
  to which the integration interval is divided and a Gauss-Legendre 
  quadrature of order Order applied)
  the knotes and weigths must be given also. Procedure is used by the 
  user-appropriate integration functions.
*/
double greens_Gauss_Legendre_Polar_Int(double (*funct)(double, double),
                                      const double Rmin, const double Rmax,
                                      const double Pmin, const double Pmax,  
				      const int OrderR, const int OrderP,
                                      const long RaumR, const long RaumP,				      
				      const double *_r, const double *_p, 
				      const double *wr, const double *wp)
{
  double res2, SubR, SubP, addr, addp, wrp, resr, r_min, r_max, p_min, p_max, 
  ri, phi, cosphi, sinphi; 
  
  /*BEGIN MACRO*/
  res2=0.0;
  SubR=(Rmax-Rmin)/2.0/RaumR; SubP=(Pmax-Pmin)/2.0/RaumP;
  for(long pr=1; pr<=RaumP; pr++){
    p_min=Pmin+(pr-1)*SubP*2.0; p_max=Pmin+pr*SubP*2.0; addp=(p_max+p_min)/2.0;
   
  for(int j=0; j<(OrderP>>1); j++){ 
    phi=addp+SubP*_p[j]; resr=0.0; sinphi=sin(phi); cosphi=cos(phi);
    for(long rr=1; rr<=RaumR; rr++){
      r_min=Rmin+(rr-1)*SubR*2.0; r_max=Rmin+rr*SubR*2.0; 
      addr=(r_max+r_min)/2.0; 
      for(int i=0; i<(OrderR>>1); i++){
        ri=addr+SubR*_r[i]; wrp=wr[i]*wp[j];
        resr=resr+wrp*funct(ri*cosphi, ri*sinphi)*ri;
	ri=addr-SubR*_r[i]; resr=resr+wrp*funct(ri*cosphi, ri*sinphi)*ri;}}; 
    res2=res2+resr;
    
    phi=addp-SubP*_p[j]; resr=0.0; sinphi=sin(phi); cosphi=cos(phi);
    for(long rr=1; rr<=RaumR; rr++){
      r_min=Rmin+(rr-1)*SubR*2.0; r_max=Rmin+rr*SubR*2.0; 
      addr=(r_max+r_min)/2.0; 
      for(int i=0; i<(OrderR>>1); i++){
        ri=addr+SubR*_r[i]; wrp=wr[i]*wp[j];
        resr=resr+wrp*funct(ri*cosphi, ri*sinphi)*ri;
        ri=addr-SubR*_r[i]; resr=resr+wrp*funct(ri*cosphi, ri*sinphi)*ri;}}; 
    res2=res2+resr;}}; 
    
  res2=res2*SubR*SubP;
  /*END MACRO*/
  return(res2);
}

//
//  real Polar Int
//:=int(int(funct(x,y)*r, r=Rmin..Rmax), p=Pmin..Pmax); with 
// transformation x=r*cos(p); y=r*sin(p);
//
/*!
  An two-dimensional quadrature over a finite polar segment.
  The procedure increased the order of the Gauss-Legendre method and, by case,
  the number of pieces to which the integration interval is divided.
  The desired precision must be given to procedure in form of a number of 
  right digits.

  This is the appropriate to the user routine.
  Integrand must be a function f(x,y) in cartesian coordinate system.
  Procedure returns an integral F
  F = \int_{Rmin}^{Rmax}\int_{Pmin}^{Pmax} f(x,y)\, r dr \,d\phi.
*/

double greens_Gauss_Legendre_Polar_Int(double (*funct)(double, double),
                                      const double Rmin, const double Rmax, 
                                      const double Pmin, const double Pmax, 
				      const int digits)
{ 
  int RaumR, RaumP, OrderR, OrderP, Begin_orderR, Begin_orderP, order;
  double res1, res2, ret_err;
              
  const double *wr, *wp, *_r, *_p; bool bbreak; 
  Double_Int_dexact=pow(10.0,(double)-digits);

  Begin_orderR=_GLegPolar_Begin_orderR; Begin_orderP=_GLegPolar_Begin_orderP;
  if (((Begin_orderR>=GLegMax_order)||(Begin_orderR<0))||
      ((Begin_orderP>=GLegMax_order)||(Begin_orderP<0)))
    greens_ERROR(_str_greens_Gauss_Legendre_Polar_Int, _str_wrong_Order);

  RaumR=_GLegPolar_Begin_RaumR; RaumP=_GLegPolar_Begin_RaumP;

  if ((RaumR<1)||(RaumR<1)) 
    greens_ERROR(_str_greens_Gauss_Legendre_Polar_Int, _str_wrong_Raum);

  OrderR = _G_Leg_Orders[Begin_orderR]; OrderP = _G_Leg_Orders[Begin_orderP];
  wr = _G_Leg_As[Begin_orderR]; wp = _G_Leg_As[Begin_orderP];
  _r = _G_Leg_xs[Begin_orderR]; _p = _G_Leg_xs[Begin_orderP];

  res1=greens_Gauss_Legendre_Polar_Int(funct, Rmin, Rmax, Pmin, Pmax,
                                      OrderR, OrderP, RaumR, RaumP, _r, _p, 
				      wr, wp);
			      
// durchversuch r-axes

  for (RaumR=1;; RaumR++){
    for (order=Begin_orderR+1; order<GLegMax_order; order++){
      OrderR = _G_Leg_Orders[order]; wr = _G_Leg_As[order]; 
      _r  = _G_Leg_xs[order];

      res2=greens_Gauss_Legendre_Polar_Int(funct, Rmin, Rmax, Pmin, Pmax,
                                      OrderR, OrderP, RaumR, RaumP, _r, _p, 
				      wr, wp);

      last_derr=abs((res1-res2)/res2); bbreak=(last_derr<=Double_Int_dexact); 
      res1=res2;
    if (bbreak) break;
  } if (last_derr<Double_Int_dexact) break;}
  
  _GLegPolar_Last_orderR=order;

// durchversuch p-axes

  for (RaumP=1;;RaumP++) {
    for (order=Begin_orderP+1; order<GLegMax_order; order++){
      OrderP = _G_Leg_Orders[order]; wp = _G_Leg_As[order]; 
      _p  = _G_Leg_xs[order];

      res2=greens_Gauss_Legendre_Polar_Int(funct, Rmin, Rmax, Pmin, Pmax,
                                      OrderR, OrderP, RaumR, RaumP, _r, _p, 
				      wr, wp);

    ret_err=abs((res1-res2)/res2); bbreak=(ret_err<=Double_Int_dexact); 
    res1=res2;
    if (bbreak) break; 
  } if (ret_err<Double_Int_dexact) {break;};
  }
  
  _GLegPolar_Last_orderP = order; _GLegPolar_Last_RaumR = RaumR; 
  _GLegPolar_Last_RaumP = RaumP;
  
  if (printlevel==DebugPrint){
  
  writeln(_str_greens_integral_GL);
  write("RaumR = "); write(RaumR); write("RaumP = "); writeln(RaumP); 
  write("OrderP = "); write(OrderP); write("OrderP = "); writeln(OrderP);
  write("last_derr = "); writeln(last_derr);}

  last_corr=true; last_derr=max(last_derr, ret_err); last_dres=res2;
  return(last_dres);
}

//
//  real Polar Int with infinity oben limit
//:=int(int(funct(x,y)*r, r=Rmin..infinity), p=Pmin..Pmax); 
// with transformation x=r*cos(p); y=r*sin(p);
//

/*!
  An two-dimensional quadrature over an infinite polar segment.
  The procedure increased the order of the Gauss-Legendre method and, by case,
  the number of pieces to which the integration interval is divided.
  The desired precision must be given to procedure in form of a number of 
  right digits.
  The variable _GLeg_Piece controls the largeness of a piece to which the 
  infinite
  integration interval is divided.

  This is the appropriate to the user routine.
  Integrand must be a function f(x,y) in cartesian coordinate system.
  Procedure returns an integral F
  F = \int_{Rmin}^{Rmax}\int_{Pmin}^{Pmax} f(x,y)\, r dr \,d\phi.
*/
double greens_Gauss_Legendre_Polar_Int(double (*funct)(double, double),
                                      const double Rmin, /*Rmax=infinity*/ 
                                      const double Pmin, const double Pmax, 
				      const int digits)
{
  double err, res1, res2, rmin, rmax; bool bbreak; 
  _GLegPolar_Begin_RaumR=1; _GLegPolar_Begin_RaumP=1; 
  _GLegPolar_Begin_orderR=0; _GLegPolar_Begin_orderP=0;
    
  res2=0; res1=0; err=0; rmin=Rmin; rmax=rmin+_GLeg_Piece;
  for (long i=1;;i++)
  {
   res1=greens_Gauss_Legendre_Polar_Int(funct, rmin, rmax, Pmin, Pmax, digits);
   res2=res2+res1; err=abs((res2*err+res1*last_derr)/res2); rmin=rmax; 
   rmax=rmax+_GLeg_Piece;  bbreak=abs(res1/res2)<Double_Int_dexact;
   
   _GLegPolar_Begin_orderR=_GLegPolar_Last_orderR-1; 
   if (_GLegPolar_Begin_orderR>GLegMax_order-2) 
                   _GLegPolar_Begin_orderR=GLegMax_order-2;
   _GLegPolar_Begin_orderP=_GLegPolar_Last_orderP-1;
   if (_GLegPolar_Begin_orderP>GLegMax_order-2) 
                   _GLegPolar_Begin_orderP=GLegMax_order-2;
   
   _GLegPolar_Begin_RaumR=_GLegPolar_Last_RaumR-1; 
   if (_GLegPolar_Begin_RaumR<1) _GLegPolar_Begin_RaumR=1;
   _GLegPolar_Begin_RaumP=_GLegPolar_Last_RaumP-1; 
   if (_GLegPolar_Begin_RaumP<1) _GLegPolar_Begin_RaumP=1;
   if (bbreak) break;
  }
  
  last_corr=true; last_derr=err; last_dres=res2; return(last_dres);
}

/*!
  An two-dimensional quadrature over an infinite polar segment.
  The procedure increased the order of the Gauss-Legendre method and, by case,
  the number of pieces to which the integration interval is divided.
  The desired precision must be given to procedure in form of a number 
  of right digits.
  The variable _GLeg_Piece controls the largeness of a piece to which 
  the infinite
  integration interval is divided.

  This is the appropriate to the user routine.
  Integrand must be a function f(x,y) in cartesian coordinate system.
  Procedure returns an integral F
  F = \int_{0}^{\infty}\int_{0}^{\pi/2} f(x,y)\, r dr \,d\phi.
*/
double greens_integral_GL(double (*funct)(double, double), 
       const int digits)
{
  double res1, res2, err1, err2;
  res1=greens_Gauss_Legendre_Polar_Int(funct, 0.0, 0.00,  
                                       0.25*Pi, digits); err1=last_derr;
  res2=greens_Gauss_Legendre_Polar_Int(funct, 0.0, 0.25*Pi, 
                                        0.5*Pi, digits); err2=last_derr;

  last_dres=res1+res2; last_derr=abs((err1*res1+err2*res2)/last_dres);  
  return(last_dres);
}

/*!
  The function returns true if some element in the matrix_2x2 is 
  "not a number" -- nan, and false otherwise.
*/
bool greens_isnan(matrix_2x2 m)
{
  if ((greens_isnan(m.e[0][0])!=0) || (greens_isnan(m.e[1][0])!=0) ||
      (greens_isnan(m.e[0][1])!=0) || (greens_isnan(m.e[1][1])!=0)) 
      return(true);
  return(false);
}

/*!
  The function prints a warning-message and setted the elements in the 
  given 2x2 matrix to zero.
  Variable b_nan_integrand is chanded to true.
*/
void nan_warning(matrix_2x2 *m)
{
  greens_WARNING(_str_greens_integral_GL, _str_nan_integrand);
  m->e[0][0] = 0.0; m->e[1][0] = 0.0; m->e[0][1] = 0.0; m->e[1][1] = 0.0;
  b_nan_integrand = true;
}

/*!
  The function calculate an inegral sum for a given polar sector, 
  Orders and Raums.
  The pointers to the appropriate knote and weight arrays must be given also.
  */
matrix_2x2 greens_integral_GL(matrix_2x2 (*funct)(double, double),
                                      const double Rmin, const double Rmax,
                                      const double Pmin, const double Pmax,  
				      const int OrderR, const int OrderP,
                                      const long RaumR, const long RaumP,
				      const double *_r, const double *_p, 
				      const double *wr, const double *wp)
{
  matrix_2x2 res2, resr, f;
  double SubR, SubP, addr, addp, wrp, r_min, r_max, p_min, p_max, ri, phi, 
  cosphi, sinphi; 
  

  /*BEGIN MACRO*/
  res2=matrix_2x2(0,0,0,0);
  SubR=(Rmax-Rmin)/2.0/RaumR; SubP=(Pmax-Pmin)/2.0/RaumP;
  for(long pr=1; pr<=RaumP; pr++){
    p_min=Pmin+(pr-1)*SubP*2.0; p_max=Pmin+pr*SubP*2.0; 
    addp=(p_max+p_min)/2.0;
   
    for(int j=0; j<(OrderP>>1); j++){ 
      phi=addp+SubP*_p[j]; resr=matrix_2x2(0,0,0,0); sinphi=sin(phi); 
      cosphi=cos(phi);
      for(long rr=1; rr<=RaumR; rr++){
        r_min=Rmin+(rr-1)*SubR*2.0; r_max=Rmin+rr*SubR*2.0; 
	addr=(r_max+r_min)/2.0; 
        for(int i=0; i<(OrderR>>1); i++){
          ri=addr+SubR*_r[i]; wrp=wr[i]*wp[j];
	  f = wrp*funct(ri*cosphi, ri*sinphi)*ri; 
	  if (greens_isnan(f)) nan_warning(&f);
	  
          resr=resr+f; ri=addr-SubR*_r[i]; 
	  f = wrp*funct(ri*cosphi, ri*sinphi)*ri; 
	  if (greens_isnan(f)) nan_warning(&f);
	  
          resr=resr+f;}}; 
        res2=res2+resr; 
        phi=addp-SubP*_p[j]; resr=matrix_2x2(0,0,0,0); 
	sinphi=sin(phi); cosphi=cos(phi);
      for(long rr=1; rr<=RaumR; rr++){
        r_min=Rmin+(rr-1)*SubR*2.0; r_max=Rmin+rr*SubR*2.0; 
	addr=(r_max+r_min)/2.0;
        for(int i=0; i<(OrderR>>1); i++){
          ri=addr+SubR*_r[i]; wrp=wr[i]*wp[j];
	  f = wrp*funct(ri*cosphi, ri*sinphi)*ri; 
	  if (greens_isnan(f)) nan_warning(&f);	
	
          resr=resr+f; ri=addr-SubR*_r[i]; 
	  f = wrp*funct(ri*cosphi, ri*sinphi)*ri; 
	  if (greens_isnan(f)) nan_warning(&f);
	
          resr=resr+f;}}; 
      res2=res2+resr;}}; 
  
  res2=res2*SubR*SubP;
  /*END MACRO*/
  return(res2);
}

//
//
//
matrix_2x2 greens_integral_GL(matrix_2x2 (*funct)(double, double),
                                          const double Rmin, const double Rmax,
                                          const double Pmin, const double Pmax, 
					  const int digits)
{ 
  int RaumR, RaumP, OrderR, OrderP, Begin_orderR, Begin_orderP, order;
  double ret_err;
  matrix_2x2   res1, res2;
              
  const double *wr, *wp, *_r, *_p; bool bbreak; 
  Double_Int_dexact=pow(10.0,(double)-digits);

  Begin_orderR=_GLegPolar_Begin_orderR; Begin_orderP=_GLegPolar_Begin_orderP;
  if (((Begin_orderR>=GLegMax_order)||(Begin_orderR<0))||
       ((Begin_orderP>=GLegMax_order)||(Begin_orderP<0)))
    greens_ERROR(_str_greens_Gauss_Legendre_Polar_Int, _str_wrong_Order);

  RaumR=_GLegPolar_Begin_RaumR; RaumP=_GLegPolar_Begin_RaumP;

  if ((RaumR<1)||(RaumR<1)) 
    greens_ERROR(_str_greens_Gauss_Legendre_Polar_Int, _str_wrong_Raum);

  OrderR = _G_Leg_Orders[Begin_orderR]; OrderP = _G_Leg_Orders[Begin_orderP];
  wr = _G_Leg_As[Begin_orderR]; wp = _G_Leg_As[Begin_orderP];
  _r = _G_Leg_xs[Begin_orderR]; _p = _G_Leg_xs[Begin_orderP];
  
  res1=greens_integral_GL(funct, Rmin, Rmax, Pmin, Pmax, 
  OrderR, OrderP, RaumR, RaumP, _r, _p, wr, wp);

// durchversuch r-axes

  for (RaumR=1;; RaumR++){
    for (order=Begin_orderR+1; order<GLegMax_order; order++){
      OrderR = _G_Leg_Orders[order]; wr = _G_Leg_As[order]; 
      _r  = _G_Leg_xs[order];

      res2=greens_integral_GL(funct, Rmin, Rmax, Pmin, 
      Pmax, OrderR, OrderP, RaumR, RaumP, _r, _p, wr, wp);

    last_derr=max_mem(abs_mem(div_mem(res1-res2,res2))); 

    bbreak=(last_derr<=Double_Int_dexact); res1=res2;

    if (bbreak) break;} if (last_derr<Double_Int_dexact) break;}
    
  
  _GLegPolar_Last_orderR=order;
   
// durchversuch y-axes

  res1=res2;
  
  for (RaumP=1;;RaumP++){
    for (order=Begin_orderP+1; order<GLegMax_order; order++){
      OrderP = _G_Leg_Orders[order]; wp = _G_Leg_As[order]; 
      _p  = _G_Leg_xs[order];
      
      res2=greens_integral_GL(funct, Rmin, Rmax, 
      Pmin, Pmax, OrderR, OrderP, RaumR, RaumP, _r, _p, wr, wp);

      ret_err=max_mem(abs_mem(div_mem(res1-res2,res2))); 
    
      bbreak=(ret_err<=Double_Int_dexact); res1=res2;
      if (bbreak) break;}
      
  if (ret_err<Double_Int_dexact) {break;}}
  
  _GLegPolar_Last_orderP = order; _GLegPolar_Last_RaumR = RaumR; 
  _GLegPolar_Last_RaumP = RaumP;
  
  last_corr=true; last_derr=max(last_derr, ret_err); last_mdres=res2;
  return(last_mdres);
}

//
//  real Polar Int with infinity oben limit
//:=int(int(funct(x,y)*r, r=Rmin..infinity), p=Pmin..Pmax); with 
// transformation x=r*cos(p); y=r*sin(p);
//

matrix_2x2 greens_integral_GL(matrix_2x2 (*funct)(double, double),
                                          const double Rmin, /*Rmax=infinity*/ 
                                          const double Pmin, const double Pmax,
					  const int digits)
{
  double err, rmin, rmax; 
  matrix_2x2 res1, res2; bool bbreak;
  
  _GLegPolar_Begin_RaumR=1; _GLegPolar_Begin_RaumP=1;
  _GLegPolar_Begin_orderR=0; _GLegPolar_Begin_orderP=0;

  res2=matrix_2x2(0,0,0,0); res1=matrix_2x2(0,0,0,0); err=0; 
  rmin=Rmin; rmax=rmin+_GLeg_Piece;

  for (long i=1;;i++)
  {
   res1=greens_integral_GL(funct, rmin, rmax, Pmin, Pmax, 
   digits); 
   res2=res2+res1; err=max_mem(abs_mem(div_mem(res2*err+res1*last_derr,res2)));
   rmin=rmax; 
   rmax=rmax+_GLeg_Piece;  
   bbreak=max_mem(abs_mem(div_mem(res1,res2)))<Double_Int_dexact;
   
   _GLegPolar_Begin_orderR=_GLegPolar_Last_orderR-1; 
   if (_GLegPolar_Begin_orderR>GLegMax_order-2) 
            _GLegPolar_Begin_orderR=_GLegPolar_Begin_orderR-2;
   _GLegPolar_Begin_orderP=_GLegPolar_Last_orderP-1;
   if (_GLegPolar_Begin_orderP>GLegMax_order-2) 
            _GLegPolar_Begin_orderP=_GLegPolar_Begin_orderP-2;
   _GLegPolar_Begin_RaumR=_GLegPolar_Last_RaumR-1; 
   if (_GLegPolar_Begin_RaumR<1) _GLegPolar_Begin_RaumR=1;
   _GLegPolar_Begin_RaumP=_GLegPolar_Last_RaumP-1; 
   if (_GLegPolar_Begin_RaumP<1) _GLegPolar_Begin_RaumP=1;
   if (bbreak) break;
  }
  last_corr=true; last_derr=err; last_mdres=res2; return(last_mdres);
}


/*!
  An two-dimensional quadrature over an infinite polar segment.
  The procedure increased the order of the Gauss-Legendre method and, by case,
  the number of pieces to which the integration interval is divided.
  The desired precision must be given to procedure in form of a number of 
  right digits.
  The variable _GLeg_Piece controls the largeness of a piece to which the 
  infinite
  integration interval is divided.

  This is the appropriate to the user routine.
  Integrand must be a function f(x,y) in cartesian coordinate system.
  Procedure returns an integral F
  F = \int_{0}^{\infty}\int_{0}^{\pi/2} f(x,y)\, r dr \,d\phi.
*/
matrix_2x2 greens_integral_GL(matrix_2x2 (*funct)(double, double), 
const int digits)
{
  double err1, err2; matrix_2x2 res1, res2;
  res1=greens_integral_GL(funct, 0.0, 0.0, Pi4, digits); 
  err1=last_derr;
  res2=greens_integral_GL(funct, 0.0, Pi4, Pi2, digits); 
  err2=last_derr;
  last_mdres=res1+res2; 
  last_derr=max_mem(abs_mem(div_mem(err1*res1+err2*res2, last_mdres))); 
  return(last_mdres);
}
