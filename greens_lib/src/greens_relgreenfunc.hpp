#ifndef __GREENS_RELGREENFUNC_HPP
#define __GREENS_RELGREENFUNC_HPP

#include "greens_mdclass.hpp"
#include "greens_const.hpp"
#include "greens_specfunc.hpp"
/*!
  The function returns a Coulomb radial Green's function 
  which is in relativistic case a 2x2 matrix.
  Energy must be negative.
  Nuclear charge can be defined by using 
  greens_set_nuclear_charge() function  
*/
matrix_2x2 greens_radial_matrix(double E, int kappa, 
                                     double r1, double r2);

/*!
  The function returns a Coulomb radial Green's function 
  which is in relativistic case a 2x2 matrix.
  Energy must be less than rest energy of electron.
  There is Drake definition
  \bibitem{Drake:91:2} R.~A.~Swainson and G.~W.~F.~Drake,
  J. Phys. A {\bf 24} (1991) 95.

  Nuclear charge can be defined by using 
  greens_set_nuclear_charge() function 
*/
matrix_2x2 greens_radial_matrix_Drake(double E, int kappa, 
                                           double r1, double r2);

#endif
