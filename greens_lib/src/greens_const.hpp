#ifndef __GREENS_CONST_HPP
#define __GREENS_CONST_HPP
//
// --------------------- mathematic constants ------------------------
//
const double Pi =
  3.141592653589793238462643383279502884197169399375105820974944592307816;
const double Pi2 =
  1.570796326794896619231321691639751442098584699687552910487472296153908;
const double Pi3 =
  1.047197551196597746154214461093167628065723133125035273658314864102606;
const double Pi4 =
  .785398163397448309615660845819875721049292349843776455243736148076954;
const double gamma_Euler =
  .577215664901532860606512090082402431042159335939923598805767234884868;
const double sqrtTwoPi =
  2.506628274631000502415765284811045253006986740609938316629923576342294;
const double sqrtPi =
  1.772453850905516027298167483341145182797549456122387128213807789852911;
const double TwoPi =
  6.283185307179586476925286766559005768394338798750211641949889184615632;
const double lnsqrtTwoPi =
  .918938533204672741780329736405617639861397473637783412817151540482766;
const double sqrt2 =
  1.414213562373095048801688724209698078569671875376948073176679737990733;
const double PsiOf2 =
  .422784335098467139393487909917597568957840664060076401194232765115132;

//
// --------------------- physical constants --------------------------
//
const double greens_constant_alpha = .7297352533e-2;
const double greens_constant_powalpha2 = .53251353990881516089e-4;
const double greens_constant_powalpha3 =
  .388593902931038890334944403437e-6;
const double greens_constant_powalpha4 =
  .2835706701862172770707215760835165855921e-8;
const double greens_constant_powalphaminus1 =
  137.0359997653686056337330578571898631336138026209840505179825605117165;
const double greens_constant_powalphaminus2 =
  18778.86523169410453830037785529442272507705399916493941406003887680295;
const double greens_constant_au_E0 =
  18778.86523169410453830037785529442272507705399916493941406003887680295;

const double greens_constant_cgs_F0 = .3219863362e35;
const double greens_constant_SI_a0  = .5291772082e-10;
const double greens_constant_cgs_a0 = .5291772082e-8;
const double greens_constant_SI_t0  = .24188843259397284e-16;
const double greens_constant_cgs_t0 = .24188843259397284e-16;

const double greens_constant_SI_c = 299792458.0;
const double greens_constant_cgs_c = 29979245800.0;
const double greens_constant_au_c =
  137.0359997653686056337330578571898631336;
//
const double greens_constant_SI_me  = 9.10938188e-31;
const double greens_constant_cgs_me = 9.10938188e-28;
const double greens_constant_au_me  = 1.0;
//
const double greens_constant_SI_e   = 1.602176462e-19;
const double greens_constant_cgs_e  = 4.8032041969E-10;
const double greens_constant_au_e   = 1.0;
//
const double greens_constant_SI_e0  = 1.602176462e-19;
const double greens_constant_cgs_e0 = 4.8032041969E-10;
const double greens_constant_au_e0  = 1.0;

//
const double greens_constant_SI_hbar = 1.054571596E-34;
const double greens_constant_cgs_hbar = 1.054571596E-27;
const double greens_constant_au_hbar = 1.0;

const double greens_constant_SI_mp = 1.67262158e-27;
const double greens_constant_SI_h = 6.62606876E-34;
const double greens_constant_SI_amu = 1.66053873E-27;
const double greens_constant_SI_eV = 1.602176462E-19;
//

#define greens_Constant_SI_c            greens_constant_SI_c
#define greens_Constant_cgs_c           greens_constant_cgs_c
#define greens_Constant_au_c            greens_constant_au_c

#define greens_Constant_SI_me           greens_constant_SI_me
#define greens_Constant_cgs_me          greens_constant_cgs_me
#define greens_Constant_au_me           greens_constant_au_me

#define greens_Constant_SI_e            greens_constant_SI_e
#define greens_Constant_cgs_e           greens_constant_cgs_e
#define greens_Constant_au_e            greens_constant_au_e

#define greens_Constant_SI_hbar         greens_constant_SI_hbar
#define greens_Constant_cgs_hbar        greens_constant_cgs_hbar
#define greens_Constant_au_hbar         greens_constant_au_hbar

#define greens_Constant_alpha           greens_constant_alpha
#define greens_Constant_powalpha2       greens_constant_powalpha2
#define greens_Constant_powalpha3       greens_constant_powalpha3
#define greens_Constant_powalpha4       greens_constant_powalpha4
#define greens_Constant_powalphaminus1  greens_constant_powalphaminus1
#define greens_Constant_powalphaminus2  greens_constant_powalphaminus2
#define greens_Constant_cgs_F0          greens_constant_cgs_F0
#define greens_Constant_cgs_a0          greens_constant_cgs_a0
#define greens_Constant_au_E0           greens_constant_au_E0
//
#define greens_Constant_SI_mp           greens_constant_SI_mp
#define greens_Constant_SI_h            greens_constant_SI_h
#define greens_Constant_SI_amu          greens_constant_SI_amu
#define greens_Constant_SI_eV           greens_constant_SI_eV

#endif
