#include "greens_tpcsrtm.hpp"
/*!
 Two photon total ionization cross section 
 [E_p] photon energy au
 [TPCSRTM] = cm^4 sec
 nonpolarized light
*/ 
double greens_TPCSRTM(double E_p, int lmax, int electr, int magn, int d)
{
  double result, c1, c2, c3, c4, c5;  int kappa_0;
  dcomplex ME, smd, ccY, cc1, cc2, cc3;
  double m, mu_1, j_1, j, j_0;
  int l, l_1; 
  matrix_2x2 mU;
    
  kappa_0 = -1; j_0 = abs(kappa_0)-0.5;
  int kappa_1_max = lmax*2+1;
  int kappa_max = lmax+1;
  _mU_n2 = 1;
  

  result=0.0;
  for (int lambda_1 = -1; lambda_1<=1; lambda_1++){ if (lambda_1==0) continue;
  for (int lambda_2 = -1; lambda_2<=1; lambda_2++){ if (lambda_2==0) continue;
  for (double mu_0 = -(abs(kappa_0)-.5); mu_0 <= (abs(kappa_0)-.5); mu_0 = mu_0 + 1.){
    
    m = lambda_2 + mu_0;
    mu_1 = lambda_1 + lambda_2 + mu_0;
  for (int kappa_1 = -kappa_1_max; kappa_1 <= kappa_1_max; kappa_1++){ if (kappa_1==0) continue;
  
  ME  = 0.0;
  
    l_1 = greens_angular_l(kappa_1); j_1 = abs(kappa_1)-0.5;
    c1 = 1.0; 
    ccY = 1.0;
    cc1 = 1.0;
    
  for (int kappa   = -kappa_max; kappa <= kappa_max; kappa++){ if (kappa==0) continue;
    l   = greens_angular_l(kappa);   j = abs(kappa)-0.5;
    
  for (int L_1    = 1; L_1<=lmax; L_1++){
    c2 = greens_ClebschGordan(j, m, L_1, lambda_1, j_1, mu_1);
    if (c2 == 0.0) continue;
  for (int L_2    = 1; L_2<=lmax; L_2++){
    c3 = greens_ClebschGordan(j_0, mu_0, L_2, lambda_2, j, m);
    if (c3 == 0.0) continue;
    
    for(int Lambda_1 = L_1-1; Lambda_1 <= L_1+1; Lambda_1++){
    cc2 = greens_xi(L_1,Lambda_1,lambda_1, electr, magn);
    for(int Lambda_2 = L_2-1; Lambda_2 <= L_2+1; Lambda_2++){
    cc3 = greens_xi(L_2,Lambda_2,lambda_2, electr, magn);
 
      for(int i1=1; i1<=2; i1++){
        c4 = _greens_RME_T(kappa_1, kappa, Lambda_1, L_1, i1);
        if (c4==0) continue;
      
      for(int i2=1; i2<=2; i2++){
        c5 = _greens_RME_T(kappa, kappa_0, Lambda_2, L_2, i2+1);
	if (c5==0) continue;
	
        smd =   ccY * cc1 * cc2 * cc3 * c1 * c2 * c3 *  c4 * c5 * pow_minus(i1+i2)
              * pow(cdI, L_1+L_2)*sqrt((L_1+L_1+1.0)*(L_2+L_2+1.0))
	      / sqrt((j_1+j_1+1.0)*(j+j +1.0));
              
	 if (smd == 0.0) continue;
     
         // check whether the initial quantum numbers appears
	 if ((mu_0==m) & (kappa_0 == kappa)) {
	    print("TPCSTM_circ : ");
	    write(mu_0); write("=="); write(m);
	    write(kappa_0); write("=="); print(kappa);}
	    
         mU  = 
	   greens_mU(Lambda_1, Lambda_2, E_p, kappa_1, kappa, kappa_0, d);
	 
         ME = ME + smd * mU.e[i1-1][i2-1];

      }}}}}}}
      result = result + 0.5*Pi*Pi*pow(abs(ME), 2.0);
      }}}}

  double coeff = TwoPi*greens_constant_cgs_c/pow(greens_constant_alpha, 3.0)/
          (greens_constant_cgs_a0*greens_constant_cgs_F0*greens_constant_cgs_F0*E_p*E_p);
  
  result = result * coeff;
return(result);
}


/*!
 Two photon total ionization cross section 
 [E_p] photon energy au
 [TPCSRTM] = cm^4 sec
 circular polarized light (left/right)
*/ 
double greens_TPCSRTM_circ(double E_p, int lmax, int electr, int magn, 
                            int lambda_2, int lambda_1, int d)
{
  double result, c2, c3, c4, c5, coeff;  
  int kappa_i, l_nu, l_f;
  dcomplex ME, smd, cc2, cc3;
  double m_nu, m_f, j_f, j_nu, j_i;

  matrix_2x2 mU;  _mU_n2 = 1;    

  kappa_i = -1; j_i = abs(kappa_i)-0.5;

  int kappa_f_max  = lmax+lmax+1;
  int kappa_nu_max = lmax+1;

  result=0.0;
  for (double m_i = -(abs(kappa_i)-.5); m_i<=(abs(kappa_i)-.5); m_i = m_i + 1.){
      
    m_nu = lambda_2 + m_i;
    m_f  = lambda_1 + lambda_2 + m_i;
  for (int kappa_f  = -kappa_f_max; kappa_f <= kappa_f_max; kappa_f++){ 
    if (kappa_f==0) continue;
  
  ME  = 0.0;
    l_f  = greens_angular_l(kappa_f);  j_f  = abs(kappa_f)  - 0.5;
  for (int kappa_nu = -kappa_nu_max; kappa_nu <= kappa_nu_max; kappa_nu++){ 
    if (kappa_nu==0) continue;
    l_nu = greens_angular_l(kappa_nu); j_nu = abs(kappa_nu) - 0.5;
  for (int L  = 1; L<=lmax; L++){
    c2 = greens_ClebschGordan(j_nu, m_nu, L, lambda_2, j_f, m_f);
    if (c2 == 0.0) continue;
  for (int L_ = 1; L_<=lmax; L_++){
    c3 = greens_ClebschGordan(j_i, m_i, L_, lambda_2, j_nu, m_nu);
    if (c3 == 0.0) continue;
    
    for(int Lambda = L-1; Lambda <= L+1; Lambda++){
      cc2 = greens_xi(L, Lambda,lambda_2, electr, magn);
      for(int Lambda_ = L_-1; Lambda_ <= L_+1; Lambda_++){

        cc3 = greens_xi(L_,Lambda_,lambda_1, electr, magn);
        
        for(int T=0; T<=1; T++){
          c4 = greens_RME_T(kappa_f,  
	                    greens_angular_l(kappa_f,  greens_invert_T(T)), 
                            L, Lambda, 
                            kappa_nu, greens_angular_l(kappa_nu, T));

          if (c4==0) continue;
      
          for(int T_=0; T_<=1; T_++){
            c5 = greens_RME_T(kappa_nu, greens_angular_l(kappa_nu, T_), 
                              L_, Lambda_, kappa_i, 
                              greens_angular_l(kappa_i, greens_invert_T(T_)));

            if (c5==0) continue;

            smd =  sqrt2 * Pi * cc2 * cc3 * c2 * c3 *  c4 * c5 * 
                   pow_minus(T+T_) * pow(cdI, L+L_) * 
		   sqrt((L+L+1.0)*(L_+L_+1.0)) /
                   sqrt((j_f+j_f+1.0)*(j_nu+j_nu +1.0));
                          
            if (smd == 0.0) continue;
            
	    // check whether the initial quantum numbers appears
	    if ((m_i==m_nu) & (kappa_i == kappa_nu)) {
	      print("TPCSTM_circ : ");
	      write(m_nu); write("=="); write(m_i);
	      write(kappa_i); write("=="); print(kappa_nu);}

            mU  = 
              greens_mU(Lambda, Lambda_, E_p, kappa_f, kappa_nu, kappa_i, d);
            ME = ME + smd * mU.e[T][T_];
        }}}}}}}
      result = result + pow(abs(ME), 2.0);
      }}

    coeff = TwoPi*greens_constant_cgs_c/pow(greens_constant_alpha, 3.0)/
         (greens_constant_cgs_a0*pow(greens_constant_cgs_F0, 2.0)*E_p*E_p);

    coeff = 8.0*pow(Pi,3.0)*greens_constant_powalpha2/(E_p*E_p) /
            pow(greens_constant_alpha, 4.0);
      
  last_dres = result * coeff;
return(last_dres);
}

//
// Two photon total ionization cross section 
// [E_p] photon energy au
// [TPCSRTM] = cm^4 sec
// circular polarized light (left/right) int wave approximation
// 
double greens_TPCSRTM_lw_circ(double E_p, int lambda_1, int lambda_2, int digits)
{
  double result, c2, c3, c4, c5;  
  int kappa_0, lmax=1;
  dcomplex ME, smd, cc2, cc3;
  double m, mu_1, j_1, j, j_0;
  int l, l_1; 
  matrix_2x2 mU;
  int electr = 1, magn=0;
    
  kappa_0 = -1; j_0 = abs(kappa_0)-0.5;
  int kappa_1_max = lmax*2+1;
  int kappa_max = lmax+1;
  _mU_n2 = 1;
  

  result=0.0;
  for (double mu_0 = -(abs(kappa_0)-.5); mu_0 <= (abs(kappa_0)-.5); mu_0 = mu_0 + 1.){
    
    m = lambda_2 + mu_0;
    mu_1 = lambda_1 + lambda_2 + mu_0;
  for (int kappa_1 = -kappa_1_max; kappa_1 <= kappa_1_max; kappa_1++){ if (kappa_1==0) continue;
  
  ME  = 0.0;
  
    l_1 = greens_angular_l(kappa_1); j_1 = abs(kappa_1)-0.5;
    
  for (int kappa   = -kappa_max; kappa <= kappa_max; kappa++){ if (kappa==0) continue;
    l   = greens_angular_l(kappa);   j = abs(kappa)-0.5;
    
  for (int L_1    = 1; L_1<=lmax; L_1++){
    
    c2 = greens_ClebschGordan(j, m, L_1, lambda_1, j_1, mu_1);
    if (c2 == 0.0) continue;
  for (int L_2    = 1; L_2<=lmax; L_2++){
    
    c3 = greens_ClebschGordan(j_0, mu_0, L_2, lambda_2, j, m);
    if (c3 == 0.0) continue;
    
    for(int Lambda_1 = 0; Lambda_1 < 1; Lambda_1++){
      cc2 = greens_xi(L_1,Lambda_1,lambda_1, electr, magn);
    for(int Lambda_2 = 0; Lambda_2 < 1; Lambda_2++){
      cc3 = greens_xi(L_2,Lambda_2,lambda_2, electr, magn);

      mU  = greens_mU_lw(E_p, kappa_1, kappa, kappa_0, digits);	 
      for(int i1=1; i1<=2; i1++){
        c4 = _greens_RME_T(kappa_1, kappa, Lambda_1, L_1, i1);
        if (c4==0) continue;
      
      for(int i2=1; i2<=2; i2++){
        c5 = _greens_RME_T(kappa, kappa_0, Lambda_2, L_2, i2+1);
	if (c5==0) continue;
	
        smd =   sqrt2 * Pi * cc2 * cc3 * c2 * c3 *  c4 * c5 * pow_minus(i1+i2)
              * pow(cdI, L_1+L_2)*sqrt((L_1+L_1+1.0)*(L_2+L_2+1.0))
	      / sqrt((j_1+j_1+1.0)*(j+j +1.0));

	 if (smd == 0.0) continue;
	 
//       write(Lambda_1); write(Lambda_2); write(kappa_1); write(kappa); print(kappa_0);

         ME = ME + smd * mU.e[i1-1][i2-1];

//	 if ((kappa_1 ==+2) && (kappa ==-2) && (kappa_0 ==-1)) {
//	     write(Lambda_1); write(Lambda_2); write(i1-1); write(i2-1); print(-2*smd*sqrt(2700.0)); }

      }}}}}}}
      result = result + pow(abs(ME), 2.0);
      }}

  // double coeff = TwoPi*greens_constant_cgs_c/pow(greens_constant_alpha, 3)/
  //       (greens_constant_cgs_a0*pow(greens_constant_cgs_F0, 2.0)*E_p*E_p);
    
  double coeff = 8.0*pow(Pi,3.0)*greens_constant_powalpha2/(E_p*E_p);
    
  result = result * coeff;
return(result);
}
