/*!
  Functions and constants for print redirection and messages from functions
  ERROR
  WARNING
  MESSAGE
  also for setting the nuclear charge and Digits variable
*/
#ifndef GREENS_SERVICE_HPP
#define GREENS_SERVICE_HPP

#include <math.h>
#include <stdio.h>
#include <cstdlib>

#include "greens_dcomplex.hpp"

extern FILE *greens_stdin;
extern FILE *greens_stdout;
extern char *_str_stdout;
extern FILE *greens_stderr;


const int terminal=1;

//
// ------------------- strings ----------------------------------------
//
extern const char* _str_Digits;

//
// ------------------- greens_ERROR ------------------------------------
//
// ------------------- Names of the functions--------------------------
extern const char* _str_greens_message;
extern const char* _str_greens_L;
extern const char* _str_greens_F2;
extern const char* _str_greens_nonrel_radial_me;
extern const char* _str_greens_cKummerM;
extern const char* _str_greens_rKummerM;
extern const char* _str_greens_spherical_j;
extern const char* _str_greens_Clenshaw_Curtis_Int;
extern const char* _str_greens_Gauss_Legendre_Polar_Int;
extern const char* _str_set_Digits;
extern const char* _str_greens_add_new_argument;
extern const char* _str_greens_energy_width_H_non;
extern const char* _str_greens_lifetime_H_non;
extern const char* _str_greens_A_n1l1n2l2;
extern const char* _str_greens_A_nl;
extern const char* _str_greens_nonrel_OPCS;
extern const char* _str_greens_TPCSN;
extern const char* _str_greens_w3j;
extern const char* _str_greens_w6j;
extern const char* _str_greens_w9j;
extern const char* _str_greens_angular_l;
extern const char* _str_greens_ClebschGordan;
extern const char* _str_greens_reduced_me;
extern const char* _str_greens_Ylm;
extern const char* _str_greens_radial_Green_function;
extern const char* _str_greens_radial_Green_function_Drake;
extern const char* _str_radial_Green_matrix;
extern const char* _str_radial_Green_matrix_Drake;
extern const char* _str_radial_spinor;
extern const char* _str_greens_energy;
extern const char* _str_greens_TPCSR;
extern const char* _str_greens_xi;


// ------------------- description of the errors-----------------------
extern const char* _str_greens_error;
extern const char* _str_ERROR;
extern const char* _str_wrong_args;
extern const char* _str_singularity;
extern const char* _str_wrong_n;
extern const char* _str_wrong_r;
extern const char* _str_wrong_E;
extern const char* _str_wrong_energy;
extern const char* _str_wrong_kappa;
extern const char* _str_wrong_coords;
extern const char* _str_wrong_l;
extern const char* _str_wrong_n_or_l;
extern const char* _str_wrong_m;
extern const char* _str_wrong_Digits;
extern const char* _str_wrong_nuclear_charge;
extern const char* _str_wrong_Raum;
extern const char* _str_wrong_Order;
extern const char* _str_wrong_state;
extern const char* _str_routine_error;
extern const char* _str_wrong_Begin_order;
extern const char* _str_wrong_Begin_Raum;
extern const char* _str_args_maybe_integer_or_halfinteger;
extern const char* _str_wrong_electric;
extern const char* _str_wrong_magnetic;
extern const char* _str_wrong_lambda;

//
// ------------------- greens_WARNING --------------------------------------
//
extern const char* _str_greens_warning;
extern const char* _str_WARNING;
extern const char* _str_precision_not_reached;
extern const char* _str_all_methods_wrong;
extern const char* _str_debug_reason;
extern const char* _str_Debug;
extern const char* _str_main;
extern const char* _str_wrong_method;
extern const char* _str_too_much_records;
extern const char* _str_perhaps_series_not_convergent;
extern const char* _str_final_state_lay_down;

/*!
  The relative error of a "double" number
*/
const double           double_exact=0.5e-14;

/*!
  Variable Digits represent a number of desired precise digits 
  by calculating some special functions 
*/
extern int ngreens_save_Digits; 
extern int             Digits;

/*!
  Variable for the temporary store of greens_dexact
*/
extern double      greens_save_dexact;

/*!
  Desired relative error for some (double precision) special functions
  greens_lexact and greens_dexact controlled by 
  greens_set_Digits()
*/
extern double      greens_dexact;

//
// ------------------- Dirac physical data -----------------------------------
//

extern double greens_save_nuclear_charge;
extern const char* _str_set_nuclear_charge;

//-------------------- Last error ----------------------------------------
//-------------------- Last correctness of the method ---------------------
//
extern double      last_derr;   // error estimation
extern bool        last_corr;  // correctness 
//
//-------------------- Last results --------------------------------------
extern double      last_dres;
extern dcomplex    last_cdres;

//
//
void writeto(char *_str);
void writeto(int term);

void appendto(char *_str);

// integer ?
bool greens_IsEven(long z);
bool greens_IsOdd(long  z);

//
//
//
bool greens_IsInteger(double z);
bool greens_IsPosInteger(double z);
bool greens_IsNonNegInteger(double z);
bool greens_IsNonPosInteger(double z);
bool greens_IsEven(int z);
bool greens_IsOdd(int  z);

bool greens_IsPositive(double z);
bool greens_IsNegative(double z);
bool greens_IsComplex(dcomplex z);
bool greens_IsComplex(double z);

//
// print write and writeln functions
//
void print(bool z);
void writeln(bool z);
void write(bool z);

void print(dcomplex a);
void writeln(dcomplex a);
void write(dcomplex a);

void print(double a);
void writeln(double a);
void write(double a);

void print(float a);
void writeln(float a);
void write(float a);

void print(int a);
void writeln(int a);
void write(int a);

void print(unsigned int a);
void writeln(unsigned int a);
void write(unsigned int a);

void print(long int a);
void writeln(long int a);
void write(long int a);

void print(long long a);
void writeln(long long a);
void write(long long a);

void print(unsigned long a);
void writeln(unsigned long a);
void write(unsigned long a);

void print(unsigned long long a);
void writeln(unsigned long long a);
void write(unsigned long long a);

void print(short a);
void writeln(short a);
void write(short a);

void print(unsigned short a);
void writeln(unsigned short a);
void write(unsigned short a);

void print(char *a);
void writeln(char *a);
void write(char *a);

void print(const char* _str);
void writeln(const char* _str);
void write(const char* _str);

void print(void);
void writeln(void);
void write(void);

double greens_get_last_err(void);
double greens_get_last_res(void);
bool greens_get_last_corr(void);

#define Stop 2
#define NoStop 0
#define Break 2
#define NoBreak 0
#define Print 4 
#define NoPrint 0
#define DebugPrint 1000
#define NoDebugPrint 0


extern int warningbreak;
extern int errorbreak;
extern int messagebreak;

extern int printlevel; 
extern int _greens_Err_Code;
extern int save_errlevel, save_warnlevel, save_msglevel, 
           save_errorbreak, save_warningbreak;
extern int warnlevel; 
extern int msglevel; 
extern int errlevel;

void greens_set_warnlevel(int w);
int greens_get_warnlevel(void);
//
void greens_ERROR(const char* _str_proc, const char* _str_err);
void ERROR(const char* _str_);
//
//
void WARNING(const char* _str_);
//
//
void greens_WARNING(const char* _str_proc, const char* _str_warn);
//
//
void greens_MESSAGE(int);
//
//
void greens_MESSAGE(const char *);

int greens_set_Digits(int d);
int greens_get_Digits(void);

//
//-------------------- Set nuclear charge ------------------------------------
// (variable greens_save_nuclear_charge);
//

void greens_set_nuclear_charge(double charge);
double greens_get_nuclear_charge(void);

//
// Save levels to save_*? variabels
//
void greens_save_mwe_levels(void);

//
// Load levels from save_*? variabels
//
void greens_load_mwe_levels(void);
//
//
// Save Digits
//
void greens_save_Digits(void);
void greens_save_Digits(int digits);
//
// Load Digits
//
void greens_load_Digits(void);

#endif
