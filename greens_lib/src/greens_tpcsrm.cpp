
#include "greens_tpcsrm.hpp"
//
// Integrand 1 for Two Photon Cross Section relativistic case 
//

int  _mU_n2=-1, _mU_kappa2, _mU_kappa, _mU_kappa1, _mU_Lambda1, _mU_Lambda2;
double _mU_E1, _mU_E, _mU_k; 
//
// integrand for greens_mU
//
matrix_2x2 mIntegrand_mU(double r1, double r2)
{
  matrix_2x2 g, res; spinor2_col f2, f1; double j1j2;

  f1 = greens_radial_spinor(_mU_E1, _mU_kappa1, r1);
  f2 = greens_radial_spinor(_mU_n2, _mU_kappa2, r2);
  g  = greens_radial_matrix(_mU_E, _mU_kappa, r1, r2);
  
  j1j2=greens_spherical_j(_mU_Lambda1, _mU_k*r1) *
       greens_spherical_j(_mU_Lambda2, _mU_k*r2);
  
//  write(_mU_Lambda2); write(_mU_k*r2); print(greens_spherical_j(_mU_Lambda2, _mU_k*r2));

  res.e[0][0]=f1.S*g.e[0][0]*f2.S; res.e[0][1]=f1.S*g.e[0][1]*f2.L;
  res.e[1][0]=f1.L*g.e[1][0]*f2.S; res.e[1][1]=f1.L*g.e[1][1]*f2.L;

  if (!(greens_isnan(res.e[0][0])==0)) print("NAN Green");;
  if (!(greens_isnan(r1)==0)) print("NAN r1");;

  return(res*j1j2);
}
//
// 
//
int _greens_mU_digits_ = 2;
//
matrix_2x2 greens_mU(int Lambda1, int Lambda2, double Ep, int kappa1, int kappa, int kappa2)
{
     matrix_2x2 result; _GLeg_Piece=5.0/greens_save_nuclear_charge;
     _mU_E1      = greens_energy(_mU_n2, kappa2)+Ep+Ep; 
     _mU_kappa1  = kappa1; _mU_kappa  = kappa; _mU_kappa2 = kappa2;
     _mU_E       = _mU_E1-Ep; _mU_k = Ep*greens_constant_alpha; 
     _mU_Lambda1 = Lambda1; _mU_Lambda2 = Lambda2;
     result      = greens_integral_GL(mIntegrand_mU, _greens_mU_digits_);
return(result);
}
//
//
//
double * _greens_mU_Ep=NULL, *_greens_mU_Z=NULL;
matrix_2x2   *_greens_mU_result=NULL; 
int *_greens_mU_n2=NULL,     *_greens_mU_Lambda1=NULL, *_greens_mU_Lambda2=NULL, 
     *_greens_mU_kappa1=NULL, *_greens_mU_kappa=NULL,   *_greens_mU_kappa2=NULL;
int  *_greens_mU_Digits=NULL, *_greens_mU_digits=NULL;
long _greens_mU_number = 0;

matrix_2x2 greens_mU(int Lambda1, int Lambda2, double Ep, 
                   int kappa1, int kappa, int kappa2, int digits)
{ 
  for (long i = 0; i<_greens_mU_number; i++ ){
    if ((_greens_mU_n2[i] == _mU_n2) &&
        (_greens_mU_Lambda1[i] == Lambda1) &&
	(_greens_mU_Lambda2[i] == Lambda2) &&
        (_greens_mU_kappa1[i] == kappa1) &&
	(_greens_mU_kappa[i] == kappa) &&
	(_greens_mU_kappa2[i] == kappa2) &&
	(_greens_mU_digits[i] == digits) &&
	(_greens_mU_Digits[i] == Digits) &&
	(_greens_mU_Ep[i] == Ep) &&
	(_greens_mU_Z[i] == greens_save_nuclear_charge))
	return(_greens_mU_result[i]);}
	
    _greens_mU_number  = _greens_mU_number + 1;
    _greens_mU_n2      = (int *) realloc(_greens_mU_n2, _greens_mU_number*sizeof(int));
    _greens_mU_Lambda1 = (int *) realloc(_greens_mU_Lambda1, _greens_mU_number*sizeof(int));
    _greens_mU_Lambda2 = (int *) realloc(_greens_mU_Lambda2, _greens_mU_number*sizeof(int));
    _greens_mU_kappa1  = (int *) realloc(_greens_mU_kappa1, _greens_mU_number*sizeof(int));
    _greens_mU_kappa   = (int *) realloc(_greens_mU_kappa, _greens_mU_number*sizeof(int));
    _greens_mU_kappa2  = (int *) realloc(_greens_mU_kappa2, _greens_mU_number*sizeof(int));
    _greens_mU_digits  = (int *) realloc(_greens_mU_digits, _greens_mU_number*sizeof(int));
    _greens_mU_Digits  = (int *) realloc(_greens_mU_Digits, _greens_mU_number*sizeof(int));
    _greens_mU_Ep      = (double*) realloc(_greens_mU_Ep, _greens_mU_number*sizeof(double));
    _greens_mU_Z       = (double*) realloc(_greens_mU_Z, _greens_mU_number*sizeof(double));
    _greens_mU_result  = (matrix_2x2*) realloc(_greens_mU_result, _greens_mU_number*sizeof(matrix_2x2));
  
    _greens_mU_n2[_greens_mU_number-1]      = _mU_n2;
    _greens_mU_Lambda1[_greens_mU_number-1] = Lambda1;  
    _greens_mU_Lambda2[_greens_mU_number-1] = Lambda2;
    _greens_mU_kappa1[_greens_mU_number-1]  = kappa1;  
    _greens_mU_kappa[_greens_mU_number-1]   = kappa;
    _greens_mU_kappa2[_greens_mU_number-1]  = kappa2;  
    _greens_mU_digits[_greens_mU_number-1]  = digits;
    _greens_mU_Digits[_greens_mU_number-1]  = Digits;
    _greens_mU_Ep[_greens_mU_number-1]      = Ep;
    _greens_mU_Z[_greens_mU_number-1]       = greens_save_nuclear_charge;
    
  _greens_mU_digits_ = digits;
  _greens_mU_result[_greens_mU_number-1] = greens_mU(Lambda1, Lambda2, Ep, kappa1, kappa, kappa2);
  
//  
//  _greens_mU_result[_greens_mU_number-1] = matrix_2x2(Lambda1*1.1, Lambda1*1.2, 
//			                           Lambda2*2.1, Lambda2*2.2);

//  _greens_mU_result[_greens_mU_number-1] = 
//  matrix_2x2(kappa1*kappa*kappa2*1.1, kappa1*kappa*kappa2*1.2, 
//	      kappa1*kappa*kappa2*2.1, kappa1*kappa*kappa2*2.2);

  if (msglevel==Print){
    fprintf(greens_stdout, "# greens_mU: Number of radial integrals calculated %ld\n",
                        _greens_mU_number ); fflush(greens_stdout);}

  return(_greens_mU_result[_greens_mU_number-1]); 
}

//
// int-wavelength radial integrals which derived from multipole ones
//
matrix_2x2 greens_mU_lw(double Ep, int kappa1, int kappa, int kappa2){

  matrix_2x2 result; 
  _GLeg_Piece=10.0/greens_save_nuclear_charge;
  _mU_E1      = greens_energy(_mU_n2, kappa2)+Ep+Ep; 
  _mU_kappa1  = kappa1; _mU_kappa  = kappa; _mU_kappa2 = kappa2;
  _mU_E       = _mU_E1-Ep; _mU_k = 0.0; 
  _mU_Lambda1 = 0; _mU_Lambda2 = 0;
  result      = greens_integral_GL(mIntegrand_mU, _greens_mU_digits_);
return(result);
}
//
//
//
int _greens_mU_lw_number = 0;

matrix_2x2 greens_mU_lw(double Ep, int kappa1, int kappa, int kappa2, int digits)
{ 
  matrix_2x2 res = greens_mU_lw(Ep, kappa1, kappa, kappa2);

  if (msglevel==Print){
    fprintf(greens_stdout, "# greens_mU_lw: Number of radial integrals calculated %d\n",
                        _greens_mU_lw_number ); fflush(greens_stdout);}
  _greens_mU_lw_number = _greens_mU_lw_number + 1;
  return(res); 
}
//
//
//
// Two Photon Cross Section Relativistic case circular polarized light
//
double greens_TPCSR_e1_circ(double Ep, int digits)
{
  double coeff;
  matrix_2x2 U_02_m3m2m1, U_20_m3m2m1, U_00_m3m2m1, U_22_m3m2m1,
            U_02_p2m2m1, U_20_p2m2m1, U_00_p2m2m1, U_22_p2m2m1,
            U_02_p2p1m1, U_20_p2p1m1, U_00_p2p1m1, U_22_p2p1m1;

  double err[12];

  _GLeg_Piece=5.0/greens_save_nuclear_charge;
  _mU_n2=1;

  coeff = TwoPi*greens_constant_cgs_c/pow(greens_constant_alpha, 3.0)/
          (greens_constant_cgs_a0*greens_constant_cgs_F0*greens_constant_cgs_F0*Ep*Ep);

  U_00_m3m2m1=greens_mU(0, 0, Ep, -3, -2, -1, digits); err[0]=last_derr;
  U_02_m3m2m1=greens_mU(0, 2, Ep, -3, -2, -1, digits); err[1]=last_derr;
  U_20_m3m2m1=greens_mU(2, 0, Ep, -3, -2, -1, digits); err[2]=last_derr;
  U_22_m3m2m1=greens_mU(2, 2, Ep, -3, -2, -1, digits); err[3]=last_derr;

  U_00_p2m2m1=greens_mU(0, 0, Ep, +2, -2, -1, digits); err[4]=last_derr;
  U_02_p2m2m1=greens_mU(0, 2, Ep, +2, -2, -1, digits); err[5]=last_derr;
  U_20_p2m2m1=greens_mU(2, 0, Ep, +2, -2, -1, digits); err[6]=last_derr;
  U_22_p2m2m1=greens_mU(2, 2, Ep, +2, -2, -1, digits); err[7]=last_derr;

  U_00_p2p1m1=greens_mU(0, 0, Ep, +2, +1, -1, digits); err[8]=last_derr;
  U_02_p2p1m1=greens_mU(0, 2, Ep, +2, +1, -1, digits); err[9]=last_derr;
  U_20_p2p1m1=greens_mU(2, 0, Ep, +2, +1, -1, digits); err[10]=last_derr;
  U_22_p2p1m1=greens_mU(2, 2, Ep, +2, +1, -1, digits); err[11]=last_derr;

  last_derr=err[0]; for(int i=1; i<12; i++){if (last_derr<err[i]) last_derr=err[i];};

  last_dres=pow(4*U_02_m3m2m1.e[1][0]-12*U_02_m3m2m1.e[1][1]-12*U_20_m3m2m1.e[0][0]+
                4*U_20_m3m2m1.e[1][0]+16*U_00_m3m2m1.e[1][0]-3 *U_22_m3m2m1.e[0][0]+
                9*U_22_m3m2m1.e[0][1]+   U_22_m3m2m1.e[1][0]-3 *U_22_m3m2m1.e[1][1], 2.0)/200.0+
           pow(
	       5*U_02_p2m2m1.e[0][0]-15*U_02_p2m2m1.e[0][1]+3 *U_02_p2m2m1.e[1][0]
              -9*U_02_p2m2m1.e[1][1] -4*U_20_p2m2m1.e[0][0]-12*U_20_p2m2m1.e[1][0]
              +20*U_00_p2m2m1.e[0][0]+12*U_00_p2m2m1.e[1][0]-   U_22_p2m2m1.e[0][0]
              +3 *U_22_p2m2m1.e[0][1]-3 *U_22_p2m2m1.e[1][0]+9 *U_22_p2m2m1.e[1][1]

              +40*U_02_p2p1m1.e[0][0]-5 *U_20_p2p1m1.e[0][0]-15*U_20_p2p1m1.e[0][1]
              +15*U_20_p2p1m1.e[1][0]+45*U_20_p2p1m1.e[1][1]-20*U_00_p2p1m1.e[0][0]
              -60*U_00_p2p1m1.e[0][1]+10*U_22_p2p1m1.e[0][0]-30*U_22_p2p1m1.e[1][0], 2.0)/2700.0;
  last_dres=last_dres*coeff;

//RETURN: 
return(last_dres);
}

//
// Two Photon Cross Section Relativistic case linear polarized light
//
double greens_TPCSR_e1_lin(double Ep, int digits)
{
  double coeff;
  matrix_2x2 U_02_m3m2m1, U_20_m3m2m1, U_00_m3m2m1, U_22_m3m2m1,
            U_02_p2m2m1, U_20_p2m2m1, U_00_p2m2m1, U_22_p2m2m1,
            U_02_p2p1m1, U_20_p2p1m1, U_00_p2p1m1, U_22_p2p1m1,
            U_00_m1p1m1, U_02_m1p1m1, U_20_m1p1m1, U_22_m1p1m1,
            U_00_m1m2m1, U_02_m1m2m1, U_20_m1m2m1, U_22_m1m2m1;

  double err[20];
  _mU_n2=1;
  
  _GLeg_Piece=5.0/greens_save_nuclear_charge;

  coeff = TwoPi*greens_constant_cgs_c/pow(greens_constant_alpha, 3.0)/
          (greens_constant_cgs_a0*greens_constant_cgs_F0*greens_constant_cgs_F0*Ep*Ep);
  
  U_00_m3m2m1=greens_mU(0, 0, Ep, -3, -2, -1, digits); err[0]=last_derr;
  U_02_m3m2m1=greens_mU(0, 2, Ep, -3, -2, -1, digits); err[1]=last_derr;
  U_20_m3m2m1=greens_mU(2, 0, Ep, -3, -2, -1, digits); err[2]=last_derr;
  U_22_m3m2m1=greens_mU(2, 2, Ep, -3, -2, -1, digits); err[3]=last_derr;

  U_00_p2m2m1=greens_mU(0, 0, Ep, +2, -2, -1, digits); err[4]=last_derr;
  U_02_p2m2m1=greens_mU(0, 2, Ep, +2, -2, -1, digits); err[5]=last_derr;
  U_20_p2m2m1=greens_mU(2, 0, Ep, +2, -2, -1, digits); err[6]=last_derr;
  U_22_p2m2m1=greens_mU(2, 2, Ep, +2, -2, -1, digits); err[7]=last_derr;

  U_00_p2p1m1=greens_mU(0, 0, Ep, +2, +1, -1, digits); err[8]=last_derr;
  U_02_p2p1m1=greens_mU(0, 2, Ep, +2, +1, -1, digits); err[9]=last_derr;
  U_20_p2p1m1=greens_mU(2, 0, Ep, +2, +1, -1, digits); err[10]=last_derr;
  U_22_p2p1m1=greens_mU(2, 2, Ep, +2, +1, -1, digits); err[11]=last_derr;

  U_00_m1p1m1=greens_mU(0, 0, Ep, -1, +1, -1, digits); err[12]=last_derr;
  U_02_m1p1m1=greens_mU(0, 2, Ep, -1, +1, -1, digits); err[13]=last_derr;
  U_20_m1p1m1=greens_mU(2, 0, Ep, -1, +1, -1, digits); err[14]=last_derr;
  U_22_m1p1m1=greens_mU(2, 2, Ep, -1, +1, -1, digits); err[15]=last_derr;

  U_00_m1m2m1=greens_mU(0, 0, Ep, -1, -2, -1, digits); err[16]=last_derr;
  U_02_m1m2m1=greens_mU(0, 2, Ep, -1, -2, -1, digits); err[17]=last_derr;
  U_20_m1m2m1=greens_mU(2, 0, Ep, -1, -2, -1, digits); err[18]=last_derr;
  U_22_m1m2m1=greens_mU(2, 2, Ep, -1, -2, -1, digits); err[19]=last_derr;

  last_derr=err[0]; for(int i=1; i<20; i++){if (last_derr<err[i]) last_derr=err[i];};

  last_dres=(
  1.0/75.0*pow(-4*U_02_m3m2m1.e[1][0]+12*U_02_m3m2m1.e[1][1]+12*U_20_m3m2m1.e[0][0]
  -4.0*U_20_m3m2m1.e[1][0]-16.0*U_00_m3m2m1.e[1][0]+3.0*U_22_m3m2m1.e[0][0]
  -9.0*U_22_m3m2m1.e[0][1]-U_22_m3m2m1.e[1][0]+3.0*U_22_m3m2m1.e[1][1], 2.0)
  +pow(2.0/9.0*U_00_m1p1m1.e[0][0]-4.0/9.0*U_02_m1p1m1.e[0][0]-4.0/3.0*U_20_m1m2m1.e[1][0]
  -4.0/3.0*U_02_m1p1m1.e[1][0]-4.0/3.0*U_20_m1p1m1.e[0][1]+2.0*U_00_m1p1m1.e[1][1]
  +U_22_m1m2m1.e[1][1]-1.0/3.0*U_22_m1m2m1.e[0][1]+4.0/9.0*U_20_m1m2m1.e[0][0]
  +2.0/3.0*U_00_m1p1m1.e[0][1]+8.0/9.0*U_22_m1p1m1.e[0][0]+4.0/9.0*U_02_m1m2m1.e[0][0]
  -4.0/9.0*U_20_m1p1m1.e[0][0]+16.0/9.0*U_00_m1m2m1.e[0][0]+2.0/3.0*U_00_m1p1m1.e[1][0]
  -4.0/3.0*U_02_m1m2m1.e[0][1]-1.0/3.0*U_22_m1m2m1.e[1][0]+1.0/9.0*U_22_m1m2m1.e[0][0], 2.0)
  +8.0*pow(1.0/10.0*U_02_p2m2m1.e[1][1]-1.0/6.0*U_20_p2p1m1.e[1][0]-1.0/30.0*U_22_p2m2m1.e[0][1]
  -1.0/2.0*U_20_p2p1m1.e[1][1]+1.0/18.0*U_20_p2p1m1.e[0][0]+1.0/30.0*U_22_p2m2m1.e[1][0]
  +2.0/15.0*U_20_p2m2m1.e[1][0]+1.0/90.0*U_22_p2m2m1.e[0][0]-2.0/9.0*U_00_p2m2m1.e[0][0]
  +1.0/6.0*U_20_p2p1m1.e[0][1]-4.0/9.0*U_02_p2p1m1.e[0][0]+2.0/9.0*U_00_p2p1m1.e[0][0]
  +1.0/3.0*U_22_p2p1m1.e[1][0]+1.0/6.0*U_02_p2m2m1.e[0][1]-1.0/10.0*U_22_p2m2m1.e[1][1]
  +2.0/45.0*U_20_p2m2m1.e[0][0]+2.0/3.0*U_00_p2p1m1.e[0][1]-1.0/18.0*U_02_p2m2m1.e[0][0]
  -2.0/15.0*U_00_p2m2m1.e[1][0]-1.0/30.0*U_02_p2m2m1.e[1][0]-1.0/9.0*U_22_p2p1m1.e[0][0], 2.0))/4.0;

  last_dres=last_dres*coeff;

//RETURN:
  return(last_dres);
}

//
//  Two Photon Cross Section Relativistic case circular polarized light
//  (e1+e2)  
//
double greens_TPCSR_e1e2_circ(double Ep, int digits)
{
  double coeff;
  int i=0;

  double err[84];

  _GLeg_Piece=10.0/greens_save_nuclear_charge;

  coeff = TwoPi*greens_constant_cgs_c/pow(greens_constant_alpha, 3.0)/
          (greens_constant_cgs_a0*greens_constant_cgs_F0*greens_constant_cgs_F0*Ep*Ep);
  
  _mU_n2=1;
  
  double t10, t11, t114, t117, t129, t139, t152, t165, t168, t192, t216, t219, t242,
              t243, t244, t262, t280, t282, t285, t29, t30, t47, t65, t67, t90;

  matrix_2x2 U_13_m5m3m1, U_31_m5m3m1, U_11_m5m3m1, U_33_m5m3m1,
            U_03_m4m3m1, U_30_m4m3m1, U_00_m4m3m1, U_33_m4m3m1,
            U_23_m4m3m1, U_32_m4m3m1, U_21_m4m3m1, U_01_m4m3m1,
            U_12_m4m2m1, U_10_m4m2m1, U_20_m3m2m1, U_00_m3m2m1,
            U_22_m3m2m1, U_13_m3p2m1, U_31_m3p2m1, U_11_m3p2m1,
            U_33_m3p2m1, U_30_m4m2m1, U_32_m4m2m1, U_13_m3m3m1,
            U_31_m3m3m1, U_33_m3m3m1, U_11_m3m3m1, U_02_m3m2m1,
            U_10_m2p1m1, U_12_m2m2m1, U_23_m2m3m1, U_01_m2m3m1,
            U_32_m2m2m1, U_30_m2p1m1, U_23_m2p2m1, U_01_m2p2m1,
            U_10_m2m2m1, U_30_m2m2m1, U_03_m2m3m1, U_21_m2m3m1,
            U_12_m2p1m1, U_32_m2p1m1, U_21_m2p2m1, U_03_m2p2m1,
            U_02_p2m2m1, U_20_p2m2m1, U_22_p2p1m1, U_13_p2p2m1,
            U_02_p2p1m1, U_20_p2p1m1, U_00_p2p1m1, U_00_p2m2m1,
            U_22_p2m2m1, U_31_p2p2m1, U_11_p2p2m1, U_13_p2m3m1,
            U_31_p2m3m1, U_33_p2p2m1, U_33_p2m3m1, U_11_p2m3m1,
            U_30_p3p1m1, U_32_p3m2m1, U_01_p3m3m1, U_03_p3m3m1,
            U_21_p3m3m1, U_21_p3p2m1, U_10_p3p1m1, U_03_p3p2m1,
            U_01_p3p2m1, U_23_p3p2m1, U_12_p3m2m1, U_30_p3m2m1,
            U_23_p3m3m1, U_32_p3p1m1, U_10_p3m2m1, U_12_p3p1m1,
            U_33_p4m3m1, U_13_p4p2m1, U_31_p4p2m1, U_11_p4p2m1,
            U_33_p4p2m1, U_13_p4m3m1, U_31_p4m3m1, U_11_p4m3m1;
	    
            U_13_m5m3m1=greens_mU(1, 3, Ep, -5, -3, -1, digits); err[i]=last_derr; i=i+1;	    
            U_31_m5m3m1=greens_mU(3, 1, Ep, -5, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_11_m5m3m1=greens_mU(1, 1, Ep, -5, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_33_m5m3m1=greens_mU(3, 3, Ep, -5, -3, -1, digits); err[i]=last_derr; i=i+1;
            
            U_03_m4m3m1=greens_mU(0, 3, Ep, -4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_30_m4m3m1=greens_mU(3, 0, Ep, -4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_00_m4m3m1=greens_mU(0, 0, Ep, -4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_33_m4m3m1=greens_mU(3, 3, Ep, -4, -3, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_23_m4m3m1=greens_mU(2, 3, Ep, -4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_32_m4m3m1=greens_mU(3, 2, Ep, -4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_21_m4m3m1=greens_mU(2, 1, Ep, -4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_01_m4m3m1=greens_mU(0, 1, Ep, -4, -3, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_12_m4m2m1=greens_mU(1, 2, Ep, -4, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_10_m4m2m1=greens_mU(1, 0, Ep, -4, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_20_m3m2m1=greens_mU(2, 0, Ep, -3, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_00_m3m2m1=greens_mU(0, 0, Ep, -3, -2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_22_m3m2m1=greens_mU(2, 2, Ep, -3, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_13_m3p2m1=greens_mU(1, 3, Ep, -3, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_31_m3p2m1=greens_mU(3, 1, Ep, -3, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_11_m3p2m1=greens_mU(1, 1, Ep, -3, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_33_m3p2m1=greens_mU(3, 3, Ep, -3, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_30_m4m2m1=greens_mU(3, 0, Ep, -4, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_32_m4m2m1=greens_mU(3, 2, Ep, -4, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_13_m3m3m1=greens_mU(1, 3, Ep, -3, -3, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_31_m3m3m1=greens_mU(3, 1, Ep, -3, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_33_m3m3m1=greens_mU(3, 3, Ep, -3, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_11_m3m3m1=greens_mU(1, 1, Ep, -3, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_02_m3m2m1=greens_mU(0, 2, Ep, -3, -2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_10_m2p1m1=greens_mU(1, 0, Ep, -2, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_12_m2m2m1=greens_mU(1, 2, Ep, -2, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_23_m2m3m1=greens_mU(2, 3, Ep, -2, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_01_m2m3m1=greens_mU(0, 1, Ep, -2, -3, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_32_m2m2m1=greens_mU(3, 2, Ep, -2, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_30_m2p1m1=greens_mU(3, 0, Ep, -2, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_23_m2p2m1=greens_mU(2, 3, Ep, -2, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_01_m2p2m1=greens_mU(0, 1, Ep, -2, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_10_m2m2m1=greens_mU(1, 0, Ep, -2, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_30_m2m2m1=greens_mU(3, 0, Ep, -2, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_03_m2m3m1=greens_mU(0, 3, Ep, -2, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_21_m2m3m1=greens_mU(2, 1, Ep, -2, -3, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_12_m2p1m1=greens_mU(1, 2, Ep, -2, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_32_m2p1m1=greens_mU(3, 2, Ep, -2, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_21_m2p2m1=greens_mU(2, 1, Ep, -2, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_03_m2p2m1=greens_mU(0, 3, Ep, -2, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_02_p2m2m1=greens_mU(0, 2, Ep, +2, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_20_p2m2m1=greens_mU(2, 0, Ep, +2, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_22_p2p1m1=greens_mU(2, 2, Ep, +2, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_13_p2p2m1=greens_mU(1, 3, Ep, +2, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_02_p2p1m1=greens_mU(0, 2, Ep, +2, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_20_p2p1m1=greens_mU(2, 0, Ep, +2, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_00_p2p1m1=greens_mU(0, 0, Ep, +2, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_00_p2m2m1=greens_mU(0, 0, Ep, +2, -2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_22_p2m2m1=greens_mU(2, 2, Ep, +2, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_31_p2p2m1=greens_mU(3, 1, Ep, +2, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_11_p2p2m1=greens_mU(1, 1, Ep, +2, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_13_p2m3m1=greens_mU(1, 3, Ep, +2, -3, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_31_p2m3m1=greens_mU(3, 1, Ep, +2, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_33_p2p2m1=greens_mU(3, 3, Ep, +2, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_33_p2m3m1=greens_mU(3, 3, Ep, +2, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_11_p2m3m1=greens_mU(1, 1, Ep, +2, -3, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_30_p3p1m1=greens_mU(3, 0, Ep, +3, +1, -1, digits); err[i]=last_derr; i=i+1; 
            U_32_p3m2m1=greens_mU(3, 2, Ep, +3, -2, -1, digits); err[i]=last_derr; i=i+1; 
            U_01_p3m3m1=greens_mU(0, 1, Ep, +3, -3, -1, digits); err[i]=last_derr; i=i+1; 
            U_03_p3m3m1=greens_mU(0, 3, Ep, +3, -3, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_21_p3m3m1=greens_mU(2, 1, Ep, +3, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_21_p3p2m1=greens_mU(2, 1, Ep, +3, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_10_p3p1m1=greens_mU(1, 0, Ep, +3, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_03_p3p2m1=greens_mU(0, 3, Ep, +3, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_01_p3p2m1=greens_mU(0, 1, Ep, +3, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_23_p3p2m1=greens_mU(2, 3, Ep, +3, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_12_p3m2m1=greens_mU(1, 2, Ep, +3, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_30_p3m2m1=greens_mU(3, 0, Ep, +3, -2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_23_p3m3m1=greens_mU(2, 3, Ep, +3, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_32_p3p1m1=greens_mU(3, 2, Ep, +3, +1, -1, digits); err[i]=last_derr; i=i+1;
            U_10_p3m2m1=greens_mU(1, 0, Ep, +3, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_12_p3p1m1=greens_mU(1, 2, Ep, +3, +1, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_33_p4m3m1=greens_mU(3, 3, Ep, +4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_13_p4p2m1=greens_mU(1, 3, Ep, +4, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_31_p4p2m1=greens_mU(3, 1, Ep, +4, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_11_p4p2m1=greens_mU(1, 1, Ep, +4, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_33_p4p2m1=greens_mU(3, 3, Ep, +4, +2, -1, digits); err[i]=last_derr; i=i+1;
            U_13_p4m3m1=greens_mU(1, 3, Ep, +4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_31_p4m3m1=greens_mU(3, 1, Ep, +4, -3, -1, digits); err[i]=last_derr; i=i+1;
            U_11_p4m3m1=greens_mU(1, 1, Ep, +4, -3, -1, digits); err[i]=last_derr; i=i+1;
            
    t10 = (6.0*U_13_m5m3m1.e[1][0]-30.0*U_13_m5m3m1.e[1][1]-30.0*
U_31_m5m3m1.e[0][0]+6.0*U_31_m5m3m1.e[1][0]+36.0*U_11_m5m3m1.e[1][0]-5.0*
U_33_m5m3m1.e[0][0]+25.0*U_33_m5m3m1.e[0][1]+U_33_m5m3m1.e[1][0]-5.0*
U_33_m5m3m1.e[1][1]);

    t11 = t10*t10;

    t29 = -4.0*U_03_m4m3m1.e[1][0]+20.0*U_03_m4m3m1.e[1][1]+18.0*
U_21_m4m3m1.e[0][0]-6.0*U_21_m4m3m1.e[1][0]-24.0*U_01_m4m3m1.e[1][0]+3.0*
U_23_m4m3m1.e[0][0]-15.0*U_23_m4m3m1.e[0][1]-U_23_m4m3m1.e[1][0]+5.0*
U_23_m4m3m1.e[1][1]-6.0*U_12_m4m2m1.e[1][0]+18.0*U_12_m4m2m1.e[1][1]+20.0*
U_30_m4m2m1.e[0][0]-4.0*U_30_m4m2m1.e[1][0]-24.0*U_10_m4m2m1.e[1][0]+5.0*
U_32_m4m2m1.e[0][0]-15.0*U_32_m4m2m1.e[0][1]-U_32_m4m2m1.e[1][0]+3.0*
U_32_m4m2m1.e[1][1];

    t30 = t29*t29;

    t47 = -140.0/27.0*U_20_m3m2m1.e[1][0]-560.0/27.0*U_00_m3m2m1.e[1][0]+35.0/
9.0*U_22_m3m2m1.e[0][0]-35.0/3.0*U_22_m3m2m1.e[0][1]-35.0/27.0*
U_22_m3m2m1.e[1][0]+35.0/9.0*U_22_m3m2m1.e[1][1]+4.0*U_13_m3p2m1.e[0][0]+28.0/
3.0*U_13_m3p2m1.e[1][0]+16.0/9.0*U_31_m3p2m1.e[0][0]+80.0/9.0*
U_31_m3p2m1.e[0][1]+4.0/9.0*U_31_m3p2m1.e[1][0]+20.0/9.0*U_31_m3p2m1.e[1][1]-
U_11_m3p2m1.e[0][0]-5.0*U_11_m3p2m1.e[0][1]-7.0/3.0*U_11_m3p2m1.e[1][0]-35.0/
3.0*U_11_m3p2m1.e[1][1];

    t65 = -64.0/9.0*U_33_m3p2m1.e[0][0]-16.0/9.0*U_33_m3p2m1.e[1][0]-16.0/9.0*
U_13_m3m3m1.e[0][0]+80.0/9.0*U_13_m3m3m1.e[0][1]+16.0/9.0*U_13_m3m3m1.e[1][0]+
32.0/3.0*U_11_m3m3m1.e[1][0]-16.0/9.0*U_33_m3m3m1.e[0][0]+80.0/9.0*
U_33_m3m3m1.e[0][1]+16.0/9.0*U_33_m3m3m1.e[1][0]-80.0/9.0*U_33_m3m3m1.e[1][1]
-140.0/27.0*U_02_m3m2m1.e[1][0]+140.0/9.0*U_02_m3m2m1.e[1][1]+140.0/9.0*
U_20_m3m2m1.e[0][0]-80.0/9.0*U_13_m3m3m1.e[1][1]-32.0/3.0*U_31_m3m3m1.e[0][0]+
32.0/3.0*U_31_m3m3m1.e[1][0]-32.0/3.0*U_11_m3m3m1.e[0][0];

    t67 = pow(t47+t65,2.0);

    t90 = -15.0*U_10_m2p1m1.e[0][1]+5.0*U_12_m2m2m1.e[0][0]+3.0*
U_23_m2m3m1.e[1][0]+5.0*U_23_m2m3m1.e[0][1]-U_23_m2m3m1.e[0][0]-24.0*
U_01_m2m3m1.e[0][0]-15.0*U_23_m2m3m1.e[1][1]-15.0*U_32_m2m2m1.e[0][1]+5.0*
U_32_m2m2m1.e[0][0]-5.0*U_10_m2p1m1.e[0][0]+60.0*U_30_m2p1m1.e[0][1];

    t114 = 36.0*U_23_m2p2m1.e[0][0]+75.0*U_01_m2p2m1.e[1][1]+15.0*
U_01_m2p2m1.e[1][0]+45.0*U_01_m2p2m1.e[0][1]+9.0*U_01_m2p2m1.e[0][0]-20.0*
U_10_m2m2m1.e[1][0]+20.0*U_30_m2m2m1.e[0][0]-4.0*U_03_m2m3m1.e[0][0]+18.0*
U_21_m2m3m1.e[1][0]-6.0*U_21_m2m3m1.e[0][0]-5.0*U_12_m2m2m1.e[1][0];

    t117 = pow(10.0*U_12_m2p1m1.e[0][0]-40.0*U_32_m2p1m1.e[0][0]-75.0*
U_10_m2p1m1.e[1][1]+15.0*U_32_m2m2m1.e[1][1]-5.0*U_32_m2m2m1.e[1][0]-15.0*
U_21_m2p2m1.e[1][1]+12.0*U_23_m2p2m1.e[1][0]-9.0*U_21_m2p2m1.e[0][0]-60.0*
U_03_m2p2m1.e[1][0]-36.0*U_03_m2p2m1.e[0][0]+t90-15.0*U_12_m2m2m1.e[0][1]-3.0*
U_21_m2p2m1.e[1][0]-45.0*U_21_m2p2m1.e[0][1]-25.0*U_10_m2p1m1.e[1][0]+20.0*
U_30_m2p1m1.e[0][0]+50.0*U_12_m2p1m1.e[1][0]-20.0*U_30_m2m2m1.e[1][0]+20.0*
U_10_m2m2m1.e[0][0]+20.0*U_03_m2m3m1.e[0][1]+15.0*U_12_m2m2m1.e[1][1]+t114,2.0);

    t129 = 5.0/3.0*U_02_p2m2m1.e[0][0]-5.0*U_02_p2m2m1.e[0][1]+
U_02_p2m2m1.e[1][0]-3.0*U_02_p2m2m1.e[1][1]-4.0/3.0*U_20_p2m2m1.e[0][0]-4.0*
U_20_p2m2m1.e[1][0]+10.0/3.0*U_22_p2p1m1.e[0][0]-10.0*U_22_p2p1m1.e[1][0]-36.0/
5.0*U_13_p2p2m1.e[0][0]+36.0/5.0*U_13_p2p2m1.e[1][0]+40.0/3.0*
U_02_p2p1m1.e[0][0];

    t139 = -5.0/3.0*U_20_p2p1m1.e[0][0]-5.0*U_20_p2p1m1.e[0][1]+5.0*
U_20_p2p1m1.e[1][0]+15.0*U_20_p2p1m1.e[1][1]-20.0/3.0*U_00_p2p1m1.e[0][0]-20.0*
U_00_p2p1m1.e[0][1]+20.0/3.0*U_00_p2m2m1.e[0][0]+4.0*U_00_p2m2m1.e[1][0]-
U_22_p2m2m1.e[0][0]/3.0+U_22_p2m2m1.e[0][1]-U_22_p2m2m1.e[1][0];

    t152 = 3.0*U_22_p2m2m1.e[1][1]+9.0/5.0*U_31_p2p2m1.e[0][0]+9.0*
U_31_p2p2m1.e[0][1]-9.0/5.0*U_31_p2p2m1.e[1][0]-9.0*U_31_p2p2m1.e[1][1]+9.0/5.0
*U_11_p2p2m1.e[0][0]+9.0*U_11_p2p2m1.e[0][1]-9.0/5.0*U_11_p2p2m1.e[1][0]-9.0/
5.0*U_13_p2m3m1.e[0][0]-27.0/35.0*U_13_p2m3m1.e[1][0]+27.0/7.0*
U_13_p2m3m1.e[1][1];

    t165 = 72.0/35.0*U_31_p2m3m1.e[0][0]+288.0/35.0*U_31_p2m3m1.e[1][0]+9.0*
U_13_p2m3m1.e[0][1]-9.0*U_11_p2p2m1.e[1][1]-36.0/5.0*U_33_p2p2m1.e[0][0]+36.0/
5.0*U_33_p2p2m1.e[1][0]+48.0/35.0*U_33_p2m3m1.e[1][0]-48.0/7.0*
U_33_p2m3m1.e[1][1]-12.0/7.0*U_33_p2m3m1.e[0][1]-54.0/5.0*U_11_p2m3m1.e[0][0]
-162.0/35.0*U_11_p2m3m1.e[1][0]+12.0/35.0*U_33_p2m3m1.e[0][0];

    t168 = pow(t129+t139+t152+t165,2.0);

    t192 = 20.0/3.0*U_30_p3p1m1.e[1][0]+20.0*U_30_p3p1m1.e[1][1]+2.0/7.0*
U_32_p3m2m1.e[0][1]+48.0/7.0*U_01_p3m3m1.e[1][0]+8.0/7.0*U_03_p3m3m1.e[1][0]
-192.0/35.0*U_21_p3m3m1.e[1][0]+27.0/2.0*U_21_p3p2m1.e[1][1]-8.0*
U_10_p3p1m1.e[0][0]-24.0*U_10_p3p1m1.e[0][1]-9.0/10.0*U_21_p3p2m1.e[0][0]+72.0/
5.0*U_03_p3p2m1.e[0][0];

    t216 = -9.0/2.0*U_21_p3p2m1.e[0][1]+27.0/10.0*U_21_p3p2m1.e[1][0]-18.0/5.0*
U_01_p3p2m1.e[0][0]+18.0/5.0*U_23_p3p2m1.e[0][0]-54.0/5.0*U_23_p3p2m1.e[1][0]
-3.0/2.0*U_12_p3m2m1.e[0][1]-9.0/14.0*U_12_p3m2m1.e[1][1]-18.0*
U_01_p3p2m1.e[0][1]-8.0/21.0*U_30_p3m2m1.e[0][0]+16.0/7.0*U_23_p3m3m1.e[0][1]+
8.0/3.0*U_32_p3p1m1.e[0][0];

    t219 = pow(-32.0/21.0*U_30_p3m2m1.e[1][0]+2.0*U_10_p3m2m1.e[0][0]+6.0/7.0*
U_10_p3m2m1.e[1][0]-2.0/21.0*U_32_p3m2m1.e[0][0]+U_12_p3m2m1.e[0][0]/2.0-8.0/
21.0*U_32_p3m2m1.e[1][0]+8.0/7.0*U_32_p3m2m1.e[1][1]-40.0/7.0*
U_03_p3m3m1.e[1][1]-96.0/35.0*U_21_p3m3m1.e[0][0]-4.0*U_30_p3p1m1.e[0][1]+t192
-40.0/3.0*U_32_p3p1m1.e[1][0]+3.0/14.0*U_12_p3m2m1.e[1][0]+8.0/5.0*
U_03_p3m3m1.e[0][0]+32.0/7.0*U_23_p3m3m1.e[1][1]+48.0/5.0*U_01_p3m3m1.e[0][0]
-16.0/35.0*U_23_p3m3m1.e[0][0]+16.0*U_12_p3p1m1.e[0][0]-4.0/3.0*
U_30_p3p1m1.e[0][0]-8.0*U_03_p3m3m1.e[0][1]-32.0/35.0*U_23_p3m3m1.e[1][0]+t216, 2.0);

    t242 = 4.0/3.0*U_33_p4m3m1.e[1][0]-20.0/3.0*U_33_p4m3m1.e[1][1]-216.0/5.0*
U_13_p4p2m1.e[0][0]+9.0/5.0*U_31_p4p2m1.e[0][0]+9.0*U_31_p4p2m1.e[0][1]-9.0*
U_31_p4p2m1.e[1][0]-45.0*U_31_p4p2m1.e[1][1]+54.0/5.0*U_11_p4p2m1.e[0][0]+54.0*
U_11_p4p2m1.e[0][1]-36.0/5.0*U_33_p4p2m1.e[0][0]+36.0*U_33_p4p2m1.e[1][0];

    t243 = -9.0/5.0*U_13_p4m3m1.e[0][0]+9.0*U_13_p4m3m1.e[0][1]-
U_13_p4m3m1.e[1][0]+5.0*U_13_p4m3m1.e[1][1]+16.0/5.0*U_31_p4m3m1.e[0][0]+8.0*
U_31_p4m3m1.e[1][0]-54.0/5.0*U_11_p4m3m1.e[0][0]-6.0*U_11_p4m3m1.e[1][0]+8.0/
15.0*U_33_p4m3m1.e[0][0]-8.0/3.0*U_33_p4m3m1.e[0][1]+t242;

    t244 = t243*t243;

    t262 = 320.0/27.0*U_30_p3m2m1.e[1][0]-140.0/9.0*U_10_p3m2m1.e[0][0]-20.0/
3.0*U_10_p3m2m1.e[1][0]+20.0/27.0*U_32_p3m2m1.e[0][0]-35.0/9.0*
U_12_p3m2m1.e[0][0]+80.0/27.0*U_32_p3m2m1.e[1][0]-80.0/9.0*U_32_p3m2m1.e[1][1]
-200.0/27.0*U_03_p3m3m1.e[1][1]-32.0/9.0*U_21_p3m3m1.e[0][0]-20.0/9.0*
U_32_p3m2m1.e[0][1]+80.0/9.0*U_01_p3m3m1.e[1][0]+40.0/27.0*U_03_p3m3m1.e[1][0]
-64.0/9.0*U_21_p3m3m1.e[1][0]-35.0/3.0*U_21_p3p2m1.e[1][1]+7.0/9.0*
U_21_p3p2m1.e[0][0]-112.0/9.0*U_03_p3p2m1.e[0][0];

    t280 = -5.0/3.0*U_12_p3m2m1.e[1][0]+56.0/27.0*U_03_p3m3m1.e[0][0]+160.0/
27.0*U_23_p3m3m1.e[1][1]+112.0/9.0*U_01_p3m3m1.e[0][0]-16.0/27.0*
U_23_p3m3m1.e[0][0]-280.0/27.0*U_03_p3m3m1.e[0][1]-32.0/27.0*
U_23_p3m3m1.e[1][0]+35.0/9.0*U_21_p3p2m1.e[0][1]-7.0/3.0*U_21_p3p2m1.e[1][0]+
28.0/9.0*U_01_p3p2m1.e[0][0]-28.0/9.0*U_23_p3p2m1.e[0][0]+28.0/3.0*
U_23_p3p2m1.e[1][0]+35.0/3.0*U_12_p3m2m1.e[0][1]+5.0*U_12_p3m2m1.e[1][1]+140.0/
9.0*U_01_p3p2m1.e[0][1]+80.0/27.0*U_30_p3m2m1.e[0][0]+80.0/27.0*
U_23_p3m3m1.e[0][1];

    t282 = pow(t262+t280,2.0);

    t285 = t243*t243;    
  
  last_dres=8.0/3969.0*t11+2.0/735.0*t30+729.0/245000.0*t67+t117/7500.0+
       t168/300.0+t219/300.0+t244/1470.0+243.0/98000.0*t282+t285/4410.0;
  
  last_dres=last_dres*coeff;
  last_derr=err[0]; for(int k=1; k<i; k++){if (last_derr<err[k]) last_derr=err[k];};  
  
//RETURN:

  return(last_dres);
}

//
//  Two Photon Cross Section Relativistic case circular polarized light
//  (e1+m1)  
//
double greens_TPCSR_e1m1_circ(double Ep, int digits)
{
  double coeff;
  int i=0;

  double err[27];

  _GLeg_Piece=5.0/greens_save_nuclear_charge;

  coeff = TwoPi*greens_constant_cgs_c/pow(greens_constant_alpha, 3.0)/
          (greens_constant_cgs_a0*greens_constant_cgs_F0*greens_constant_cgs_F0*Ep*Ep);
  
  _mU_n2=1;

  matrix_2x2 U_02_m3m2m1, U_20_m3m2m1, U_22_m3m2m1, U_11_m3p2m1,
            U_00_m3m2m1, U_21_m2p2m1, U_12_m2p1m1, U_10_m2p1m1,
	    U_12_m2m2m1, U_21_m2m1m1, U_01_m2m1m1, U_01_m2p2m1,
	    U_10_m2m2m1, U_11_p2m1m1, U_22_p2m2m1, U_11_p2p2m1,
	    U_02_p2m2m1, U_00_p2m2m1, U_20_p2m2m1, U_20_p2p1m1,
	    U_00_p2p1m1, U_22_p2p1m1, U_02_p2p1m1, U_10_p3m2m1,
	    U_12_p3m2m1, U_01_p3p2m1, U_21_p3p2m1;
	    
            U_00_m3m2m1=greens_mU(0, 0, Ep, -3, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_02_m3m2m1=greens_mU(0, 2, Ep, -3, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_20_m3m2m1=greens_mU(2, 0, Ep, -3, -2, -1, digits); err[i]=last_derr; i=i+1;
            U_22_m3m2m1=greens_mU(2, 2, Ep, -3, -2, -1, digits); err[i]=last_derr; i=i+1;
	    
            U_11_m3p2m1=greens_mU(1, 1, Ep, -3, +2, -1, digits); err[i]=last_derr; i=i+1;	    
	    U_21_m2p2m1=greens_mU(2, 1, Ep, -2, +2, -1, digits); err[i]=last_derr; i=i+1;
	    U_12_m2p1m1=greens_mU(1, 2, Ep, -2, +1, -1, digits); err[i]=last_derr; i=i+1;
	    U_10_m2p1m1=greens_mU(1, 0, Ep, -2, +1, -1, digits); err[i]=last_derr; i=i+1;
	    
	    U_12_m2m2m1=greens_mU(1, 2, Ep, -2, -2, -1, digits); err[i]=last_derr; i=i+1;
	    U_21_m2m1m1=greens_mU(2, 1, Ep, -2, -1, -1, digits); err[i]=last_derr; i=i+1;
	    U_01_m2m1m1=greens_mU(0, 1, Ep, -2, -1, -1, digits); err[i]=last_derr; i=i+1;
	    U_01_m2p2m1=greens_mU(0, 1, Ep, -2, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
	    U_10_m2m2m1=greens_mU(1, 0, Ep, -2, -2, -1, digits); err[i]=last_derr; i=i+1;
	    U_11_p2m1m1=greens_mU(1, 1, Ep, +2, -1, -1, digits); err[i]=last_derr; i=i+1;
	    U_22_p2m2m1=greens_mU(2, 2, Ep, +2, -2, -1, digits); err[i]=last_derr; i=i+1;
	    U_11_p2p2m1=greens_mU(1, 1, Ep, +2, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
	    U_02_p2m2m1=greens_mU(0, 2, Ep, +2, -2, -1, digits); err[i]=last_derr; i=i+1;
	    U_00_p2m2m1=greens_mU(0, 0, Ep, +2, -2, -1, digits); err[i]=last_derr; i=i+1;
	    U_20_p2m2m1=greens_mU(2, 0, Ep, +2, -2, -1, digits); err[i]=last_derr; i=i+1;
	    U_20_p2p1m1=greens_mU(2, 0, Ep, +2, +1, -1, digits); err[i]=last_derr; i=i+1;
	    
	    U_00_p2p1m1=greens_mU(0, 0, Ep, +2, +1, -1, digits); err[i]=last_derr; i=i+1;
	    U_22_p2p1m1=greens_mU(2, 2, Ep, +2, +1, -1, digits); err[i]=last_derr; i=i+1;
	    U_02_p2p1m1=greens_mU(0, 2, Ep, +2, +1, -1, digits); err[i]=last_derr; i=i+1;
	    U_10_p3m2m1=greens_mU(1, 0, Ep, +3, -2, -1, digits); err[i]=last_derr; i=i+1;
	    
	    U_12_p3m2m1=greens_mU(1, 2, Ep, +3, -2, -1, digits); err[i]=last_derr; i=i+1;
	    U_01_p3p2m1=greens_mU(0, 1, Ep, +3, +2, -1, digits); err[i]=last_derr; i=i+1;
	    U_21_p3p2m1=greens_mU(2, 1, Ep, +3, +2, -1, digits); err[i]=last_derr; i=i+1;
	    
 double t13, t14, t29, t43, t45, t61, t77, t79, t93, t94;
 

t13 = -4.0*U_02_m3m2m1.e[1][0]+12.0*U_02_m3m2m1.e[1][1]+
12.0*U_20_m3m2m1.e[0][0]-4.0*U_20_m3m2m1.e[1][0]-16.0*U_00_m3m2m1.e[1][0]+3.0*
U_22_m3m2m1.e[0][0]-9.0*U_22_m3m2m1.e[0][1]-U_22_m3m2m1.e[1][0]+3.0*
U_22_m3m2m1.e[1][1]-9.0*U_11_m3p2m1.e[0][0]-9.0*U_11_m3p2m1.e[0][1]-9.0*
U_11_m3p2m1.e[1][0]-9.0*U_11_m3p2m1.e[1][1];
    t14 = t13*t13;
    t29 = -40.0*U_12_m2p1m1.e[0][0]-40.0*U_12_m2p1m1.e[1][0]+12.0*
U_01_m2p2m1.e[0][0]+60.0*U_21_m2m1m1.e[0][0]-80.0*U_01_m2m1m1.e[1][1]-80.0*
U_01_m2m1m1.e[1][0]+12.0*U_01_m2p2m1.e[0][1]+20.0*U_01_m2p2m1.e[1][1]+20.0*
U_01_m2p2m1.e[1][0]-20.0*U_21_m2m1m1.e[1][0]-8.0*U_12_m2m2m1.e[0][0]+24.0*
U_12_m2m2m1.e[0][1]+20.0*U_10_m2p1m1.e[0][0];
    t43 = 60.0*U_21_m2m1m1.e[0][1]-32.0*U_10_m2m2m1.e[1][0]-20.0*
U_21_m2m1m1.e[1][1]+60.0*U_10_m2p1m1.e[0][1]+20.0*U_10_m2p1m1.e[1][0]-12.0*
U_21_m2p2m1.e[0][0]-12.0*U_21_m2p2m1.e[0][1]-4.0*U_21_m2p2m1.e[1][0]-8.0*
U_12_m2m2m1.e[1][0]+24.0*U_12_m2m2m1.e[1][1]-4.0*U_21_m2p2m1.e[1][1]-32.0*
U_10_m2m2m1.e[0][0]+60.0*U_10_m2p1m1.e[1][1];
    t45 = pow(t43+t29,2.0);
    t61 = -120.0*U_22_p2p1m1.e[1][0]+20.0*U_02_p2m2m1.e[0][0]+40.0*
U_22_p2p1m1.e[0][0]-60.0*U_20_p2p1m1.e[0][1]+60.0*U_20_p2p1m1.e[1][0]-60.0*
U_02_p2m2m1.e[0][1]+180.0*U_20_p2p1m1.e[1][1]+12.0*U_22_p2m2m1.e[0][1]-20.0*
U_20_p2p1m1.e[0][0]+160.0*U_02_p2p1m1.e[0][0]-48.0*U_20_p2m2m1.e[1][0]+12.0*
U_02_p2m2m1.e[1][0]+48.0*U_00_p2m2m1.e[1][0]+72.0*U_11_p2p2m1.e[0][0];
    t77 = -80.0*U_00_p2p1m1.e[0][0]+80.0*U_00_p2m2m1.e[0][0]-180.0*
U_11_p2m1m1.e[0][0]+72.0*U_11_p2p2m1.e[0][1]+72.0*U_11_p2p2m1.e[1][1]+72.0*
U_11_p2p2m1.e[1][0]-240.0*U_00_p2p1m1.e[0][1]+36.0*U_22_p2m2m1.e[1][1]-180.0*
U_11_p2m1m1.e[1][0]-180.0*U_11_p2m1m1.e[0][1]-4.0*U_22_p2m2m1.e[0][0]-36.0*
U_02_p2m2m1.e[1][1]-12.0*U_22_p2m2m1.e[1][0]-180.0*U_11_p2m1m1.e[1][1]-16.0*
U_20_p2m2m1.e[0][0];
    t79 = pow(t77+t61,2.0);
    t93 = 12.0*U_10_p3m2m1.e[0][0]+12.0*U_10_p3m2m1.e[1][0]+3.0*
U_12_p3m2m1.e[0][0]-9.0*U_12_p3m2m1.e[0][1]+3.0*U_12_p3m2m1.e[1][0]-9.0*
U_12_p3m2m1.e[1][1]-12.0*U_01_p3p2m1.e[0][0]-12.0*U_01_p3p2m1.e[0][1]-3.0*
U_21_p3p2m1.e[0][0]-3.0*U_21_p3p2m1.e[0][1]+9.0*U_21_p3p2m1.e[1][0]+9.0*
U_21_p3p2m1.e[1][1];
    t94 = t93*t93;

  last_dres=t14/200.0+t45/4800.0+t79/43200.0+t94/200.0;
  
  last_dres=last_dres*coeff;
  last_derr=err[0]; for(int k=1; k<i; k++){if (last_derr<err[k]) last_derr=err[k];};  
  
//RETURN:
  return(last_dres);
}

