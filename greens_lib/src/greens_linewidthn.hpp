#ifndef __GREENS_LINEWIDTHN_HPP
#define __GREENS_LINEWIDTHN_HPP

#include "greens_wnj.hpp"
#include "greens_nonrelme.hpp"
//
// transitions probability A_{ki} from 
// Gordon W.F.Drake, Atomic & Molecule Opt Phys, Handbook, Chapter 10.
//[greens_A_ki]=1/sec. 
// Transition k -> i; greens_A_ki(2,1),  L_{\alpha} Lyman \alpha
// Transition k -> i; greens_A_ki(3,2),  H_{\alpha} Balmer \alpha
//
double greens_A_ki(int k, int i);
//
// radiative lifetime tau_k 
// Gordon W.F.Drake, Atomic & Molecule Opt Phys, Handbook, Chapter 10.
// [tau_k]=sec
// greens_lifetime_H_non(k)=sum(greens_A_ki(k,i),i=1..k-1)
//
double greens_lifetime_H_non(int k);

//
//
// width of energy level of Hydrogen (usually denoted as Gamma), atomic units  
// Gamma(k)=1/tau(k); [Gamma]=au
//
double greens_energy_width_H_non(int);
//
//
// width of energy level of Hydrogen (usually denoted as Gamma), atomic units  
// Gamma(n,l)=1/tau(n,l); [Gamma]=au
//
double greens_energy_width_H_non(int n, int l);

//
//
// width of energy level of Hydrogen (usually denoted as Gamma), atomic units  
// Gamma(n,l)=1/tau(n,l); [Gamma]=au
// numerical integration 
double greens_energy_width_H_non(int n, int l, int digits);


//
// Transition probability by spontan decay 
// from (n2,l2) to (n1,l1) state (sum over m1 and average over m2)
// [A(n_f,l_f,n_i,l_i)] = 1/sec
//
double greens_A_n1l1n2l2(int n1, int l1, int n2, int l2);
//
// stontan transition rate A_nl = sum_{nf,lf} A_{nf,lf,n,l} 
// [A_nl] = 1/sec
//
double greens_A_nl(int n, int l);
//
// radiative lifetime tau(n,l)=1/A_nl(n,l)
// [tau(n,l)] = sec
//
double greens_lifetime_H_non(int n, int l);

#endif
